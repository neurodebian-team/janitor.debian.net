#!/usr/bin/python3

import argparse
import json
import logging
import os
import sys
import tarfile
import tempfile
import urllib.error
from urllib.request import urlopen

import breezy.bzr
import breezy.git
from breezy.controldir import ControlDir
from breezy.errors import AlreadyBranchError, DivergedBranches
from breezy.workingtree import WorkingTree
from debmutate.control import ControlEditor
from lintian_brush.publish import (NoVcsLocation, update_control_for_vcs_url,
                                   update_offical_vcs)
from yarl import URL

parser = argparse.ArgumentParser()
parser.add_argument(
    '--branch-name', type=str, help="Branch name to import (defaults to HEAD)")
parser.add_argument('tarball_url', type=str)
args = parser.parse_args()

wt, subpath = WorkingTree.open_containing('.')

logging.basicConfig(format='%(message)s', level=logging.INFO)


def report_fatal(code: str, description: str, transient=None) -> None:
    if os.environ.get('SVP_API') == '1':
        with open(os.environ['SVP_RESULT'], 'w') as f:
            json.dump({
                'result_code': code,
                'transient': transient,
                'description': description}, f)
    logging.fatal('%s', description)


with tempfile.TemporaryDirectory() as td:
    with tempfile.NamedTemporaryFile() as rf:
        try:
            with urlopen(args.tarball_url) as resp:
                rf.write(resp.read())
                rf.seek(0)
        except urllib.error.HTTPError as e:
            if e.status == 404:
                report_fatal(
                    'archive-missing',
                    f'Tarball was not found at {args.tarball_url}',
                    transient=False)
                sys.exit(1)
            raise

        for suffix in ['xz', 'gz', 'bz2']:
            if args.tarball_url.endswith('.' + suffix):
                compression = ":" + suffix
                break
        else:
            compression = ""

        with tarfile.open(fileobj=rf, mode='r' + compression) as tf:
            tf.extractall(td)

    [gd] = os.listdir(td)
    cd = ControlDir.open(os.path.join(td, gd))
    branches = []
    for name in cd.branch_names():
        if name in (cd.open_branch(name=args.branch_name).name, ""):
            continue
        if (name in ('pristine-tar', 'upstream', 'pristine-lfs')
                or name.startswith('patch-queue/')):
            logging.info('Importing branch %s', name)
            try:
                target_branch = wt.controldir.create_branch(name=name)
            except AlreadyBranchError as e:
                logging.info(
                    'Branch already found creating %s: %s', name, e)
                target_branch = wt.controldir.open_branch(name=name)
            try:
                target_branch.pull(cd.open_branch(name=name))
            except DivergedBranches as e:
                report_fatal(
                    'diverged-branches',
                    f'Branch {name} diverged while pulling from archive: {e}',
                    transient=False)
                sys.exit(1)
            branches.append(name)
        else:
            logging.info('Not importing branch %s', name)
    wt.pull(cd.open_branch(name=args.branch_name))

    try:
        repo_url, _branch, _subpath = update_offical_vcs(wt, subpath)
    except NoVcsLocation as e:
        logging.warning(
            'Unable to determine proper vcs location, '
            'assuming collab-maint')
        with ControlEditor.from_tree(wt, subpath) as ce:
            repo_url = URL('https://salsa.debian.org/debian') / ce.source['Source']
            update_control_for_vcs_url(ce.source, 'Git', repo_url)
        wt.commit(specific_files=ce.changed_files, message="Set Vcs headers")

    if os.environ.get('SVP_API') == '1':
        with open(os.environ['SVP_RESULT'], 'w') as f:
            json.dump({
                'description': f'Imported branches {branches}',
                'value': 60,
                'context': {
                    'tarball-url': args.tarball_url,
                },
                'target-branch-url': repo_url, 
            }, f)
