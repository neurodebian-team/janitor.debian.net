{% extends "base.md" %}
{% block codemod %}
{% if tags|length == 1 -%}
Import version missing from the VCS: {{ tags[0][1] }}
{% else %}
Import {{ tags|length }} uploaded versions missing from the VCS
{% endif %}

Imported versions:

{% for tag, version in tags %}
* {{ version }}
{% endfor %}

If you already have an import of these versions, e.g. a local branch you haven't pushed yet, then you can simply ignore this merge proposal. One the versions show up in the branch, this merge proposal will automatically be closed.

{% endblock %}
