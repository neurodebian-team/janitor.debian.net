{% extends "base.md" %}
{% block codemod %}
{% if role == 'pristine-tar' %}
pristine-tar data for new upstream version {{ upstream_version }}.
{% elif role == 'upstream' %}
Import of new upstream version {{ upstream_version }}.
{% elif role == 'main' %}
Merge new upstream version {{ upstream_version }}.
{% endif %}

N.B. The package has been verified to build and pass its testuite with
the new upstream release merged, but has not been verified otherwise. Please
check for upstream changes that need to be reflected in the packaging, such as:

* license changes
* possibly breakage of reverse dependencies
{% include "binary-diff.md" %}
{% endblock %}
