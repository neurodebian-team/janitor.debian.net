{% extends "base.md" %}
{%- block codemod -%}
Fix watch file.

With the updated watch file, all recent previous upstream releases could be
located:

{% for version, details in codemod['entries'][0]['releases'].items() %}
* [{{ version }}]({{ details['url'] }})
{% endfor %}
{%- endblock -%}
