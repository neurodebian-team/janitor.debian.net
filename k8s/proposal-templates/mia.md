{% extends "base.md" %}
{% block codemod %}
Remove MIA uploaders:

{% for uploader in removed_uploaders %}
* [{{ parseaddr(uploader)[0] or parseaddr(uploader)[1] }}](https://qa.debian.org/developer.php?login={{ parseaddr(uploader)[1] }})
{% endfor %}

{% if bugs %}
See bugs {% for bug in bugs %}[{{bug}}](https://bugs.debian.org/{{bug}}){% if not loop.last %}, {% endif %}{% endfor %} for details.
{% endif %}
{% endblock %}
