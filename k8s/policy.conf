# Syntax:
# This file is read in order; later entries that match a package win.

# Each "policy" stanza can contain a number of settings and a set of
# matches that determine what packages the policy applies to.
# All conditions in a match must be met. A match can check for:
#
# * uploader: E-mail appears in Uploaders field.
# * maintainer: E-mail matches Maintainer field email.
# * source_package: Name matches Source package name
# * vcs_url_regex: Regex matching the Vcs-{Git,Hg,Svn,Bzr} URL
# * in_base: Does the package appear in the base distribution
# * before_stage: whether a release stage has not yet been entered

# Support modes:
#  build_only: Build the package, but don't do anything with it
#  push: Push the changes to the packaging branch
#  propose: Create a merge proposal with the changes
#  bts: Create and maintain a patch in the BTS
#  attempt_push: Push the changes to the packaging branch;
#     if the janitor doesn't have permission to push, create a merge proposal
#     instead.
#  push_derived: Push a derived branch to the Janitor's own fork,
#     but do not propose it for merging

# Default behaviour
policy {
  # Note that there's no match stanza here so policy here applies to all
  # packages.

  # Fixing lintian issues
  campaign {
    name: "lintian-fixes"
    command: "lintian-brush"
    publish { mode: attempt_push }
    manual_review: required;
  }

  # Don't propose merges for upstream merges for now, since the janitor only
  # proposes changes to the main branch, not to pristine-tar/upstream.
  # See https://salsa.debian.org/janitor.debian.net/debian-janitor/issues/18
  campaign {
    name: "fresh-releases"
    command: "deb-new-upstream --refresh-patches"
    publish { mode: push_derived }
    publish { role: 'main' mode: push_derived }
    publish { role: 'pristine-tar' mode: push_derived }
    publish { role: 'upstream' mode: push_derived }
    manual_review: required;
  }

  campaign {
    name: "fresh-snapshots"
    publish { mode: push_derived }
    publish { role: 'main' mode: push_derived }
    publish { role: 'pristine-tar' mode: push_derived }
    publish { role: 'upstream' mode: push_derived }
    command: "deb-new-upstream --snapshot --refresh-patches"
    manual_review: required;
  }

  # Builds of packaging repositories without any changes applied.
  # These are used as base when generating debdiffs and diffoscope diffs.
  campaign {
    name: "unchanged"
    publish { mode: build_only }
    command: "true"
    manual_review: required;
  }

  # Apply multi-arch changes (apply-multiarch-hints)
  campaign {
    name: "multiarch-fixes"
    publish { mode: attempt_push }
    command: "apply-multiarch-hints"
    manual_review: not_required;
  }

  # Mark orphaned packages as orphaned, updating Vcs-Git headers and Maintainer
  # field.
  campaign {
    name: "orphan"
    publish { mode: attempt_push }
    command: "deb-move-orphaned --no-update-vcs"
    manual_review: required;
  }

  # Drop people who are MIA from uploader fields
  campaign {
    name: "mia"
    publish { mode: attempt_push }
    command: "drop-mia-uploaders"
    manual_review: required;
  }

  # Import NMU changes
  campaign {
    name: "uncommitted"
    publish { mode: push_derived }
    command: "deb-import-uncommitted"
    manual_review: required;
  }

  # Import unversioned package
  campaign {
    name: "unversioned"
    publish { mode: push_derived }
    command: "deb-import-uncommitted --vcs-git-base=https://salsa.debian.org/debian/ --vcs-browser-base=https://salsa.debian.org/debian/"
    manual_review: not_required;
  }

  # Import packages from alioth archive
  campaign {
    name: "alioth-imports"
    publish { mode: push_derived }
    command: "import-from-alioth.py"
    manual_review: not_required;
  }

  # Remove obsolete dependencies and other settings.
  campaign {
    name: "scrub-obsolete"
    publish { mode: propose }
    command: "deb-scrub-obsolete"
    manual_review: required;
  }

  # Generating Debian packaging from scratch
  campaign {
    name: "debianize"
    publish { mode: push_derived }
    command: "debianize"
    manual_review: required;
  }

  # Test that we can debianize things currently in Debian
  campaign {
    name: "test-debianize"
    publish { mode: push_derived }
    command: "debianize"
    manual_review: required;
  }

  campaign {
    name: "bullseye-backports"
    publish { mode: push_derived }
    command: "deb-auto-backport --target-release=bullseye"
    manual_review: required;
  }

  campaign {
    name: "bookworm-backports"
    publish { mode: push_derived }
    command: "deb-auto-backport --target-release=bookworm"
    manual_review: required;
  }

  campaign {
    name: "watch-fixes"
    publish: { mode: push_derived }
    command: "fix-watch-file"
  }

  # Possible changelog types: auto, update, leave
  #
  # Auto means that the changelog will be updated by default, unless
  # some indicator is found that gbp-dch is used with the package
  # (e.g. a [dch] section in debian/gbp.conf)
  update_changelog: auto
}

# Don't upgrade exabgp packages beyond oldstable.
policy {
  match { source_package: "exabgp" }

  compat_release: "oldstable"
}

# Don't upgrade
# https://github.com/edsantiago/xlbiff/pull/10#issuecomment-1402863559
policy {
  match { source_package: "xlbiff" }

  compat_release: "oldstable"
}

# Allow sudo backports to oldoldstable
# https://salsa.debian.org/sudo-team/sudo/-/merge_requests/11#note_344701
policy {
  match { source_package: "sudo" }

  compat_release: "oldoldstable"

  # Hack until https://github.com/jelmer/janitor/issues/409 is fixed
  # See https://salsa.debian.org/sudo-team/sudo/-/merge_requests/8#note_364274
  campaign {
    name: "scrub-obsolete"
    publish { mode: push_derived }
  }
}

# Don't upgrade pypy packages beyond anything still in Ubuntu ESM.
policy {
  match { source_package: "pypy" }

  compat_release: "ubuntu/esm"
}

# Don't remove Binary fields that duplicate Source fields for Gard.
# https://salsa.debian.org/gspr/optuna/-/merge_requests/1#note_345370
policy {
  match { maintainer: "gspr@nonempty.org" }

  campaign {
    name: "lintian-fixes"
    command: "lintian-brush --exclude=binary-control-field-duplicates-source"
  }
}

# Don't remove Binary fields that duplicate Source fields for Michael Biebl
# https://salsa.debian.org/utopia-team/libnma/-/merge_requests/7#note_362923
policy {
  match { maintainer: "biebl@debian.org" }

  campaign {
    name: "lintian-fixes"
    command: "lintian-brush --exclude=binary-control-field-duplicates-source"
  }
}

# Packages that need to be backportable to Ubuntu LTS.
policy {
  match { maintainer: "pkg-grass-devel@lists.alioth.debian.org" }
  match { uploader: "sebastic@debian.org" }

  compat_release: "ubuntu/esm"

  campaign {
    name: "scrub-obsolete"
    publish { mode: propose }
  }
}

# People who prefer merge proposals without changelog updates,
# and for whom the autodetection doesn't work.
# See https://salsa.debian.org/janitor.debian.net/debian-janitor/issues/93
policy {
  match { maintainer: "hertzog@debian.org" }
  match { uploader: "hertzog@debian.org" }
  match { uploader: "olebole@debian.org" }
  match { source_package: "rdma-core" }
  match { maintainer: "pkg-gnome-maintainers@lists.alioth.debian.org" }
  match { maintainer: "pkg-go-maintainers@lists.alioth.debian.org" }
  match { maintainer: "paride@debian.org" }
  match { uploader: "paride@debian.org" }
  update_changelog: leave
}

# The GNOME team tries to keep dependencies in line with upstream declared
# requirements. Ideally we'd automatically detect when this is the case
# and not make any updates. Until then, don't mess with dependencies.
policy {
  match { maintainer: "pkg-gnome-maintainers@lists.alioth.debian.org" }

  campaign {
    name: "scrub-obsolete"
    command: "deb-scrub-obsolete --keep-minimum-depends-versions"
  }
}

# Haskell tooling just preserve what is declared upstream.
policy {
  match { maintainer: "pkg-haskell-maintainers@lists.alioth.debian.org" }
  match { maintainer: "debian-haskell@lists.debian.org" }

  campaign {
    name: "scrub-obsolete"
    command: "deb-scrub-obsolete --keep-minimum-depends-versions"
  }
}

# Same for the utopia team
policy {
  # https://salsa.debian.org/utopia-team/network-manager-openvpn/-/merge_requests/4#note_322472
  match { maintainer: "pkg-utopia-maintainers@lists.alioth.debian.org" }

  campaign {
    name: "scrub-obsolete"
    command: "deb-scrub-obsolete --keep-minimum-depends-versions"
  }

  # Force not updating the changelog
  update_changelog: leave
}

# The GNUstep maintainers don't like debhelper-compat.
policy {
  match { maintainer: "pkg-gnustep-maintainers@lists.alioth.debian.org" }

  campaign {
    name: "lintian-fixes"
    command: "lintian-brush --exclude=uses-debhelper-compat-file"
  }
}

# Rust uses debcargo
# See also https://salsa.debian.org/rust-team/debcargo/-/issues/39
policy {
  match { vcs_url_regex: "https://salsa.debian.org/rust-team/debcargo-conf.*" }

  campaign {
    name: "lintian-fixes"
    command: "lintian-brush --exclude=upstream-metadata-file"
  }
}

# Romain prefers to apply public-upstream-key-not-minimal manually.
policy {
  match { uploader: "rfrancoise@debian.org" }
  match { maintainer: "rfrancoise@debian.org" }

  campaign {
    name: "lintian-fixes"
    command: "lintian-brush --exclude=public-upstream-key-not-minimal"
  }
}

# For packages that are maintained by the QA team, attempt to push
# but fall back to proposing changes.
policy {
  match {
    maintainer: "packages@qa.debian.org"
  }
  campaign {
    name: "lintian-fixes"
    publish { mode: attempt_push }
  }
  campaign {
    name: "scrub-obsolete"
    publish { mode: attempt_push }
  }

  campaign {
    name: "fresh-releases"
    publish { mode: propose }
    publish { role: 'main' mode: propose }
    publish { role: 'pristine-tar' mode: propose }
    publish { role: 'upstream' mode: propose }
  }
}

policy {
  match { maintainer: "pkg-perl-maintainers@lists.alioth.debian.org" }
  update_changelog: update
}

# Enable attempt-push for the Perl team.
# See https://lists.debian.org/debian-perl/2019/12/msg00026.html
policy {
  match { maintainer: "pkg-perl-maintainers@lists.alioth.debian.org" }
  campaign {
    name: "scrub-obsolete"
    publish { mode: attempt_push }
  }
}

# Enable attempt-push for the Hurd and Compiz teams.
policy {
  match { maintainer: "debian-hurd@lists.debian.org" }
  match { maintainer: "bugs@hypra.fr" }
  campaign {
    name: "scrub-obsolete"
    publish { mode: attempt_push }
  }
}

# Enable attempt-push for the Go team.
policy {
  match { maintainer: "pkg-go-maintainers@lists.alioth.debian.org" }

  campaign {
    name: "scrub-obsolete"
    publish { mode: attempt_push }
  }
}

# Disable uses-debhelper-compat-file for the science team
# See https://lists.debian.org/debian-science/2022/11/msg00017.html
policy {
  match { maintainer: "debian-science-maintainers@lists.alioth.debian.org" }

  campaign {
    name: "lintian-fixes"
    command: "lintian-brush --exclude=uses-debhelper-compat-file"
  }
}

# Enable attempt-push for the JS team. See
# https://alioth-lists.debian.net/pipermail/pkg-javascript-devel/2019-December/037607.html
policy {
  match {
    maintainer: "pkg-javascript-devel@lists.alioth.debian.org"
  }

  campaign {
    name: "scrub-obsolete"
    publish { mode: attempt_push }
  }

  campaign {
    name: "fresh-releases"
    publish { mode: propose }
    publish { role: 'main' mode: propose }
    publish { role: 'pristine-tar' mode: propose }
    publish { role: 'upstream' mode: propose }
  }
}

# Enable attempt-push for the Ruby team.
# See https://lists.debian.org/debian-ruby/2019/12/msg00009.html
policy {
  match { maintainer: "pkg-ruby-extras-maintainers@lists.alioth.debian.org" }
  match { vcs_url_regex: "https:\/\/salsa\.debian\.org\/ruby-team\/.*" }
  campaign {
    name: "scrub-obsolete"
    publish { mode: attempt_push }
  }
}

policy {
  match { source_package: "git-build-recipe" }

  # Oldest release used to host Launchpad builders.
  compat_release: "focal"
}

# Enable attempt-push for some individuals
policy {
  match { maintainer: "jelmer@debian.org" }
  match { maintainer: "johfel@debian.org" }
  campaign {
    name: "lintian-fixes"
    manual_review: not_required;
  }

  campaign {
    name: "fresh-releases"
    publish { mode: attempt_push }
    publish { role: 'main' mode: attempt_push }
    publish { role: 'pristine-tar' mode: attempt_push }
    publish { role: 'upstream' mode: propose }
  }

  campaign {
    name: "scrub-obsolete"
    publish { mode: attempt_push }
  }
}

# See https://salsa.debian.org/md/libretls/-/merge_requests/1#note_303640
policy {
  match { maintainer: "md@linux.it" }

  campaign {
    name: "lintian-fixes"
    command: "lintian-brush --exclude=file-contains-trailing-whitespace"
  }
}

# usrmerge is meant to be backportable further
policy {
  match { source_package: "usrmerge" }

  compat_release: "oldoldstable"
}

# policy for picca@debian.org
policy {
  match { maintainer: "picca@debian.org" }
  match { uploader: "picca@debian.org" }

  campaign {
    name: "fresh-releases"
    publish { mode: attempt_push }
    publish { role: 'main' mode: attempt_push }
    publish { role: 'pristine-tar' mode: attempt_push }
    publish { role: 'upstream' mode: attempt_push }
  }

  campaign {
    name: "scrub-obsolete"
    publish { mode: attempt_push }
  }
}

policy {
  match { source_package: "debian-security-support" }

  compat_release: "jessie"
}

policy {
  match { maintainer: "pkg-java-maintainers@lists.alioth.debian.org" }
  # statsvn lives in java-team/, but is actually orphaned.
  match { source_package: "statsvn" }
  # epubcheck lives in java-team/, but is actually maintained by the Docbook
  # XML/SGML maintainers.
  match { source_package: "epubcheck" }

  # Backwards compatibility with debian LTS, see
  # https://salsa.debian.org/java-team/maven/-/merge_requests/3#note_351860
  compat_release: "debian/lts"

  campaign {
    name: "lintian-fixes"

    # No metadata file changes for the java maintainers
    # https://salsa.debian.org/java-team/carrotsearch-hppc/-/merge_requests/1#note_165639
    command: "lintian-brush --exclude=upstream-metadata-file"
    publish { mode: attempt_push }
  }

  campaign {
    name: "scrub-obsolete"
    command: "deb-scrub-obsolete --keep-minimum-depends-versions"
  }
}

# https://salsa.debian.org/webkit-team/webkit/-/merge_requests/5#note_176679
policy {
  match { source_package: "webkit2gtk" }

  compat_release: "oldstable"
}

# https://salsa.debian.org/debian/ddclient/-/merge_requests/1#note_177512
policy {
  match { source_package: "ddclient" }

  compat_release: "xenial"

  campaign {
    name: "lintian-fixes"
    command: "lintian-brush --exclude=changelog-has-duplicate-line"
  }
}

# https://lists.debian.org/debian-python/2020/07/msg00112.html
# https://lists.debian.org/debian-python/2022/02/msg00046.html
policy {
  match { maintainer: "python-modules-team@lists.alioth.debian.org" }
  match { maintainer: "python-apps-team@lists.alioth.debian.org" }
  match { maintainer: "team+python@tracker.debian.org" }
  match { vcs_url_regex: "https:\/\/salsa\.debian\.org\/python-team\/.*" }

  campaign {
    name: "scrub-obsolete"
    publish { mode: attempt_push }
  }
}

# testing bts mode
policy {
  match { source_package: "breezy" }
  campaign {
    name: "lintian-fixes"
    publish { mode: bts }
    manual_review: not_required;
  }
  campaign {
    name: "multiarch-fixes"
    publish { mode: bts }
  }
}

policy {
  match { source_package: "libtimedate-perl" }
  campaign {
    name: "lintian-fixes"
    publish { mode: push_derived }
  }
}

# The LLVM team backports to oldstable (?)
policy {
  match { maintainer: "pkg-llvm-team@lists.alioth.debian.org" }

  compat_release: "oldstable"
}

# These packages are too large
policy {
  match { source_package: "git-annex" }
  match { source_package: "ansible" }
  match { source_package: "stellarium" }
  match { source_package: "freedict" }

  campaign {
    name: "lintian-fixes"
    publish { mode: skip }
  }

  campaign {
    name: "unchanged"
    publish { mode: skip }
  }

  campaign {
    name: "fresh-releases"
    publish { mode: skip }
    publish { role: 'main' mode: skip }
    publish { role: 'pristine-tar' mode: skip }
    publish { role: 'upstream' mode: skip }
  }

  campaign {
    name: "fresh-snapshots"
    publish { mode: skip }
    publish { role: 'main' mode: skip }
    publish { role: 'pristine-tar' mode: skip }
    publish { role: 'upstream' mode: skip }
  }
}

# explicitly opt in my packages, -- anarcat, 2021-08-26
policy {
  match { maintainer: "anarcat@debian.org" }
  # default policy is "push_derived", but I do want those new upstream releases, this is great
  campaign {
    name: "fresh-releases"
    command: "deb-new-upstream --refresh-patches"
    publish { mode: attempt_push }
    publish { role: 'main' mode: attempt_push }
    publish { role: 'pristine-tar' mode: attempt_push }
    publish { role: 'upstream' mode: attempt_push }
  }
  # Import NMU changes as well
  campaign {
    name: "uncommitted"
    publish { mode: attempt_push }
  }
}

# explicitly opt in Andrea for fresh-releases, requested via email
policy {
  match { maintainer: "andrea@pappacoda.it" }
  # default policy is "push_derived", but I do want those new upstream releases, this is great
#
  campaign {
    name: "fresh-releases"
    command: "deb-new-upstream --refresh-patches"
    publish { mode: attempt_push }
    publish { role: 'main' mode: attempt_push }
    publish { role: 'pristine-tar' mode: attempt_push }
    publish { role: 'upstream' mode: attempt_push }
  }
}

# policy of georg@debian.org
policy {
  match { maintainer: "georg@debian.org" }
  match { uploader: "georg@debian.org" }

  campaign {
    name: "scrub-obsolete"
    publish { mode: attempt_push }
  }

  update_changelog: leave
}

# https://salsa.debian.org/debian/lighttpd/-/merge_requests/44#note_342232
policy {
  match: { source_package: "lighttpd" }
  campaign {
    name: "scrub-obsolete"
    publish { mode: push_derived }
  }
}

# Allow backport to oldest supported Ubuntu LTS
# https://salsa.debian.org/games-team/minigalaxy/-/merge_requests/1#note_346229
policy {
  match: { source_package: "minigalaxy" }

  compat_release: "ubuntu/lts"
}

# URL to load freeze information from.
freeze_dates_url: "https://release.debian.org/testing/freeze-and-release-dates.yaml"

# Don't push or create new merge proposals during the release freeze
# (currently 30 days before the soft freeze)

policy {
  match {
    after_stage {
      stage_name: "soft_freeze"
      days_delta: -30
    }
  }

  campaign {
    name: "lintian-fixes"
    publish { mode: push_derived }
  }
  campaign {
    name: "multiarch-fixes"
    publish { mode: push_derived }
  }
  campaign {
    name: "fresh-snapshots"
    publish { mode: push_derived }
    publish { role: 'main' mode: push_derived }
    publish { role: 'pristine-tar' mode: push_derived }
    publish { role: 'upstream' mode: push_derived }
  }
  campaign {
    name: "fresh-releases"
    publish { mode: push_derived }
    publish { role: 'main' mode: push_derived }
    publish { role: 'pristine-tar' mode: push_derived }
    publish { role: 'upstream' mode: push_derived }
  }
  campaign {
    name: "mia"
    publish { mode: push_derived }
  }
  campaign {
    name: "scrub-obsolete"
    publish { mode: push_derived }
  }

  # Note that uncommitted is intentionally excluded here, since it merely keeps
  # the repo in sync with the archive.
}

# Opt out
policy {
  match { maintainer: "pali.rohar@gmail.com" }
  match { maintainer: "pkg-grass-devel@lists.alioth.debian.org" }
  # https://lists.debian.org/debian-devel/2020/04/msg00201.html
  match { maintainer: "debian-med-packaging@lists.alioth.debian.org" }
  match { vcs_url_regex: "https:\/\/salsa\.debian\.org\/med-team\/.*" }
  # https://lists.debian.org/debian-devel/2020/04/msg00205.html
  match { maintainer: "r-pkg-team@alioth-lists.debian.net" }
  match { maintainer: "abe@debian.org" }
  # https://github.com/az143/glimpse/pull/3#issuecomment-1304441454
  match { maintainer: "az@debian.org" }

  #  https://gitlab.com/ubports/development/core/lomiri-schemas/-/merge_requests/8#note_1195474776
  match { vcs_url_regex: "htts://gitlab.com/ubports/.*" }

  # Requires CLA
  match { source_package: "zeroc-ice" }
  match { source_package: "ice-builder-gradle" }

  # Google CLA (enforced by CLA bot)
  match { vcs_url_regex: "https:\/\/github.com\/google\/.*" }

  # Requires DCO
  match { source_package: "rdma-core" }

  campaign {
    name: "lintian-fixes"
    publish { mode: push_derived }
  }
  campaign {
    name: "multiarch-fixes"
    publish { mode: push_derived }
  }
  campaign {
    name: "fresh-snapshots"
    publish { mode: push_derived }
    publish { role: 'main' mode: push_derived }
    publish { role: 'pristine-tar' mode: push_derived }
    publish { role: 'upstream' mode: push_derived }
  }
  campaign {
    name: "fresh-releases"
    publish { mode: push_derived }
    publish { role: 'main' mode: push_derived }
    publish { role: 'pristine-tar' mode: push_derived }
    publish { role: 'upstream' mode: push_derived }
  }
  campaign {
    name: "mia"
    publish { mode: push_derived }
  }
  campaign {
    name: "scrub-obsolete"
    publish { mode: push_derived }
  }
}
