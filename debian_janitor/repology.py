import json
import logging
import time
from urllib.request import Request, urlopen


def load_from_repology():
    headers = {
        'Accept': 'application/json',
    }

    last_project = None
    while True:
        url = f"https://repology.org/api/v1/projects/{last_project or ''}"
        logging.debug('Retrieving %s', url)
        req = Request(url, headers=headers)

        with urlopen(req) as resp:
            data = json.load(resp)
            yield from data.items()
            if len(data) < 200:
                return
            last_project = sorted(data.keys())[-1] + '/'
            time.sleep(1)
