#!/usr/bin/python
# Copyright (C) 2022 Jelmer Vernooij <jelmer@jelmer.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

import asyncio
import json
import logging
from contextlib import AsyncExitStack
from datetime import timedelta
from typing import Any, TypedDict

import apt_pkg
import asyncpg
import uvloop
import aiojobs
from aiojobs.aiohttp import setup as setup_aiojobs
from aiojobs.aiohttp import get_scheduler
from aiohttp import ClientSession, web
from aiohttp.web_middlewares import normalize_path_middleware
from aiohttp_openmetrics import setup_metrics, Counter
from breezy.plugins.debian.apt_repo import RemoteApt
from buildlog_consultant import Problem, problem_clses
from debian.changelog import Version
from debian.deb822 import PkgRelation
from janitor import state
from janitor.config import (Config, get_campaign_config, get_distribution,
                            read_config)
from janitor.schedule import do_schedule, do_schedule_control
from ognibuild.buildlog import problem_to_upstream_requirement
from ognibuild.debian.apt import AptManager
from ognibuild.resolver.apt import AptRequirement, resolve_requirement_apt
from ognibuild.resolver.dep_server import (DepServerError,
                                           resolve_apt_requirement_dep_server)
from ognibuild.session.plain import PlainSession
from ognibuild.upstream import UpstreamInfo, find_upstream
from redis.asyncio import Redis

from debian_janitor.policy import (ExpandedPolicy, PolicyApplyer,
                                   publish_policy_name,
                                   serialize_publish_policy,
                                   upload_publish_policy)


resolve_requirement_error_count = Counter(
    'resolve_requirement_error', "Errors during requirement resolving")

upstream_requirement_convert_error_count = Counter(
    'upstream_requirement_convert_error',
    'Errors while converting problem to upstream requirement')

problem_reconstruct_error_count = Counter(
    'problem_reconstruct_error',
    'Errors reconstructuring problems',
    ['result_code'])

schedule_error_count = Counter(
    'schedule_error',
    'Errors scheduling')

processed_count = Counter(
    'processed',
    'Processed count')


DEFAULT_SUCCESS_CHANCE = 0.5

PROBLEM_KINDS_TO_IGNORE = [
    "hosted-on-alioth",
    "unexpected-local-upstream-changes",
    "unsupported-vcs",
    "unsupported-vcs-svn",
    "unsupported-vcs-hg",
    "worker-failure",
    "401-unauthorized",
    "too-many-requests",
]


routes = web.RouteTableDef()


class Action:

    requestor: str | None

    async def schedule(
            self, conn, config,
            publisher_url: str | None,
            expanded_policy: ExpandedPolicy) -> int | None:
        raise NotImplementedError

    def json(self):
        raise NotImplementedError(self.json)


class Debianize(Action):

    upstream_info: UpstreamInfo
    DEFAULT_NEW_PACKAGE_VALUE = 150

    def __init__(self, upstream_info, change_set=None, requestor=None):
        self.upstream_info = upstream_info
        self.requestor = requestor
        self.change_set = change_set

    def __repr__(self):
        return f"<{type(self).__name__}({self.upstream_info!r})>"

    def json(self):
        return {
            "action": "debianize",
            "change_set": self.change_set,
            "requestor": self.requestor,
            "upstream_info": self.upstream_info.json(),
        }

    async def schedule(
            self, conn, config, publisher_url, expanded_policy):
        campaign = "debianize"
        if self.upstream_info.branch_url is None or self.upstream_info.name is None:
            raise IncompleteUpstreamInfo(self.upstream_info)
        package = self.upstream_info.name.replace('/', '-')
        logging.info(
            "Creating new upstream %s ⇒ %s",
            package, self.upstream_info.branch_url)
        codebase = await conn.fetchval(
            "INSERT INTO codebase (name, branch_url, url, branch, subpath) "
            "VALUES ($1, $2, $3, $4, $5) ON CONFLICT DO NOTHING RETURNING name",
            package, None, None, None, None)
        if codebase is None:
            codebase = package
        maintainer_email = config.committer
        await conn.execute(
            "INSERT INTO package (name, distribution, branch_url, subpath, "
            "maintainer_email, origin, vcs_url, codebase) "
            "VALUES ($1, $2, $3, $4, $5, $6, $7, $8) ON CONFLICT DO NOTHING",
            package, 'upstream', None, None,
            maintainer_email, self.requestor, None, codebase)
        if publisher_url:
            publish_policy = publish_policy_name(campaign, package)
            serialized_publish_policy = serialize_publish_policy(
                expanded_policy, maintainer_email)
            with ClientSession() as session:
                await upload_publish_policy(
                    session, publisher_url, publish_policy,
                    serialized_publish_policy)
        else:
            publish_policy = None
        command = get_campaign_config(config, campaign).command
        if self.upstream_info.version:
            command += ' --upstream-version=%s' % self.upstream_info.version

        if self.upstream_info.branch_subpath:
            command += ' {}/{}'.format(
                self.upstream_info.branch_url, self.upstream_info.branch_subpath)
        else:
            command += ' %s' % self.upstream_info.branch_url

        candidate_id = await conn.fetchval(
            "INSERT INTO candidate "
            "(codebase, suite, command, change_set, value, "
            "success_chance, publish_policy) "
            "VALUES ($1, $2, $3, $4, $5, $6, $7) "
            "ON CONFLICT (codebase, suite, COALESCE(change_set, ''::text)) "
            "DO UPDATE SET context = EXCLUDED.context, value = EXCLUDED.value, "
            "success_chance = EXCLUDED.success_chance, command = EXCLUDED.command, "
            "publish_policy = EXCLUDED.publish_policy "
            "RETURNING id",
            codebase, campaign, command, self.change_set,
            self.DEFAULT_NEW_PACKAGE_VALUE, DEFAULT_SUCCESS_CHANCE, publish_policy)

        await do_schedule(
            conn, campaign=campaign, codebase=codebase,
            change_set=self.change_set, requestor=self.requestor,
            bucket='missing-deps', command=command)
        return candidate_id


class NewUpstreamVersion(Action):

    name: str
    desired_version: Version | None = None
    DEFAULT_UPDATE_PACKAGE_VALUE = 150

    def __init__(self, name, desired_version=None, change_set=None, requestor=None):
        self.name = name
        self.release = "sid"
        self.desired_version = desired_version
        self.requestor = requestor
        self.change_set = change_set

    def json(self):
        return {
            "action": "new-upstream",
            "name": self.name,
            "release": self.release,
            "desired_version": self.desired_version,
            "requestor": self.requestor,
            "change_set": self.change_set,
        }

    def __repr__(self):
        return "<{}({!r}, {!r})>".format(
            type(self).__name__, self.name, self.desired_version)

    async def schedule(self, conn, config, publisher_url, expanded_policy):
        campaign = "fresh-releases"
        logging.info('Scheduling new run for %s/%s', self.name, campaign)
        # TODO(jelmer): Do something with desired_version
        # TODO(jelmer): fresh-snapshots?
        codebase = await conn.fetchval(
            'SELECT codebase FROM package '
            'WHERE name = $1 AND distribution = $2', self.name, self.release)
        assert codebase
        command = get_campaign_config(config, campaign).command
        candidate_id = await conn.fetchval(
            "INSERT INTO candidate "
            "(codebase, suite, context, value, success_chance, command) "
            "VALUES ($1, $2, $3, $4, $5, $6) ON CONFLICT DO NOTHING "
            "RETURNING id",
            codebase, campaign, None, self.DEFAULT_UPDATE_PACKAGE_VALUE,
            DEFAULT_SUCCESS_CHANCE, command)
        await do_schedule(
            conn, campaign=campaign,
            change_set=self.change_set, requestor=self.requestor,
            bucket='missing-deps',
            codebase=codebase)
        return candidate_id


class Backport(Action):

    name: str
    desired_version: Version | None = None
    target_release: str
    DEFAULT_BACKPORT_PACKAGE_VALUE = 150

    def __init__(self, name, target_release, desired_version=None,
                 change_set=None, requestor=None):
        self.name = name
        self.desired_version = desired_version
        self.target_release = target_release
        self.requestor = requestor
        self.release = "sid"
        self.change_set = change_set

    def json(self):
        return {
            "action": "backport",
            "name": self.name,
            "desired_version": self.desired_version,
            "target_release": self.target_release,
            "requestor": self.requestor,
            "release": self.release,
            "change_set": self.change_set,
        }

    def __repr__(self):
        return "<{}({!r}, {!r}, {!r})>".format(
            type(self).__name__, self.name, self.target_release,
            self.desired_version)

    async def schedule(self, conn, config, publisher_url, expanded_policy):
        # TODO(jelmer): Check desired_version
        campaign = f"{self.target_release}-backports"
        args: list[Any] = [campaign, self.name]
        query = (
            'SELECT * FROM debian_build WHERE distribution = $1 '
            'AND source = $2')
        if self.desired_version:
            query += " AND version >= $3"
            args.append(self.desired_version)
        existing_build = await conn.fetchrow(query, *args)
        if existing_build:
            return None
        logging.info('Scheduling new run for %s/%s', self.name, campaign)
        row = await conn.fetchrow(
            'SELECT codebase FROM package WHERE name = $1 AND release = $2',
            self.name, self.release)
        if not row:
            raise NonExistentPackage(self.name)
        codebase = row['codebase']
        assert codebase
        command = get_campaign_config(config, campaign).command
        candidate_id = await conn.fetchval(
            "INSERT INTO candidate "
            "(codebase, suite, context, value, success_chance, command) "
            "VALUES ($1, $2, $3, $4, $5, $6) ON CONFLICT DO NOTHING "
            "RETURNING id",
            codebase, campaign, None, self.DEFAULT_BACKPORT_PACKAGE_VALUE,
            DEFAULT_SUCCESS_CHANCE, command)
        await do_schedule(
            conn, campaign=campaign, change_set=self.change_set,
            requestor=self.requestor, bucket='missing-deps',
            codebase=codebase)
        return candidate_id


class ResultJson(TypedDict):
    """A result."""

    package: str
    codebase: str
    campaign: str
    change_set: str
    log_id: str
    description: str
    code: str
    failure_details: Any
    failure_stage: str
    target: dict[str, Any]
    logfilenames: list[str]
    codemod: Any
    value: int | None
    remotes: Any
    main_branch_revision: str | None
    start_time: str
    finish_time: str
    duration: int
    branch_url: str


class DebianBuildResultJson(TypedDict):

    build_distribution: str
    binary_packages: list[str]
    build_version: str


class IncompleteUpstreamInfo(Exception):
    """Incomplete upstream info."""

    def __init__(self, upstream_info):
        self.upstream_info = upstream_info


class NonExistentPackage(Exception):
    """Package does not exist."""

    def __init__(self, source):
        self.source = source


def has_relation(v, pkg):
    if not v:
        return False
    for r in PkgRelation.parse_relations(v):
        for o in r:
            if o['name'] == pkg:
                return True
    return False


def has_build_relation(c, pkg):
    return any(
        has_relation(c.get(f, ""), pkg)
        for f in ["Build-Depends", "Build-Depends-Indep", "Build-Depends-Arch",
                  "Build-Conflicts", "Build-Conflicts-Indep",
                  "Build-Conflicts-Arch"])


def has_runtime_relation(c, pkg):
    return any(
        has_relation(c.get(f, ""), pkg)
        for f in ["Depends", "Recommends", "Suggests", "Breaks", "Replaces"])


def find_reverse_source_deps(apt, binary_packages):
    # TODO(jelmer): in the future, we may want to do more than trigger
    # control builds here, e.g. trigger fresh-releases
    # (or maybe just if the control build fails?)

    need_control = set()
    with apt:
        for source in apt.iter_sources():
            if any(has_build_relation(source, p) for p in binary_packages):
                need_control.add(source['Package'])
                break

        for binary in apt.iter_binaries():
            if any(has_runtime_relation(binary, p) for p in binary_packages):
                need_control.add(binary['Source'].split(' ')[0])
                break

    return need_control


def reconstruct_problem(result_code: str, failure_details: Any) -> Problem | None:
    # TODO(Jelmer): Remove this once we've migrated away from these prefixes
    for prefix in ['build-', 'post-build-', 'dist-', 'install-deps-', 'unpack-']:
        if result_code.startswith(prefix):
            kind = result_code[len(prefix):]
            break
    else:
        kind = result_code

    if kind in PROBLEM_KINDS_TO_IGNORE:
        return None
    try:
        ret = problem_clses[kind].from_json(failure_details or {})
    except KeyError:
        logging.info(
            'Unable to reconstruct problem with kind %s, unknown.',
            kind)
        return None

    assert ret.kind == kind, \
        f"kind does not match: {ret.kind} != {kind}"
    return ret


class Control(Action):

    def __init__(self, codebase, *, main_branch_revision=None,
                 estimated_duration=None, change_set=None, requestor=None):
        self.codebase = codebase
        self.main_branch_revision = main_branch_revision
        self.estimated_duration = estimated_duration
        self.change_set = change_set
        self.requestor = requestor

    def __repr__(self):
        return "<{}({!r}, {!r})>".format(
            type(self).__name__, self.codebase, self.main_branch_revision)

    async def schedule(self, conn, config, publisher_url, expanded_policy):
        candidate_id = await conn.fetchval(
            "SELECT id FROM candidate WHERE suite = $1 AND codebase = $2 "
            "AND COALESCE(change_set, '') = $3",
            'control', self.codebase, self.change_set or '')
        await do_schedule_control(
            conn,
            codebase=self.codebase,
            change_set=self.change_set,
            main_branch_revision=self.main_branch_revision,
            estimated_duration=self.estimated_duration,
            requestor=self.requestor,
        )
        return candidate_id


async def schedule_control_run(
        conn, *, codebase: str, main_branch_revision: bytes | None,
        duration: timedelta | None = None) -> list[Action]:
    assert main_branch_revision is not None
    last_run = await conn.fetchrow(
        "SELECT 1 FROM run "
        "WHERE codebase = $1 AND revision = $2 "
        "AND run.result_code = 'success'",
        codebase, main_branch_revision.decode('utf-8')
        if main_branch_revision else None
    )
    if last_run is None:
        logging.info("Scheduling control run for %s.", codebase)
        return [Control(
            codebase=codebase,
            main_branch_revision=main_branch_revision,
            estimated_duration=duration,
            requestor="control")]
    else:
        logging.info(
            "Not requesting control run for %s, since one already exists",
            codebase)
        return []


class RetryRun(Action):

    def __init__(self, campaign, codebase, change_set=None, requestor=None):
        self.campaign = campaign
        self.codebase = codebase
        self.requestor = requestor
        self.change_set = change_set

    async def schedule(self, conn, config, publisher_url, expanded_policy):
        candidate_id = await conn.fetchval(
            'SELECT id FROM candidate WHERE codebase = $1 AND suite = $2 '
            'AND change_set = $3', self.codebase, self.campaign,
            self.change_set)
        await do_schedule(
            conn,
            change_set=self.change_set,
            bucket='missing-deps',
            requestor=self.requestor,
            campaign=self.campaign,
            codebase=self.codebase)
        return candidate_id


async def schedule_retry_reverse_deps(
        conn, *, config, package: str, change_set: str | None,
        debian_build_distribution: str | None) -> list[Action]:
    dependent_suites = [
        campaign.name for campaign in config.campaign
        if campaign.debian_build
        and debian_build_distribution
        in campaign.debian_build.extra_build_distribution]
    runs_to_retry = await conn.fetch(
        "SELECT codebase, suite AS campaign "
        "FROM last_missing_apt_dependencies "
        "WHERE name = $1 AND suite = ANY($2::text[])",
        package, dependent_suites)
    return [RetryRun(
        run_to_retry['campaign'],
        run_to_retry['codebase'],
        change_set=change_set,
        requestor=(
            f'schedule-missing-deps '
            f'(now newer {package} is available)'))
            for run_to_retry in runs_to_retry]


async def schedule_reverse_dependencies_control(
        conn, config, *, campaign: str,
        change_set: str,
        debian_build_version: Version | None,
        debian_binary_packages: list[str] | None) -> list[Action]:
    # TODO(jelmer): Get old_build_version from base_distribution

    campaign_config = get_campaign_config(config, campaign)
    base_distribution = get_distribution(
        config, campaign_config.debian_build.base_distribution)
    apt = RemoteApt(
        base_distribution.archive_mirror_uri, base_distribution.name,
        base_distribution.component)

    need_control = await asyncio.to_thread(
        find_reverse_source_deps, apt, debian_binary_packages)

    # TODO(jelmer): check test dependencies?

    ret: list[Action] = []
    for row in await conn.fetch(
            'SELECT name, codebase FROM package WHERE name = ANY($1::text[]) '
            'AND distribution = $2',
            need_control, base_distribution.name):
        logging.info("Scheduling control run for %s (%s).", row['codebase'],
                     row['name'])
        ret.append(Control(
            codebase=row['codebase'], requestor="control",
            change_set=change_set))

    return ret


async def followup_run(
        config: Config, policy_applier: PolicyApplyer, db,
        publisher_url: str | None,
        requirement_resolvers, *,
        campaign: str,
        codebase: str,
        result_code: str,
        run_id: str,
        change_set: str | None,
        main_branch_revision: bytes | None,
        failure_details: Any,
        duration: timedelta,
        debian_build_version: Version | None,
        debian_binary_packages: list[str] | None,
        debian_build_distribution: str | None) -> tuple[str, list[Action]]:
    async with db.acquire() as conn:
        publish_status = 'unknown'

        actions: list[Action] = []

        package = await conn.fetchrow(
            'SELECT name, maintainer_email, uploader_emails, in_base, '
            'vcs_url FROM package WHERE codebase = $1', codebase)

        expanded_policy = policy_applier.expand(
            campaign, package['name'],
            package['vcs_url'], package['maintainer_email'],
            package['uploader_emails'], package['in_base'])

        # If we get a successful run, then schedule a control run if we don't have one
        if result_code == "success":
            # TODO(jelmer): If there is an open merge proposal for this campaign,
            # then auto-approve
            if expanded_policy == "not-required":
                publish_status = 'approved'
            else:
                publish_status = 'needs-manual-review'
                # If we need review, then we'd like a base for the delta, for
                # some campaigns
                if campaign not in (
                        "control", "unchanged", "debianize", "unversioned",
                        "alioth-imports"):
                    needed_control_runs = await schedule_control_run(
                            conn, codebase=codebase,
                            main_branch_revision=main_branch_revision,
                            duration=duration)
                    if needed_control_runs:
                        actions.extend(needed_control_runs)
                        # Wait for the base to appear before reviewing
                        publish_status = 'blocked'

            # Trigger builds for any reverse dependencies in the same changeset
            if campaign in ('fresh-releases', 'fresh-snapshots'):
                # Find all binaries that have changed in this run
                if debian_binary_packages is None:
                    logging.warning(
                        'Missing debian result for run %s (%s/%s)',
                        run_id, package['name'], campaign)
                else:
                    actions.extend(await schedule_reverse_dependencies_control(
                        conn, config, campaign=campaign,
                        change_set=change_set,
                        debian_build_version=debian_build_version,
                        debian_binary_packages=debian_binary_packages))

            # Retry any runs that failed because they require this one
            if debian_binary_packages:
                actions.extend(await schedule_retry_reverse_deps(
                    conn, config=config, package=package['name'], change_set=change_set,
                    debian_build_distribution=debian_build_distribution))

            # If this is a control build, then update the status for anything that
            # requires it
            await conn.execute(
                "UPDATE run SET publish_status = 'needs-manual-review' "
                "WHERE main_branch_revision = $1 AND publish_status = 'blocked'",
                main_branch_revision.decode('utf-8') if main_branch_revision else None)
        elif result_code in ('nothing-to-do', 'nothing-new-to-do'):
            publish_status = 'ignored'
        else:
            if (campaign in ('unversioned', 'alioth-imports')
                    and result_code in ("unsupported-debhelper-compat-level", )):
                publish_status = 'needs-manual-review'
            else:
                # TODO(jelmer): set publish_status = 'ignored' for transient failures?
                # In the Debian Janitor, we never publish anything that didn't
                # successfully build.
                publish_status = 'rejected'
                try:
                    problem = reconstruct_problem(
                        result_code, failure_details)
                except Exception:  # noqa: PIE786
                    logging.exception('error reconstructing problem')
                    problem_reconstruct_error_count.labels(result_code=result_code).inc()
                else:
                    if problem is None:
                        logging.warning(
                            'Unable to reconstruct problem %s, skipping.',
                            result_code)
                    else:
                        try:
                            requirement = problem_to_upstream_requirement(problem)
                        except Exception:  # noqa: PIE786
                            upstream_requirement_convert_error_count.inc()
                            logging.exception(
                                'error converting problem to upstream requirement')
                        else:
                            if requirement is None:
                                logging.info(
                                    'Unable to convert problem to '
                                    'upstream requirement: %r', problem)
                            else:
                                actions.extend(
                                    await followup_missing_requirement(
                                        requirement_resolvers,
                                        campaign,
                                        requirement, change_set=change_set,
                                        needed_by=(
                                            f"{campaign}/{package['name']}")))

        await schedule_actions(
            conn, config, publisher_url, expanded_policy, run_id, actions)

        # TODO(jelmer): Use the runner API
        logging.info('Setting publish status of %s to %s',
                     run_id, publish_status)

        processed_count.inc()
        await conn.execute(
            'UPDATE run SET publish_status = $1 WHERE id = $2',
            publish_status, run_id)

        return publish_status, actions


class RequirementResolver:

    async def resolve(self, requirement, change_set=None,
                      requestor=None) -> list[Action] | None:
        raise NotImplementedError(self.resolve)


class FreshRequirementResolver(RequirementResolver):

    def __init__(self, apt_mgr, dep_server_url=None):
        self.apt_mgr = apt_mgr
        self.dep_server_url = dep_server_url

    async def to_apt_req(self, requirement) -> list[AptRequirement]:
        if self.dep_server_url:
            try:
                return await resolve_apt_requirement_dep_server(
                    self.dep_server_url, requirement)
            except DepServerError as e:
                logging.warning('Error from dep server: %s', e)
                logging.info('Falling back to local')
                return await resolve_requirement_apt(self.apt_mgr, requirement)
            except NotImplementedError:
                logging.info(
                    'Requirement %s not supported by depserver. '
                    'Falling back to local', requirement)
                return await resolve_requirement_apt(self.apt_mgr, requirement)

        else:
            return await resolve_requirement_apt(self.apt_mgr, requirement)

    def _construct_new_upstream_version(self, r, change_set=None):
        if not r.get('version'):
            logging.debug('package already available: %s', r['name'])
            return None
        elif r['version'][0] == '>=':
            depcache = apt_pkg.DepCache(self.apt_mgr.apt_cache._cache)
            depcache.init()
            version = depcache.get_candidate_ver(
                self.apt_mgr.apt_cache._cache[r['name']])
            if not version:
                logging.warning('unable to find source package matching %s',
                                r['name'])
                raise KeyError
            for file, index in version.file_list:
                records = apt_pkg.PackageRecords(
                    self.apt_mgr.apt_cache._cache)
                records.lookup((file, index))
                if records.source_pkg:
                    return NewUpstreamVersion(
                        records.source_pkg, r['version'][1], change_set=change_set)
            else:
                logging.warning(
                    "unable to find source package "
                    "matching %s", r['name'])
                raise KeyError
        else:
            logging.warning(
                "don't know what to do with constraint %r", r['version'])
            raise KeyError

    async def resolve(self, requirement, change_set=None,
                      requestor=None) -> list[Action] | None:
        options = []
        apt_opts = await self.to_apt_req(requirement)
        if apt_opts:
            for apt_req in apt_opts:
                option: list[Action] | None = []
                for entry in apt_req.relations:
                    for r in entry:
                        versions = self.apt_mgr.package_versions(r['name'])
                        if not versions:
                            upstream = find_upstream(apt_req)
                            if upstream:
                                option.append(Debianize(  # type: ignore
                                    upstream, requestor=requestor))
                            else:
                                option = None
                                break
                        else:
                            try:
                                u = self._construct_new_upstream_version(
                                    r, change_set=change_set)
                            except KeyError:
                                option = None
                                break
                            else:
                                if u:
                                    option.append(u)  # type: ignore
                    if option is None:
                        break
                if option == []:
                    return []
                if option is not None:
                    options.append(option)
        else:
            upstream = find_upstream(requirement)
            if upstream:
                options.append([Debianize(
                    upstream, change_set=change_set, requestor=requestor)])

        if options:
            return options[0]
        return None


class BackportRequirementResolver(RequirementResolver):

    def __init__(self, apt, target_release):
        self.apt = apt
        self.target_release = target_release

    def __repr__(self):
        return "{}({!r}, {!r})".format(
            type(self).__name__, self.apt, self.target_release)

    async def resolve(self, requirement, change_set=None,
                      requestor=None) -> list[Action] | None:
        # Check if it's in testing, and if so, schedule a backport
        if not isinstance(requirement, AptRequirement):
            return None
        ret: list[Action] = []
        with self.apt:
            for condition in requirement.relations:
                for option in condition:
                    sources = set()
                    for binary in self.apt.iter_binary_by_name(option['name']):
                        sources.add(binary['Source'])
                    if not sources:
                        continue
                    if len(sources) > 1:
                        logging.warning('more than one source found for %s: %r',
                                        option['name'], sources)
                    source_name = sources.pop()
                    if option['version']:
                        if option['version'][0] == '>=':
                            minimum_version = option['version'][1]
                        else:
                            logging.warning(
                                "don't know what to do with constraint %r for %r",
                                option['version'], option['name'])
                            continue
                    else:
                        minimum_version = None
                    ret.append(Backport(source_name, self.target_release,
                                        desired_version=minimum_version,
                                        change_set=change_set,
                                        requestor=requestor))
                    break
                else:
                    return None
            return ret


async def followup_missing_requirement(
         req_resolvers, campaign, requirement,
         change_set=None, needed_by=None) -> list[Action]:
    requestor = 'schedule-missing-deps'

    if needed_by:
        requestor += ' (needed by %s)' % needed_by

    try:
        req_resolver = req_resolvers[campaign]
    except KeyError:
        logging.debug('no requirement resolver for %s', campaign)
        return []

    try:
        actions = await req_resolver.resolve(
            requirement, change_set=change_set, requestor=requestor)
    except NotImplementedError:
        logging.exception('unable to resolve requirement')
        resolve_requirement_error_count.inc()
        return []
    logging.debug('%s: %r', requirement, actions)
    if actions is None:
        # We don't know what to do
        logging.info('Unable to find actions for requirement %r with %r',
                     requirement, req_resolver)
        return []
    assert isinstance(actions, list)
    if actions == []:
        # We don't need to do anything - could retry things that need this?
        return []

    return actions


async def schedule_actions(conn: asyncpg.Connection, config: Config,
                           publisher_url: str | None,
                           expanded_policy: ExpandedPolicy,
                           run_id: str,
                           actions: list[Action]):
    followups = []
    for action in actions:
        try:
            candidate_id = await action.schedule(
                conn, config, publisher_url, expanded_policy)
        except Exception:  # noqa: PIE786
            logging.exception('unable to schedule %r', action)
            schedule_error_count.inc()
            continue
        if candidate_id is not None:
            followups.append((run_id, candidate_id))

    await conn.executemany(
        'INSERT INTO followup (origin, candidate) VALUES ($1, $2) '
        'ON CONFLICT DO NOTHING', followups)


async def gather_requirements(
        db, *, run_ids: list[str] | None = None, reprocess: bool = False):
    async with db.acquire() as conn:
        query = """
SELECT
run.id AS run_id,
run.suite AS campaign,
run.change_set AS change_set,
run.result_code AS result_code,
run.codebase AS codebase,
run.failure_stage AS failure_stage,
run.failure_details AS failure_details,
run.finish_time - run.start_time AS duration,
run.main_branch_revision AS main_branch_revision,
debian_build.version AS debian_build_version,
debian_build.binary_packages AS debian_binary_packages,
debian_build.distribution AS debian_build_distribution
FROM last_unabsorbed_runs run
LEFT JOIN debian_build ON debian_build.run_id = run.id
WHERE True
"""
        args = []
        if run_ids:
            query += " AND id = ANY($1::text[])"
            args.append(run_ids)
        if not reprocess:
            query += " AND publish_status = 'unknown'"
        return await conn.fetch(query, *args)


@routes.get("/health", name="health")
async def handle_health(request):
    return web.Response(text="ok")


@routes.get("/ready", name="ready")
async def handle_ready(request):
    return web.Response(text="ok")


@routes.post("/scan", name="scan")
async def handle_scan(request):
    json = await request.json()
    await scan_existing(
        scheduler=get_scheduler(request), pool=request.app['db'],
        config=request.app['config'],
        policy_applier=request.app['policy_applier'],
        requirement_resolvers=request.app['requirement_resolvers'],
        run_ids=None, reprocess=json.get('reprocess', False),
        publisher_url=request.app['publisher_url'])
    return web.json_response()


@routes.post("/process/{run_id}", name="process")
async def handle_process(request):
    run_id = request.match_info['run_id']
    for row in await gather_requirements(
            request.app['db'], run_ids=[run_id], reprocess=True):
        publish_status, actions = await followup_run(
            request.app['config'], request.app['policy_applier'],
            request.app['db'], request.app['publisher_url'],
            request.app['requirement_resolvers'], campaign=row['campaign'],
            codebase=row['codebase'],
            result_code=row['result_code'],
            run_id=row['run_id'], change_set=row['change_set'],
            main_branch_revision=(
                row['main_branch_revision'].encode('utf-8')
                if row["main_branch_revision"] else None),
            failure_details=row['failure_details'],
            duration=row['duration'],
            debian_build_distribution=row['debian_build_distribution'],
            debian_build_version=row['debian_build_version'],
            debian_binary_packages=row['debian_binary_packages'])
        return web.json_response({
            'publish_status': publish_status,
            "actions": [a.json() for a in actions],
        })
    raise web.HTTPNotFound(text=f"no run {run_id}")


async def create_app(
        *, config, db, publisher_url, policy_applier, requirement_resolvers):
    trailing_slash_redirect = normalize_path_middleware(append_slash=True)
    app = web.Application(middlewares=[
        trailing_slash_redirect, state.asyncpg_error_middleware])
    app["db"] = db
    app['config'] = config
    app['publisher_url'] = publisher_url
    app['policy_applier'] = policy_applier
    app['requirement_resolvers'] = requirement_resolvers
    setup_metrics(app)
    app.router.add_routes(routes)
    setup_aiojobs(app, limit=20)
    return app


async def listen_to_runner(
        redis, config, policy_applier, publisher_url, database,
        requirement_resolvers):
    async def handle_result_message(msg):
        result: ResultJson = json.loads(msg['data'])
        debian_build_result: DebianBuildResultJson | None
        if result["target"] and result["target"].get("name") == "debian":
            debian_build_result = result["target"]["details"]
        else:
            debian_build_result = None
        await followup_run(
            config, policy_applier, database, publisher_url,
            requirement_resolvers, campaign=result['campaign'],
            result_code=result['code'],
            codebase=result['codebase'],
            run_id=result['log_id'], change_set=result['change_set'],
            main_branch_revision=(
                result['main_branch_revision'].encode('utf-8')
                if result["main_branch_revision"] else None),
            failure_details=result['failure_details'],
            duration=timedelta(seconds=result['duration']),
            debian_binary_packages=(
                debian_build_result['binary_packages']
                if debian_build_result else None),
            debian_build_version=(
                Version(debian_build_result['build_version'])
                if debian_build_result else None),
            debian_build_distribution=(
                debian_build_result['build_distribution']
                if debian_build_result else None))

    try:
        async with redis.pubsub(ignore_subscribe_messages=True) as ch:
            await ch.subscribe('result', result=handle_result_message)
            await ch.run()
    finally:
        await redis.close()


async def scan_existing(
        *, scheduler, pool, config, policy_applier, publisher_url,
        requirement_resolvers, run_ids=None, reprocess=False):
    run_ids = []
    for row in await gather_requirements(
            pool, run_ids=run_ids, reprocess=reprocess):
        run_ids.append(row['run_id'])
        await scheduler.spawn(followup_run(
            config, policy_applier, pool, publisher_url,
            requirement_resolvers, campaign=row['campaign'],
            change_set=row['change_set'],
            codebase=row['codebase'], result_code=row['result_code'],
            run_id=row['run_id'], main_branch_revision=(
                row['main_branch_revision'].encode('utf-8')
                if row['main_branch_revision'] else None),
            failure_details=row['failure_details'], duration=row['duration'],
            debian_build_version=row['debian_build_version'],
            debian_binary_packages=row['debian_binary_packages'],
            debian_build_distribution=row['debian_build_distribution']))
    return web.json_response({'run_ids': run_ids})


async def main(argv: list[str] | None = None):
    import argparse
    import warnings
    parser = argparse.ArgumentParser("followup")
    parser.add_argument(
        "--config", type=str, default="janitor.conf", help="Path to configuration."
    )
    parser.add_argument('--subscribe', action='store_true')
    parser.add_argument(
        "-r", dest="run_id", type=str, help="Run to process.", action="append"
    )
    parser.add_argument('--debug', action='store_true')
    parser.add_argument(
        '--dep-server-url', type=str, help="URL for ognibuild dep server")
    parser.add_argument(
        "--gcp-logging", action='store_true', help='Use Google cloud logging')
    parser.add_argument(
        "--listen-address", type=str, help="Listen address", default="localhost"
    )
    parser.add_argument(
        '--policy', type=str, help="Policy file", default="policy.conf")
    parser.add_argument("--port", type=int, help="Listen port", default=9931)
    parser.add_argument(
        "--publisher-url", type=str, help="Publisher URL")
    parser.add_argument("--reprocess", action="store_true")
    parser.add_argument(
        "--package-overrides", type=str, help="Package overrides")

    args = parser.parse_args(argv)
    with open(args.config) as f:
        config = read_config(f)

    if args.gcp_logging:
        import google.cloud.logging
        client = google.cloud.logging.Client()
        client.get_default_handler()
        client.setup_logging()
    elif args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    if args.debug:
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.slow_callback_duration = 0.1
        warnings.simplefilter('always', ResourceWarning)

    if args.subscribe and args.run_id:
        parser.error("Can only specify one of --run-id or --subscribe")
        return 1

    async with AsyncExitStack() as es:
        # TODO(jelmer): Use schroot rather than PlainSession
        sid_session = es.enter_context(PlainSession())
        policy_applier = await es.enter_async_context(
            PolicyApplyer.from_paths(args.policy, args.package_overrides))
        apt_mgr = AptManager.from_session(sid_session)

        testing = RemoteApt(
            "http://deb.debian.org/debian",
            "testing", ["main", "contrib", "non-free"])

        requirement_resolvers = {
            "fresh-releases": FreshRequirementResolver(apt_mgr, args.dep_server_url),
            "fresh-snapshots": FreshRequirementResolver(apt_mgr, args.dep_server_url),
            # apt(testing)
            "bookworm-backports": BackportRequirementResolver(testing, "bookworm"),
            # apt(testing)
            "bullseye-backports": BackportRequirementResolver(testing, "bullseye"),
        }

        pool = await es.enter_async_context(
            state.create_pool(config.database_location))
        if args.subscribe:
            redis = Redis.from_url(config.redis_location)
            app = await create_app(
                db=pool, config=config, publisher_url=args.publisher_url,
                policy_applier=policy_applier,
                requirement_resolvers=requirement_resolvers)

            runner = web.AppRunner(app)
            await runner.setup()
            site = web.TCPSite(runner, args.listen_address, args.port)
            logging.info("Listening on %s:%s", args.listen_address, args.port)
            await site.start()
            await listen_to_runner(
                redis, config, policy_applier, args.publisher_url, pool,
                requirement_resolvers)
        else:
            scheduler = aiojobs.Scheduler(limit=20)
            await scan_existing(
                scheduler=scheduler, pool=pool, config=config,
                policy_applier=policy_applier,
                requirement_resolvers=requirement_resolvers,
                run_ids=args.run_id or None, reprocess=args.reprocess,
                publisher_url=args.publisher_url)


if __name__ == '__main__':
    import sys

    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    sys.exit(asyncio.run(main(sys.argv[1:])))
