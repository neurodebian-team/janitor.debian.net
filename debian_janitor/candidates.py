#!/usr/bin/python
# Copyright (C) 2018 Jelmer Vernooij <jelmer@jelmer.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

"""Import candidates."""

import asyncio
import logging
import shlex
import sys

import backoff
import uvloop
from aiohttp import (ClientOSError, ClientResponseError, ClientSession,
                     ServerDisconnectedError)
from aiohttp_openmetrics import Counter
from google.protobuf import text_format  # type: ignore
from janitor import state
from janitor.config import get_campaign_config, read_config
from yarl import URL

from .candidates_pb2 import CandidateList
from .package_overrides_pb2 import OverrideConfig
from .policy import expand_policy, read_policy, read_release_stages

unknown_campaigns_count = Counter("unknown_campaigns", "Number of unknown campaigns")
unknown_codebase_count = Counter("unknown_codebases", "Number of unknown codebases")
invalid_command_count = Counter(
    "invalid_commands", "Number of candidates with invalid command")


def iter_candidates_from_script(stdin):
    candidate_list = text_format.Parse(stdin.read(), CandidateList())
    return candidate_list.candidate


def determine_command(
        policy, overrides, config, package_info, release_stages_passed,
        campaign, package, extra_args):
    if not package_info:
        campaign = get_campaign_config(config, campaign)
        command = campaign.command
    else:
        command = expand_policy(
            policy,
            overrides,
            campaign,
            package_info['name'],
            package_info['vcs_url'],
            package_info['maintainer_email'],
            package_info['uploader_emails'],
            package_info['in_base'],
            release_stages_passed
        ).command
    if extra_args:
        command = shlex.join(shlex.split(command) + extra_args)
    return command


async def ensure_upstream_package_exists(conn, config, package, origin=None):
    codebase = await conn.fetchval(
        "INSERT INTO codebase (name, branch_url, url, branch, subpath) "
        "VALUES ($1, $2, $3, $4, $5) ON CONFLICT DO NOTHING RETURNING name",
        f'{package}-upstream', None, None, None, None)
    codebase = f'{package}-upstream'
    await conn.execute(
        "INSERT INTO package (name, distribution, branch_url, subpath, "
        "maintainer_email, origin, vcs_url, codebase) "
        "VALUES ($1, $2, $3, $4, $5, $6, $7, $8) ON CONFLICT DO NOTHING",
        package, 'upstream', None, '',
        config.committer, origin, None, codebase)
    return codebase


@backoff.on_exception(
    backoff.expo,
    (ConnectionResetError, ClientOSError, ServerDisconnectedError,
     asyncio.TimeoutError),
    max_tries=8)
async def upload_candidate(session, runner_url, json_candidate):
    ret = 0
    async with session.post(
            URL(runner_url) / "candidates",
            json=[json_candidate], raise_for_status=True) as resp:
        result = await resp.json()
        if result['unknown_campaigns']:
            logging.warning(
                'unknown campaigns: %r', result['unknown_campaigns'])
            unknown_campaigns_count.inc()
            ret = 1
        if result['unknown_codebases']:
            logging.warning(
                'unknown codebases: %r', result['unknown_codebases'])
            unknown_codebase_count.inc()
            ret = 1
        if result['invalid_command']:
            logging.warning(
                'invalid command: %r', result['invalid_command'])
            invalid_command_count.inc()
            ret = 1
    return ret


async def main():
    import argparse

    from aiohttp_openmetrics import REGISTRY, Gauge, push_to_gateway

    parser = argparse.ArgumentParser(prog="candidates")
    parser.add_argument("packages", nargs="*")
    parser.add_argument(
        "--prometheus", type=str, help="Prometheus push gateway to export to."
    )
    parser.add_argument(
        "--policy", type=str, help="Path to policy", default="policy.conf")
    parser.add_argument(
        "--gcp-logging", action='store_true', help='Use Google cloud logging.')
    parser.add_argument(
        "--config", type=str, default="janitor.conf", help="Path to configuration.")
    parser.add_argument(
        "--campaign", type=str, help="Campaign to report to prometheus")
    parser.add_argument(
        "--package-overrides", type=str, help="Package overrides")
    parser.add_argument('--debug', action='store_true')
    parser.add_argument("--runner-url", type=str, help="Runner URL")

    args = parser.parse_args()

    with open(args.policy) as f:
        policy = read_policy(f)

    if args.package_overrides:
        with open(args.package_overrides) as f:
            overrides = text_format.Parse(f.read(), OverrideConfig())
    else:
        overrides = OverrideConfig()

    if policy.freeze_dates_url:
        release_stages_passed = await read_release_stages(policy.freeze_dates_url)
        logging.info('Release stages passed: %r', release_stages_passed)
    else:
        release_stages_passed = None

    if args.gcp_logging:
        import google.cloud.logging
        client = google.cloud.logging.Client()
        client.get_default_handler()
        client.setup_logging()
    else:
        logging.basicConfig(
            level=(logging.DEBUG if args.debug else logging.INFO),
            format='%(message)s')

    last_success_gauge = Gauge(
        "job_last_success_unixtime", "Last time a batch job successfully finished"
    )
    with open(args.config) as f:
        config = read_config(f)

    db = await state.create_pool(config.database_location)
    async with db.acquire() as conn:
        package_info = {
            (row['distribution'], row['name']): row
            for row in await conn.fetch('SELECT * FROM package')}

    logging.info("Adding candidates")
    ret = 0
    errors = 0
    total = 0
    async with ClientSession() as session:
        for candidate in iter_candidates_from_script(sys.stdin):
            info = package_info.get((candidate.distribution, candidate.package))
            if info is None:
                if candidate.distribution == 'upstream':
                    async with db.acquire() as conn:
                        codebase = await ensure_upstream_package_exists(
                            conn, config, candidate.package,
                            origin=candidate.origin or args.campaign)
                else:
                    logging.warning('Unable to find package %s/%s',
                                    candidate.distribution, candidate.package)
                    continue
            else:
                if candidate.distribution == 'sid':
                    codebase = info['codebase']
                else:
                    codebase = info['codebase'] + '-' + candidate.distribution
            assert codebase
            assert candidate.value is None or candidate.value > 0
            json_candidate = {
                'codebase': codebase,
                'campaign': candidate.campaign,
                'publish-policy': f'{candidate.campaign}-{candidate.package}',
                'command': candidate.command or determine_command(
                     policy, overrides, config, info, release_stages_passed,
                     candidate.campaign, candidate.package,
                     list(candidate.extra_arg)),
                'context': candidate.context,
                'value': candidate.value,
                'success_chance': candidate.success_chance,
                'requestor': args.campaign,
                'comment': candidate.comment,
            }

            total += 1
            logging.debug('Uploading candidate: %r', json_candidate)
            try:
                await upload_candidate(session, args.runner_url, json_candidate)
            except ClientResponseError as e:
                errors += 1
                if e.status // 100 == 5:
                    logging.warning('Error from /candidates: %d', e.status)
                    ret = 1
                else:
                    raise

    logging.info('Uploaded %d candidates, with %d errors.', total, errors)
    last_success_gauge.set_to_current_time()
    if args.prometheus and args.campaign:
        await push_to_gateway(
            args.prometheus, job="debian_janitor.candidates",
            registry=REGISTRY, grouping_key={'campaign': args.campaign})
    return ret


if __name__ == "__main__":
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    asyncio.run(main())
