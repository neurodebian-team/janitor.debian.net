#!/usr/bin/python
# Copyright (C) 2019 Jelmer Vernooij <jelmer@jelmer.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

from typing import Any

import asyncpg
from aiohttp import web, ClientResponseError, ClientConnectorError

from breezy.revision import NULL_REVISION

from janitor import state
from janitor.config import get_campaign_config
from janitor.queue import Queue
from janitor.site.common import (
    get_unchanged_run, get_run, get_last_unabsorbed_run, get_candidate,
    get_previous_runs, splitout_env)
from janitor.site import (
    BuildDiffUnavailable, DebdiffRetrievalError,
    get_archive_diff)


async def iter_candidates(
        conn: asyncpg.Connection,
        codebases: list[str] | None = None,
        campaign: str | None = None):
    query = """
SELECT
  package.name AS package,
  candidate.codebase AS codebase,
  candidate.suite AS suite,
  candidate.context AS context,
  candidate.value AS value,
  candidate.success_chance AS success_chance
FROM candidate
LEFT JOIN package ON candidate.codebase = package.codebase
"""
    args = []
    if campaign is not None and codebases is not None:
        query += " WHERE candidate.codebase = ANY($1::text[]) AND suite = $2"
        args.extend([codebases, campaign])
    elif campaign is not None:
        query += " WHERE suite = $1"
        args.append(campaign)
    elif codebases is not None:
        query += " WHERE candidate.codebase = ANY($1::text[])"
        args.append(codebases)
    return await conn.fetch(query, *args)


async def generate_pkg_context(
    db, config, suite, client, differ_url, vcs_managers, package_name, span, run_id=None
):
    async with db.acquire() as conn:
        # TODO(jelmer): Run these in parallel with gather()
        with span.new_child('sql:package'):
            package = await conn.fetchrow("""\
SELECT package.*, named_publish_policy.per_branch_policy AS publish_policy
FROM package
LEFT JOIN candidate ON package.codebase = candidate.codebase AND candidate.suite = $2
LEFT JOIN named_publish_policy ON named_publish_policy.name = candidate.publish_policy
WHERE package.name = $1 AND package.distribution = 'sid'""", package_name, suite)
        if package is None:
            raise web.HTTPNotFound(text='no such package: %s' % package_name)
        if run_id is not None:
            with span.new_child('sql:run'):
                run = await get_run(conn, run_id)
            if not run:
                raise web.HTTPNotFound(text='no such run: %s' % run_id)
            merge_proposals = []
        else:
            with span.new_child('sql:unchanged-run'):
                run = await get_last_unabsorbed_run(conn, package_name, suite)
            with span.new_child('sql:merge-proposals'):
                merge_proposals = await conn.fetch("""\
SELECT
    DISTINCT ON (merge_proposal.url)
    merge_proposal.url AS url, merge_proposal.status AS status
FROM
    merge_proposal
LEFT JOIN run
ON merge_proposal.revision = run.revision AND run.result_code = 'success'
WHERE run.codebase = $1 AND run.suite = $2
""", package['codebase'], suite)
        if run is None:
            # No runs recorded
            run_id = None
            unchanged_run = None
        else:
            run_id = run['id']
            if run['main_branch_revision']:
                with span.new_child('sql:unchanged-run'):
                    unchanged_run = await get_unchanged_run(
                        conn, run['codebase'],
                        run['main_branch_revision'].encode('utf-8'))
            else:
                unchanged_run = None

        with span.new_child('sql:candidate'):
            candidate = await get_candidate(conn, package['codebase'], suite)
        if candidate is not None:
            (candidate_context, candidate_value, candidate_success_chance) = candidate
        else:
            candidate_context = None
            candidate_value = None
            candidate_success_chance = None
        with span.new_child('sql:previous-runs'):
            previous_runs = await get_previous_runs(conn, package['codebase'], suite)
        with span.new_child('sql:queue-position'):
            queue = Queue(conn)
            (queue_position, queue_wait_time) = await queue.get_position(
                suite, package['codebase'])
        if run_id:
            with span.new_child('sql:reviews'):
                reviews = await conn.fetch(
                    'SELECT * FROM review WHERE run_id = $1 '
                    'ORDER BY reviewed_at ASC', run_id)
        else:
            reviews = None

    async def show_diff(role):
        try:
            (remote_name, base_revid, revid) = state.get_result_branch(
                run['result_branches'], role)
        except KeyError:
            return "no result branch with role %s" % role
        if base_revid == revid:
            return ""
        if run['vcs_type'] is None:
            return "not in a VCS"
        if revid is None:
            return "Branch deleted"
        try:
            with span.new_child('vcs-diff'):
                diff = await vcs_managers[run['vcs_type']].get_diff(
                    run['codebase'],
                    base_revid.encode('utf-8')
                    if base_revid is not None else NULL_REVISION,
                    revid.encode('utf-8'))
                return diff.decode("utf-8", "replace")
        except ClientResponseError as e:
            return "Unable to retrieve diff; error %d" % e.status
        except ClientConnectorError as e:
            return "Unable to retrieve diff; error %s" % e

    async def show_debdiff():
        if not run['build_version']:
            return ""
        if not unchanged_run or not unchanged_run['build_version']:
            return ""
        try:
            with span.new_child('archive-diff'):
                debdiff, content_type = await get_archive_diff(
                    client,
                    differ_url,
                    run['id'],
                    unchanged_run['id'],
                    kind="debdiff",
                    filter_boring=True,
                    accept="text/html",
                )
                return debdiff.decode("utf-8", "replace")
        except BuildDiffUnavailable:
            return ""
        except DebdiffRetrievalError as e:
            return "Error retrieving debdiff: %s" % e

    kwargs: dict[str, Any] = {}
    kwargs.update([(k, v) for (k, v) in package.items() if k != 'name'])

    if run:
        kwargs.update(run)
        env, plain_command = splitout_env(run['command'])
        kwargs['env'] = env
        kwargs['plain_command'] = plain_command
    else:
        env = {}

    campaign = get_campaign_config(config, suite)

    kwargs.update({
        "package": package['name'],
        "reviews": reviews,
        "unchanged_run": unchanged_run,
        "merge_proposals": merge_proposals,
        "run_id": run_id,
        "suite": suite,
        "campaign": campaign,
        "show_diff": show_diff,
        "show_debdiff": show_debdiff,
        "previous_runs": previous_runs,
        "run": run,
        "candidate_context": candidate_context,
        "candidate_success_chance": candidate_success_chance,
        "candidate_value": candidate_value,
        "queue_position": queue_position,
        "queue_wait_time": queue_wait_time,
        "changelog_policy": env.get('DEB_UPDATE_CHANGELOG', 'auto'),
        "config": config,
    })
    return kwargs
