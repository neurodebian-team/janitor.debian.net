from janitor.site.common import html_template

SUITE = "watch-fixes"


@html_template("watch-fixes/start.html")
async def handle_watch_fixes_start(request):
    return {}


def register_watch_fixes_endpoints(router):
    router.add_get("/watch-fixes/", handle_watch_fixes_start, name="watch-fixes-start")
