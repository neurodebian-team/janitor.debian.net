import asyncpg
from collections.abc import Callable
from typing import Any

from breezy.revision import NULL_REVISION

import aiozipkin
from aiohttp import (
    ClientConnectorError, ClientResponseError, web, ClientOSError,
    ContentTypeError)
from aiohttp_apispec import docs
from janitor import CAMPAIGN_REGEX, state
from janitor.config import get_campaign_config
from janitor.site import (
    is_qa_reviewer, get_archive_diff, DebdiffRetrievalError,
    BuildDiffUnavailable, check_logged_in, highlight_diff)
from janitor.site.common import (get_last_unabsorbed_run,
                                 render_template_for_request)
import mimeparse
from yarl import URL

routes = web.RouteTableDef()


async def find_vcs_info(db, role, run_id=None, package=None, campaign=None):
    async with db.acquire() as conn:
        if run_id is None:
            return await conn.fetchrow(
                'SELECT id, codebase, vcs_type, new_result_branch.base_revision, '
                'new_result_branch.revision FROM last_unabsorbed_runs '
                'LEFT JOIN new_result_branch ON '
                'new_result_branch.run_id = last_unabsorbed_runs.id '
                'LEFT JOIN package ON package.codebase = last_unabsorbed_runs.codebase '
                'WHERE package.name = $1 AND suite = $2 AND role = $3',
                package, campaign, role)
        else:
            return await conn.fetchrow(
                'SELECT id, codebase, vcs_type, '
                'new_result_branch.base_revision AS base_revision, '
                'new_result_branch.revision AS revision FROM run '
                'LEFT JOIN new_result_branch ON new_result_branch.run_id = run.id '
                'WHERE run.id = $1 AND role = $2', run_id, role)


@docs()
@routes.get("/{campaign}/pkg/{package}/diff", name="package-diff")
@routes.get("/pkg/{package}/run/{run_id}/diff", name="package-run-diff")
async def handle_diff(request):
    role = request.query.get("role", "main")
    run_id = request.match_info.get('run_id')
    package = request.match_info.get("package")
    campaign = request.match_info.get("campaign")
    run = await find_vcs_info(request.app['pool'], role, run_id, package, campaign)
    span = aiozipkin.request_span(request)
    if run is None:
        if run_id:
            raise web.HTTPNotFound(text="no run %s" % (run_id, ))
        else:
            raise web.HTTPNotFound(
                text="no unabsorbed run for %s/%s" % (package, campaign))

    if run['vcs_type'] is None:
        return web.Response(
            status=404,
            text="Not in a VCS")

    if run['revision'] is None:
        return web.Response(text='Branch deleted')
    try:
        try:
            with span.new_child('vcs-diff'):
                diff = await request.app['vcs_managers'][run['vcs_type']].get_diff(
                    run['codebase'], run['base_revision'].encode('utf-8')
                    if run['base_revision'] else NULL_REVISION,
                    run['revision'].encode('utf-8'))
        except ClientResponseError as e:
            return web.Response(status=e.status, text="Unable to retrieve diff")
        except NotImplementedError as e:
            raise web.HTTPBadRequest(text="unsupported vcs %s" % run['vcs_type']) from e

        best_match = mimeparse.best_match(
            ['text/x-diff', 'text/plain', 'text/html'],
            request.headers.get('Accept', '*/*'))
        if best_match in ("text/x-diff", "text/plain"):
            return web.Response(
                body=diff,
                content_type="text/x-diff",
                headers={"Vary": "Accept"})
        elif best_match == "text/html":
            return web.Response(
                text=highlight_diff(diff.decode("utf-8", "replace")),
                content_type="text/html",
                headers={"Vary": "Accept"})
        raise web.HTTPNotAcceptable(
            text="Acceptable content types: text/html, text/x-diff"
        )
    except ContentTypeError as e:
        return web.Response(text="publisher returned error %d" % e.code, status=400)
    except ClientConnectorError:
        return web.Response(text="unable to contact publisher", status=502)
    except ClientOSError:
        return web.Response(text="unable to contact publisher - oserror", status=502)


@docs()
@routes.get(
    "/pkg/{package}/run/{run_id}/{kind:debdiff|diffoscope}",
    name="package-run-archive-diff")
async def handle_archive_diff(request):
    run_id = request.match_info["run_id"]
    kind = request.match_info["kind"]
    span = aiozipkin.request_span(request)
    with span.new_child('sql:get-run'):
        async with request.app['pool'].acquire() as conn:
            run = await conn.fetchrow(
                'SELECT id, codebase, suite AS campaign, '
                'main_branch_revision, result_code FROM run WHERE id = $1',
                run_id)
            if run is None:
                raise web.HTTPNotFound(text="No such run: %s" % run_id)
            unchanged_run_id = await conn.fetchval(
                "SELECT id FROM run WHERE "
                "codebase = $1 AND revision = $2 AND result_code = 'success' "
                "ORDER BY finish_time DESC LIMIT 1",
                run['codebase'], run['main_branch_revision'])
            if unchanged_run_id is None:
                return web.json_response(
                    {
                        "reason": "No matching unchanged build for %s" % run_id,
                        "run_id": [run['id']],
                        "unavailable_run_id": None,
                        "campaign": run['campaign'],
                    },
                    status=404,
                )

    if run['result_code'] != 'success':
        raise web.HTTPNotFound(text="Build %s has no artifacts" % run_id)

    filter_boring = "filter_boring" in request.query

    try:
        with span.new_child('archive-diff'):
            debdiff, content_type = await get_archive_diff(
                request.app['http_client_session'],
                request.app['differ_url'],
                run_id,
                unchanged_run_id,
                kind=kind,
                filter_boring=filter_boring,
                accept=request.headers.get("ACCEPT", "*/*"),
            )
    except BuildDiffUnavailable as e:
        return web.json_response(
            {
                "reason": "debdiff not calculated yet (run: %s, unchanged run: %s)"
                % (run['id'], unchanged_run_id),
                "run_id": [unchanged_run_id, run['id']],
                "unavailable_run_id": e.unavailable_run_id,
                "campaign": run['campaign'],
            },
            status=404,
        )
    except DebdiffRetrievalError as e:
        return web.json_response(
            {
                "reason": "unable to contact differ for binary diff: %r" % e,
                "inner_reason": e.args[0],
            },
            status=503,
        )

    return web.Response(
        body=debdiff,
        content_type=content_type,
        headers={"Vary": "Accept"},
    )


@docs()
@routes.get("/pkg/{package}/run/{run_id}", name="package-run")
@routes.get("/run/{run_id}", name="run")
async def handle_run(request):
    run_id = request.match_info.get("run_id")
    async with request.app['pool'].acquire() as conn:
        run = await conn.fetchrow(
            'SELECT id, start_time, finish_time, command, description, '
            'build_info, result_code, vcs_type, branch_url, '
            'debian_build.version as build_version, '
            'debian_build.distribution as build_distribution, '
            'package.name AS package '
            'FROM run '
            'LEFT JOIN package ON package.codebase = run.codebase '
            'LEFT JOIN debian_build ON debian_build.run_id = run.id '
            'WHERE id = $1', run_id)
    if run is None:
        raise web.HTTPNotFound(text='no such run')
    if run['build_version']:
        build_info = {
            "version": str(run['build_version']),
            "distribution": run['build_distribution'],
        }
    else:
        build_info = None
    return web.json_response({
        "run_id": run['id'],
        "start_time": run['start_time'].isoformat(),
        "finish_time": run['finish_time'].isoformat(),
        "command": run['command'],
        "description": run['description'],
        "package": run['package'],
        "build_info": build_info,
        "result_code": run['result_code'],
        "vcs_type": run['vcs_type'],
        "branch_url": run['branch_url'],
    }, headers={"Cache-Control": "max-age=600"})


@docs()
@routes.get("/pkgnames", name="package-names")
async def handle_packagename_list(request):
    response_obj = []
    async with request.app['pool'].acquire() as conn:
        # TODO(jelmer): Query specific distribution
        for row in await conn.fetch('SELECT name FROM package WHERE NOT removed'):
            response_obj.append(row['name'])
    return web.json_response(response_obj, headers={"Cache-Control": "max-age=600"})


@docs()
@routes.get("/policy", name="policy")
async def handle_global_policy(request):
    return web.Response(
        content_type="text/protobuf",
        text=str(request.app['policy']),
        headers={"Cache-Control": "max-age=60"},
    )


@routes.post("/resolve-apt", name="resolve-apt")
async def handle_resolve_apt(request):
    dep_server_url = request.app['dep_server_url']
    if dep_server_url is None:
        return web.HTTPNotFound(text='dep server not configured')
    url = URL(dep_server_url) / "resolve-apt"
    try:
        async with request.app['http_client_session'].post(
                url, json=await request.json()) as resp:
            return web.json_response(await resp.json(), status=resp.status)
    except ClientConnectorError:
        return web.Response(text="unable to contact publisher", status=503)


@routes.get("/resolve-apt/{distribution}/{target}", name="resolve-apt-simple")
async def handle_resolve_apt_simple(request):
    dep_server_url = request.app['dep_server_url']
    if dep_server_url is None:
        return web.HTTPNotFound(text='dep server not configured')
    url = (URL(dep_server_url) / "resolve-apt"
           / request.match_info["distribution"] / request.match_info["target"])
    try:
        async with request.app['http_client_session'].get(url) as resp:
            return web.json_response(await resp.json(), status=resp.status)
    except ClientConnectorError:
        return web.Response(text="unable to contact publisher", status=503)


@routes.post(
    "/{campaign:" + CAMPAIGN_REGEX + "}/pkg/{package}/schedule",
    name="package-schedule")
async def handle_schedule(request):
    package_name = request.match_info["package"]
    async with request.app['pool'].acquire() as conn:
        package = await conn.fetchrow(
            'SELECT codebase FROM package WHERE name = $1', package_name)
        if package is None:
            raise web.HTTPNotFound(text=f'no such package: {package_name}') from None
        codebase = package['codebase']

    campaign = request.match_info["campaign"]
    post = await request.post()
    offset = post.get("offset")
    try:
        refresh = bool(int(post.get("refresh", "0")))
    except ValueError:
        return web.json_response({"error": "invalid boolean for refresh"}, status=400)
    if request['user']:
        requestor = request['user']["email"]
    else:
        requestor = "user from web UI"
    schedule_url = URL(request.app['runner_url']) / "schedule"
    queue_position_url = URL(request.app['runner_url']) / "queue" / "position"
    async with request.app['http_client_session'].post(schedule_url, json={
        'codebase': codebase,
        'package': package_name,
        'campaign': campaign,
        'refresh': refresh,
        'offset': offset,
        'requestor': requestor,
        'bucket': "manual"
    }, raise_for_status=True) as resp:
        ret = await resp.json()
    try:
        async with request.app['http_client_session'].get(queue_position_url, params={
                'campaign': campaign,
                'codebase': codebase}, raise_for_status=True) as resp:
            queue_position = await resp.json()
    except ClientResponseError as e:
        if e.status == 400:
            raise web.HTTPBadRequest(text=e.message) from e
        raise
    return web.json_response({
        "codebase": ret['codebase'],
        "campaign": ret['campaign'],
        "bucket": ret['bucket'],
        "offset": ret['offset'],
        "package": package_name,
        "estimated_duration_seconds": ret['estimated_duration_seconds'],
        "queue_position": queue_position['position'],
        "queue_wait_time": queue_position['wait_time'],
    })


@docs()
@routes.post(
        "/pkg/{package}/run/{run_id}/+followup", name="package-run-publish-status")
async def handle_run_publish_status(request):
    run_id = request.match_info['run_id']
    url = URL(request.app['followup_url']) / "process" / run_id
    try:
        async with request.app['http_client_session'].post(
                url, raise_for_status=True, json={}) as resp:
            json = await resp.json()
    except ContentTypeError as e:
        return web.json_response(
            {"reason": f"followup returned error {e}"}, status=400
        )
    except ClientConnectorError:
        return web.json_response(
            {"reason": "unable to contact followup"}, status=400)
    return web.json_response(
        {"publish_status": json['publish_status'], "actions": json["actions"]})


@docs()
@routes.post("/pkg/{package}/run/{run_id}", name="package-run-update")
async def handle_run_post(request):
    from janitor.review import store_review
    run_id = request.match_info["run_id"]
    check_logged_in(request)

    async with request.app['pool'].acquire() as conn:
        span = aiozipkin.request_span(request)
        post = await request.post()
        verdict = post.get("verdict")
        review_comment = post.get("review-comment")
        if verdict:
            verdict = verdict.lower()
            with span.new_child('sql:update-run'):
                try:
                    user = request['user']['email']
                except KeyError:
                    user = request['user']['name']
                await store_review(
                    conn, request.app['http_client_session'],
                    request.app['runner_url'],
                    run_id, verdict=verdict, comment=review_comment,
                    reviewer=user, is_qa_reviewer=is_qa_reviewer(request))
        return web.json_response(
            {"verdict": verdict, "review-comment": review_comment})


@docs()
@routes.post("/vcswatch", name="vcswatch")
async def handle_vcswatch(request):
    json = await request.json()
    # Keys set:
    # * old-hash
    # * new-hash
    # * package
    # * status
    # * branch
    # * url

    package = json['package']
    url = json['url']

    rescheduled: list[str] = []
    candidate_unavailable: list[str] = []
    requestor = "vcswatch notification"
    async with request.app['pool'].acquire() as conn:
        codebase = await conn.fetchval(
            'SELECT codebase FROM package WHERE name = $1 AND distribution = $2',
            package, 'sid')
        if await state.has_cotenants(conn, codebase, url):
            # TODO(jelmer): Have vcswatch pass along path, and only
            # notify for changes under path
            return web.json_response({
                'rescheduled': [],
                'policy-unavailable': [],
                'ignored': 'package is in repository with cotenants',
            }, status=200)
        for suite in await state.iter_publishable_suites(conn, codebase):
            if suite not in ('lintian-fixes', 'unchanged',
                             'fresh-releases', 'fresh-snapshots',
                             'scrub-obsolete', 'debianize'):
                continue
            schedule_url = URL(request.app['runner_url']) / "schedule"
            async with request.app['http_client_session'].post(schedule_url, json={
                'codebase': codebase,
                'campaign': suite,
                'requestor': requestor,
                'bucket': "hook"
            }, raise_for_status=True) as resp:
                await resp.json()
            rescheduled.append(suite)

    return web.json_response({
        'rescheduled': rescheduled,
        'candidate-unavailable': candidate_unavailable,
    }, status=200)


@docs()
@routes.post("/{campaign}/pkg/{package}/publish", name="package-publish")
async def handle_publish(request):
    publisher_url = request.app['publisher_url']
    package = request.match_info["package"]
    campaign = request.match_info["campaign"]
    post = await request.post()
    mode = post.get("mode")
    if mode not in (None, "push-derived", "push", "propose", "attempt-push"):
        return web.json_response({"error": "Invalid mode", "mode": mode}, status=400)
    async with request.app['pool'].acquire() as conn:
        campaign_config = get_campaign_config(request.app['config'], campaign)
        codebase = await conn.fetchval(
            'SELECT codebase FROM package WHERE name = $1 AND distribution = $2',
            package, campaign_config.debian_build.base_distribution)
    url = URL(publisher_url) / campaign / codebase / "publish"
    if request['user']:
        requestor = request['user']["email"]
    else:
        requestor = "user from web UI"
    data = {"requestor": requestor}
    if mode:
        data["mode"] = mode
    try:
        async with request.app['http_client_session'].post(url, data=data) as resp:
            if resp.status in (200, 202):
                return web.json_response(await resp.json(), status=resp.status)
            else:
                return web.json_response(await resp.json(), status=400)
    except ContentTypeError as e:
        return web.json_response(
            {"reason": "publisher returned error %s" % e}, status=400
        )
    except ClientConnectorError:
        return web.json_response({"reason": "unable to contact publisher"}, status=400)


@routes.get("/pkg/{package}/merge-proposals", name="package-merge-proposals")
async def handle_package_merge_proposal_list(request):
    package = request.match_info["package"]
    async with request.app['pool'].acquire() as conn:
        codebases = await conn.fetchval(
            'SELECT DISTINCT codebase FROM package WHERE name = $1',
            package)

    ret = {}
    for codebase in codebases:
        url = URL(request.app['publisher_url']) / "c" / codebase / "merge-proposals"
        async with request.app['http_client_session'].get(
                url, raise_for_status=True) as resp:
            ret.update(resp.json())
    return web.json_response(ret)


@docs()
@routes.get("/{campaign:fresh-releases|fresh-snapshots}/published-packages",
            name="published-packages")
async def handle_published_packages(request):
    from .new_upstream import get_published_packages
    campaign = request.match_info["campaign"]
    async with request.app['pool'].acquire() as conn:
        response_obj = []
        for (
            package,
            build_version,
            archive_version,
        ) in await get_published_packages(conn, campaign):
            response_obj.append(
                {
                    "package": package,
                    "build_version": build_version,
                    "archive_version": archive_version,
                }
            )
    return web.json_response(response_obj)


CAMPAIGN_RESULT_TO_JSON: dict[str, Callable] = {}


async def _get_run_json(db, package_name: str, campaign: str) -> Any:
    async with db.acquire() as conn:
        package = await conn.fetchrow("""\
SELECT package.*, named_publish_policy.per_branch_policy AS publish_policy,
candidate.command AS intended_command
FROM package
LEFT JOIN candidate ON package.codebase = candidate.codebase AND candidate.suite = $2
LEFT JOIN named_publish_policy ON named_publish_policy.name = candidate.publish_policy
WHERE package.name = $1 AND package.distribution = 'sid'""", package_name, campaign)
        if package is None:
            raise web.HTTPNotFound(text='no such package: %s' % package)
        run = await get_last_unabsorbed_run(conn, package_name, campaign)
        if run is None:
            raise web.HTTPNotFound(
                text=f'no relevant runs for {package_name}/{campaign}')

    per_branch_policy = {
        p[0]: {
            'mode': p[1],
            'max_frequency_days': p[2],
        } for p in package['publish_policy']}

    ret = {
        'package': {
            'name': package['name'],
            'maintainer_email': package['maintainer_email'],
            'uploader_emails': package['uploader_emails'],
        },
        'run_id': run['id'],
        'review_status': run['review_status'],
        'intended-command': package['intended_command'],
        'command': run['command'],
        'result_code': run['result_code'],
        'description': run['description'],
        'start_time': run['start_time'].isoformat(),
        'finish_time': run['finish_time'].isoformat(),
        'change_set': run['change_set'],
        'build': {
            'version': str(run['build_version']),
            'distribution': run['build_distribution'],
            'lintian_result': run['lintian_result'],
            'binary_packages': run['binary_packages'],
        } if run['result_code'] == 'success' else None,
        'branches': {
            b[0]: {
                'remote_name': b[1],
                'publish_policy': per_branch_policy.get(b[0]),
            }
            for b in run['result_branches']
        },
        'tags': {tag[0]: tag[1] for tag in (run['result_tags'] or [])}
    }

    if run['result_code'] not in ('success', 'nothing-to-do', 'nothing-new-to-do'):
        ret['failure'] = {
            'stage': run['failure_stage'],
            'transient': run['failure_transient'],  # Should always be false..
        }

    result_to_json = CAMPAIGN_RESULT_TO_JSON.get(campaign)
    if result_to_json is None:
        ret['_raw_result'] = run['result']
    else:
        result_to_json(run['result'], ret)

    return ret


@docs()
@routes.get(
    '/{campaign:lintian-fixes|multiarch-fixes|fresh-releases|fresh-snapshots|'
    'scrub-obsolete}'
    '/pkg/{pkg}', name='campaign-pkg')
async def handle_custom_pkg(request):
    ret = await _get_run_json(
        request.app['pool'], request.match_info['pkg'], request.match_info['campaign'])
    return web.json_response(ret)


@routes.post("/webhook", name="webhook")
@routes.get("/webhook", name="webhook-help")
async def handle_webhook(request):
    if request.headers.get("Content-Type") != "application/json":
        text = await render_template_for_request("webhook.html", request, {})
        return web.Response(
            content_type="text/html",
            text=text,
        )
    from debian_janitor.site.__main__ import process_webhook
    return await process_webhook(request, request.app['pool'])


@routes.get('/', name='redirect-docs')
async def redirect_docs(req):
    raise web.HTTPFound(location='docs')


@docs()
@routes.get("/{campaign:" + CAMPAIGN_REGEX + "}/report", name="report")
async def handle_report(request):
    campaign = request.match_info["campaign"]
    report = {}
    merge_proposal = {}
    async with request.app['pool'].acquire() as conn:
        for package, url in await conn.fetch("""
SELECT
    DISTINCT ON (merge_proposal.url)
    merge_proposal.package, merge_proposal.url
FROM
    merge_proposal
LEFT JOIN run
ON merge_proposal.revision = run.revision AND run.result_code = 'success'
AND status = 'open'
WHERE run.suite = $1
""", campaign):
            merge_proposal[package] = url
        query = """
SELECT DISTINCT ON (package)
  result_code,
  start_time,
  package.name AS package,
  result
FROM
  last_unabsorbed_runs
LEFT JOIN package ON last_unabsorbed_runs.codebase = package.codebase
WHERE suite = $1
ORDER BY package.name, suite, start_time DESC
"""
        for record in await conn.fetch(query, campaign):
            if record['result_code'] not in ("success", "nothing-to-do"):
                continue
            data = {
                "timestamp": record['start_time'].isoformat(),
                "result": record['result'],
            }
            if record['package'] in merge_proposal:
                data["merge-proposal"] = merge_proposal[record['package']]
            report[record['package']] = data
    return web.json_response(report)


def format_madison_lines(ret):
    if not ret:
        return []

    def pad(column, length):
        return column + ' ' * (length - len(column))

    collengths = [max([len(row[i]) for row in ret]) for i in range(len(ret[0]))]

    for row in ret:
        yield ' | '.join(
            [pad(col, collengths[i]) for i, col in enumerate(row)]) + '\n'


@routes.get("/madison", name="madison")
async def handle_madison(request):
    package = request.query['package']
    text = request.query.get('text', 'off')
    if text != 'on':
        raise web.HTTPBadRequest(text='text=off not yet supported')
    binary_type = request.query.get('b')
    if binary_type:
        raise web.HTTPBadRequest(text='binary_type not yet supported')
    component = request.query.get('c')
    if component:
        raise web.HTTPBadRequest(text='component not yet supported')
    greaterorequal: bool = ('g' in request.query)
    if greaterorequal:
        raise web.HTTPBadRequest(text='greaterthan not yet supported')
    greaterthan: bool = ('G' in request.query)
    if greaterthan:
        raise web.HTTPBadRequest(text='greaterthan not yet supported')
    regex = request.query.get('r')
    if regex:
        raise web.HTTPBadRequest(text='regex not yet supported')
    suite = request.query.get('s')
    source_and_binary = request.query.get('S')
    if source_and_binary:
        raise web.HTTPBadRequest(text='source_and_binary not yet supported')
    time = request.query.get('t')
    if time:
        raise web.HTTPBadRequest(text='time not yet supported')
    arch = request.query.get('a')
    if arch:
        raise web.HTTPBadRequest(text='arch not yet supported')

    args = [package]
    query = (
        "SELECT DISTINCT ON (distribution) source, version, distribution "
        "FROM debian_build "
        "WHERE ($1 = ANY(binary_packages) OR source = $1) AND "
        "distribution != 'control' ORDER BY distribution, version DESC")

    if suite:
        args.append(suite)
        query += ' OR distribution = $%d' % (len(args))

    ret = []
    async with request.app['pool'].acquire() as conn:
        for row in await conn.fetch(query, *args):
            ret.append((row['source'], str(row['version']), row['distribution'], ''))

    return web.Response(text=''.join(format_madison_lines(ret)))


async def fetch_missing_upstream_branch_urls(conn: asyncpg.Connection):
    query = """\
select
  package.name AS package,
  package.archive_version AS archive_version
from
  last_runs
inner join package on last_runs.codebase = package.codebase
left outer join upstream on upstream.name = package.name
where
  result_code = 'upstream-branch-unknown' and
  upstream.upstream_branch_url is null
order by package.name asc
"""
    return await conn.fetch(query)


@routes.get("/missing-upstream-urls", name="missing-upstream-urls")
async def handle_missing_upstream_urls(request):
    async with request.app['pool'].acquire() as conn:
        return web.json_response([
            {'package': row['package'], 'archive-version': row['archive-version']}
            for row in await fetch_missing_upstream_branch_urls(conn)])
