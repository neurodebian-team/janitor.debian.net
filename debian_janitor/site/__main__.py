#!/usr/bin/python
# Copyright (C) 2019 Jelmer Vernooij <jelmer@jelmer.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

"""Serve the janitor site."""

import asyncio
import functools
import json
import logging
import os
import re
import time
import warnings
from datetime import date, datetime
from itertools import chain
from typing import Any

import aiohttp_jinja2
import aiozipkin
import asyncpg
import gpg
import janitor.site as base_site
import uvloop
from aiohttp import (ClientConnectorError, ClientResponseError, ClientSession,
                     web)
from aiohttp.web_middlewares import normalize_path_middleware
from aiohttp.web_urldispatcher import URL
from aiohttp_apispec import setup_aiohttp_apispec
from aiohttp_openmetrics import metrics, metrics_middleware
from debian.changelog import Version
from janitor import state
from janitor.config import get_campaign_config
from janitor.schedule import do_schedule
from janitor.site import TEMPLATE_ENV as COMMON_TEMPLATE_ENV
from janitor.site import template_loader as common_template_loader
from janitor.site.common import html_template, render_template_for_request
from janitor.site.openid import setup_openid
from janitor.site.pkg import (BuildDiffUnavailable, DebdiffRetrievalError,
                              get_archive_diff, get_unchanged_run)
from janitor.site.pubsub import Topic, pubsub_handler
from janitor.site.setup import (setup_artifact_manager, setup_gpg,
                                setup_logfile_manager, setup_postgres,
                                setup_redis)
from janitor.site.webhook import is_webhook_request, parse_webhook
from janitor.vcs import get_vcs_managers_from_config
from jinja2 import ChoiceLoader, PackageLoader, select_autoescape

from .api import routes as api_routes

routes = web.RouteTableDef()


def create_background_task(fn, title):
    loop = asyncio.get_event_loop()
    task = loop.create_task(fn)

    def log_result(future):
        try:
            future.result()
        except asyncio.CancelledError:
            logging.debug('%s cancelled', title)
        except BaseException:   # noqa: PIE786
            logging.exception('%s failed', title)
        else:
            logging.debug('%s succeeded', title)
    task.add_done_callback(log_result)
    return task


async def get_credentials(session, publisher_url):
    url = URL(publisher_url) / "credentials"
    async with session.get(url=url) as resp:
        if resp.status != 200:
            raise Exception("unexpected response")
        return await resp.json()


async def handle_simple(templatename, request):
    vs: dict[str, Any] = {}
    return web.Response(
        content_type="text/html",
        text=await render_template_for_request(templatename, request, vs),
        headers={"Cache-Control": "max-age=3600"})


@html_template("generic/start.html")
async def handle_generic_start(request):
    return {"suite": request.match_info["campaign"]}


@html_template(
    "generic/candidates.html",
    headers={"Cache-Control": "max-age=3600", "Vary": "Cookie"})
async def handle_generic_candidates(request):
    from janitor.site.common import generate_candidates

    return await generate_candidates(
        request.app.database, suite=request.match_info["campaign"]
    )


@html_template(
    "merge-proposals.html",
    headers={"Cache-Control": "max-age=60", "Vary": "Cookie"})
async def handle_merge_proposals(request):
    from janitor.site.merge_proposals import write_merge_proposals

    campaign = request.match_info.get("campaign")
    return await write_merge_proposals(request.app.database, campaign)


@html_template(
    "merge-proposal.html",
    headers={"Cache-Control": "max-age=60", "Vary": "Cookie"})
async def handle_merge_proposal(request):
    from janitor.site.merge_proposals import write_merge_proposal

    url = request.query["url"]
    return await write_merge_proposal(request.app.database, url)


async def handle_apt_repo(request):
    campaign = request.match_info["campaign"]

    async with request.app.database.acquire() as conn:
        packages = await conn.fetch("""
select distinct on (debian_build.source) debian_build.source, debian_build.version,
archive_version from debian_build
where debian_build.distribution = $1 and not package.removed
order by debian_build.source, debian_build.version desc
""", campaign)

        vs = {
            "packages": packages,
            "suite": campaign,
            "campaign_config": get_campaign_config(request.app['config'], campaign),
        }
        text = await render_template_for_request(campaign + ".html", request, vs)
        return web.Response(
            content_type="text/html",
            text=text,
            headers={"Cache-Control": "max-age=60", "Vary": "Cookie"},
        )


@html_template(
    "credentials.html",
    headers={"Cache-Control": "max-age=10", "Vary": "Cookie"})
async def handle_credentials(request):
    try:
        credentials = await get_credentials(
            request.app['http_client_session'], request.app['publisher_url']
        )
    except ClientConnectorError:
        return web.Response(status=500, text='Unable to retrieve credentials')
    pgp_fprs = []
    for keydata in credentials["pgp_keys"]:
        result = request.app['gpg'].key_import(keydata.encode("utf-8"))
        pgp_fprs.extend([i.fpr for i in result.imports])

    pgp_validity = {
        gpg.constants.VALIDITY_FULL: "full",
        gpg.constants.VALIDITY_MARGINAL: "marginal",
        gpg.constants.VALIDITY_NEVER: "never",
        gpg.constants.VALIDITY_ULTIMATE: "ultimate",
        gpg.constants.VALIDITY_UNDEFINED: "undefined",
        gpg.constants.VALIDITY_UNKNOWN: "unknown",
    }

    return {
        "format_pgp_date": lambda ts: time.strftime("%Y-%m-%d", time.localtime(ts)),
        "pgp_validity": pgp_validity.get,
        "pgp_algo": gpg.core.pubkey_algo_name,
        "ssh_keys": credentials["ssh_keys"],
        "pgp_keys": request.app['gpg'].keylist("\0".join(pgp_fprs)),
        "hosting": credentials["hosting"],
    }


async def handle_ssh_keys(request):
    credentials = await get_credentials(
        request.app['http_client_session'], request.app['publisher_url']
    )
    return web.Response(
        text="\n".join(credentials["ssh_keys"]), content_type="text/plain"
    )


async def handle_pgp_keys(request):
    credentials = await get_credentials(
        request.app['http_client_session'], request.app['publisher_url']
    )
    armored = request.match_info["extension"] == ".asc"
    if armored:
        return web.Response(
            text="\n".join(credentials["pgp_keys"]),
            content_type="application/pgp-keys",
        )
    else:
        fprs = []
        for keydata in credentials["pgp_keys"]:
            result = request.app['gpg'].key_import(keydata.encode("utf-8"))
            fprs.extend([i.fpr for i in result.imports])
        return web.Response(
            body=request.app['gpg'].key_export_minimal("\0".join(fprs)),
            content_type="application/pgp-keys",
        )


async def handle_archive_keyring(request):
    url = URL(request.app['archiver_url']) / "pgp_keys"
    session = request.app['http_client_session']
    async with session.get(url=url, raise_for_status=True) as resp:
        pgp_keys = await resp.json()
    armored = request.match_info["extension"] == ".asc"
    if armored:
        return web.Response(
            text="\n".join(pgp_keys),
            content_type="application/pgp-keys",
        )
    else:
        fprs = []
        for keydata in pgp_keys:
            result = request.app['gpg'].key_import(keydata.encode("utf-8"))
            fprs.extend([i.fpr for i in result.imports])
        return web.Response(
            body=request.app['gpg'].key_export_minimal("\0".join(fprs)),
            content_type="application/pgp-keys",
        )


@html_template(
    "maintainer-overview.html",
    headers={"Cache-Control": "max-age=60", "Vary": "Cookie"})
async def handle_maintainer_overview(request):
    from .stats import write_maintainer_overview

    span = aiozipkin.request_span(request)
    async with request.app.database.acquire() as conn:
        return await write_maintainer_overview(
            conn,
            request.app['http_client_session'],
            request.app['publisher_url'],
            request.match_info["maintainer"],
            span,
        )


async def handle_static_file(path, request):
    return web.FileResponse(path)


@html_template(
    "package-name-list.html",
    headers={"Cache-Control": "max-age=600", "Vary": "Cookie"})
async def handle_pkg_list(request):
    # TODO(jelmer): The javascript plugin thingy should just redirect to
    # the right URL, not rely on query parameters here.
    pkg = request.query.get("package")
    if pkg:
        async with request.app.database.acquire() as conn:
            if not await conn.fetchrow('SELECT 1 FROM package WHERE name = $1', pkg):
                raise web.HTTPNotFound(text="No package with name %s" % pkg)
        return web.HTTPFound(pkg)

    async with request.app.database.acquire() as conn:
        packages = [
            row['name']
            for row in await conn.fetch(
                'SELECT name, maintainer_email '
                'FROM package WHERE NOT removed GROUP BY name '
                'ORDER BY name, maintainer_email')]
    return {'packages': packages}


@html_template(
    "by-maintainer-package-list.html",
    headers={"Cache-Control": "max-age=600", "Vary": "Cookie"})
async def handle_maintainer_list(request):
    async with request.app.database.acquire() as conn:
        packages = [
            (row['name'], row['maintainer_email'])
            for row in await conn.fetch(
                'SELECT name, maintainer_email FROM package WHERE NOT removed '
                'GROUP BY name, maintainer_email')]
    by_maintainer: dict[str, list[str]] = {}
    for name, maintainer in packages:
        by_maintainer.setdefault(maintainer, []).append(name)
    return {"by_maintainer": by_maintainer}


@html_template(
    "maintainer-index.html",
    headers={"Cache-Control": "max-age=600", "Vary": "Cookie"})
async def handle_maintainer_index(request):
    if request['user']:
        email = request['user'].get("email")
    else:
        email = request.query.get("email")
    if email and "/" in email:
        raise web.HTTPBadRequest(text="invalid maintainer email")
    if email:
        raise web.HTTPFound(
            request.app.router["maintainer-overview-short"].url_for(
                maintainer=email
            )
        )
    return {}


@html_template(
    "vcs-regressions.html",
    headers={"Cache-Control": "max-age=600", "Vary": "Cookie"})
async def handle_vcs_regressions(request):
    async with request.app.database.acquire() as conn:
        query = """\
select
package.name,
run.suite,
run.id,
run.result_code,
package.vcswatch_status
from
last_runs run left join package on run.codebase = package.codebase
where
result_code in (
'branch-missing',
'branch-unavailable',
'401-unauthorized',
'hosted-on-alioth',
'missing-control-file'
)
and
vcswatch_status in ('old', 'new', 'commits', 'ok')
"""
        return {"regressions": await conn.fetch(query)}


async def handle_result_file(request):
    pkg = request.match_info["pkg"]
    filename = request.match_info["filename"]
    try:
        run_id = request.match_info["run_id"]
    except KeyError:
        campaign = request.match_info["campaign"]
        async with request.app.database.acquire() as conn:
            run_id = await conn.fetchval(
                'SELECT id FROM last_runs '
                'LEFT JOIN package ON last_runs.codebase = package.codebase '
                'WHERE package.name = $1 AND last_runs.suite = $2',
                pkg, campaign)
        if run_id is None:
            raise web.HTTPNotFound(text=f"No runs for {pkg}/{campaign}")
    if not re.match("^[a-z0-9+-\\.]+$", pkg) or len(pkg) < 2:
        raise web.HTTPNotFound(text=f"Invalid package {pkg} for run {run_id}")
    if not re.match("^[a-z0-9-]+$", run_id) or len(run_id) < 5:
        raise web.HTTPNotFound(text=f"Invalid run run id {run_id}")
    if filename.endswith(".log") or re.match(r".*\.log\.[0-9]+", filename):
        if not re.match("^[+a-z0-9\\.]+$", filename) or len(filename) < 3:
            raise web.HTTPNotFound(
                text=f"No log file {filename} for run {run_id}"
            )

        try:
            logfile = await request.app['logfile_manager'].get_log(
                pkg, run_id, filename)
        except FileNotFoundError:
            raise web.HTTPNotFound(
                text=f"No log file {filename} for run {run_id}"
            )
        else:
            with logfile as f:
                text = f.read().decode("utf-8", "replace")
        return web.Response(
            content_type="text/plain",
            text=text,
            headers={"Cache-Control": "max-age=3600", "Vary": "Cookie"},
        )
    else:
        try:
            artifact = await request.app['artifact_manager'].get_artifact(
                run_id, filename
            )
        except FileNotFoundError:
            raise web.HTTPNotFound(
                text=f"No artifact {filename} for run {run_id}")
        with artifact as f:
            return web.Response(
                body=f.read(),
                headers={"Cache-Control": "max-age=3600", "Vary": "Cookie"}
            )


async def generate_ready_list(
    db, suite: str | None, publish_status: str | None = None
):
    async with db.acquire() as conn:
        query = ('SELECT package.name AS package, run.suite, run.id, '
                 'run.command, run.result FROM publish_ready')

        query += ' LEFT JOIN package ON run.codebase = package.name '

        conditions = [
            "EXISTS (SELECT * FROM unnest(unpublished_branches) upb "
            "WHERE mode in "
            "('propose', 'attempt-push', 'push-derived', 'push') "
            "AND NOT EXISTS "
            "(SELECT FROM merge_proposal WHERE revision = upb.revision) "
            ")"]

        args = []
        if suite:
            args.append(suite)
            conditions.append('suite = $%d' % len(args))
        if publish_status:
            args.append(publish_status)
            conditions.append('publish_status = $%d' % len(args))

        query += " WHERE " + " AND ".join(conditions)

        query += " ORDER BY package.name ASC"

        return await conn.fetch(query, *args)


@html_template(
    "ready-list.html",
    headers={"Cache-Control": "max-age=60", "Vary": "Cookie"})
async def handle_ready_proposals(request):
    campaign = request.match_info.get("campaign")
    review_status = request.query.get("review_status")
    runs = await generate_ready_list(request.app.database, campaign, review_status)
    return {"runs": runs, "campaign": campaign}


async def generate_done_list(
        db, campaign: str | None, since: datetime | None = None):

    from janitor.site.pkg import MergeProposalUserUrlResolver

    async with db.acquire() as conn:
        oldest = await conn.fetchval(
            "SELECT MIN(absorbed_at) FROM absorbed_runs WHERE campaign = $1",
            campaign)

        if since:
            orig_runs = await conn.fetch(
                "SELECT *, package.* FROM absorbed_runs "
                "LEFT JOIN package ON package.codebase = absorbed_runs.codebase "
                "WHERE absorbed_at >= $1 AND campaign = $2 "
                "ORDER BY absorbed_at DESC NULLS LAST", since, campaign)
        else:
            orig_runs = await conn.fetch(
                "SELECT *, package.* FROM absorbed_runs "
                "LEFT JOIN package ON package.codebase = absorbed_runs.codebase "
                "WHERE campaign = $1 "
                "ORDER BY absorbed_at DESC NULLS LAST", campaign)

    mp_user_url_resolver = MergeProposalUserUrlResolver()

    runs = []
    for orig_run in orig_runs:
        run = dict(orig_run)
        if not run['merged_by']:
            run['merged_by_url'] = None
        else:
            run['merged_by_url'] = mp_user_url_resolver.resolve(
                run['merge_proposal_url'], run['merged_by'])
        runs.append(run)

    return {
        "oldest": oldest, "runs": runs, "campaign": campaign,
        "since": since}


@html_template(
    "generic/done.html",
    headers={"Cache-Control": "max-age=60", "Vary": "Cookie"})
async def handle_done_proposals(request):
    campaign = request.match_info["campaign"]

    since_str = request.query.get("since")
    if since_str:
        try:
            since = datetime.fromisoformat(since_str)
        except ValueError as e:
            raise web.HTTPBadRequest(text="invalid since") from e
    else:
        # Default to beginning of the month
        since = datetime.fromisoformat('%04d-%02d-01' % (
            date.today().year, date.today().month))

    return await generate_done_list(request.app.database, campaign, since)


@html_template(
    "generic/package.html",
    headers={"Cache-Control": "max-age=600", "Vary": "Cookie"})
async def handle_generic_pkg(request):
    from . import generate_pkg_context

    # TODO(jelmer): Handle Accept: text/diff
    pkg = request.match_info["pkg"]
    run_id = request.match_info.get("run_id")
    return await generate_pkg_context(
        request.app.database,
        request.app['config'],
        request.match_info["campaign"],
        request.app['http_client_session'],
        request.app['differ_url'],
        request.app['vcs_managers'],
        pkg,
        aiozipkin.request_span(request),
        run_id,
    )


@html_template("repo-list.html")
async def handle_repo_list(request):
    vcs = request.match_info["vcs"]
    url = request.app['vcs_managers'][vcs].base_url
    async with request.app['http_client_session'].get(url) as resp:
        return {"vcs": vcs, "repositories": await resp.json()}


async def handle_health(request):
    return web.Response(text='ok')


async def handle_ready(request):
    return web.Response(text='ok')


@html_template("fresh-builds.html", headers={"Cache-Control": "max-age=60"})
async def handle_fresh_builds(request):
    from .new_upstream import get_published_packages
    archive_version = {}
    campaign_version: dict[str, dict[str, Version]] = {}
    sources = set()
    CAMPAIGNS = ["fresh-releases", "fresh-snapshots"]
    url = URL(request.app['archiver_url']) / "last-publish"
    try:
        async with request.app['http_client_session'].get(url) as resp:
            if resp.status == 200:
                last_publish_time = {
                    campaign: datetime.fromisoformat(v)
                    for campaign, v in (await resp.json()).items()
                }
            else:
                last_publish_time = {}
    except ClientConnectorError:
        last_publish_time = {}

    async with request.app.database.acquire() as conn:
        for campaign in CAMPAIGNS:
            for name, jv, av in await get_published_packages(conn, campaign):
                sources.add(name)
                archive_version[name] = av
                campaign_version.setdefault(campaign, {})[name] = jv
        return {
            "base_distribution": get_campaign_config(
                request.app['config'], CAMPAIGNS[0]
            ).debian_build.base_distribution,
            "archive_version": archive_version,
            "suite_version": campaign_version,
            "sources": sources,
            "suites": CAMPAIGNS,
            "last_publish_time": last_publish_time,
        }


async def handle_fresh(request):
    return web.HTTPPermanentRedirect("/fresh-builds")


async def handle_github_app_callback(request):
    json = await request.json()
    if json['action'] == 'request':
        for repository in json['repositories']:  # noqa: B007
            # TODO(jelmer): Install our web hook on this repository
            pass
    else:
        raise web.HTTPBadRequest(text='invalid action %s' % json['action'])
    return web.json_response(status=200)


async def generate_pkg_file(
        db, config, package, merge_proposals, runs, available_suites, span):
    from janitor.site.common import iter_candidates
    kwargs = {
        "package": package['name'],
    }
    kwargs.update([(k, v) for (k, v) in package.items() if k != 'name'])
    kwargs["merge_proposals"] = merge_proposals
    kwargs["runs"] = runs
    kwargs["removed"] = package['removed']
    kwargs["distributions"] = config.distribution
    kwargs["available_suites"] = available_suites
    async with db.acquire() as conn:
        with span.new_child('sql:candidates'):
            kwargs["candidates"] = {
                row['suite']: (row['context'], row['value'], row['success_chance'])
                for row in await iter_candidates(conn, codebases=[package['codebase']])
            }
    return kwargs


@routes.get("/pkg/{pkg}/", name="package-overview")
@html_template("package-overview.html", headers={"Vary": "Cookie"})
async def handle_pkg(request):
    span = aiozipkin.request_span(request)

    package_name = request.match_info["pkg"]
    async with request.app.database.acquire() as conn:
        with span.new_child('sql:package'):
            package = await conn.fetchrow(
                'SELECT name, vcswatch_status, maintainer_email, vcs_type, '
                'vcs_url, branch_url, vcs_browse, removed, codebase FROM package '
                'WHERE name = $1', package_name)
        if package is None:
            raise web.HTTPNotFound(text="No package with name %s" % package_name)
        with span.new_child('sql:merge-proposals'):
            merge_proposals = await conn.fetch("""\
SELECT DISTINCT ON (merge_proposal.url)
merge_proposal.url AS url, merge_proposal.status AS status, run.suite AS suite
FROM
merge_proposal
LEFT JOIN run
ON merge_proposal.revision = run.revision AND run.result_code = 'success'
INNER JOIN package ON run.codebase = package.codebase
WHERE package.name = $1
ORDER BY merge_proposal.url, run.finish_time DESC
""", package['name'])
        with span.new_child('sql:publishable-suites'):
            available_suites = await state.iter_publishable_suites(
                conn, package['codebase'])
    with span.new_child('sql:runs'):
        async with request.app.database.acquire() as conn:
            runs = await conn.fetch(
                "SELECT id, finish_time, result_code, suite FROM run "
                "LEFT JOIN debian_build ON run.id = debian_build.run_id "
                "WHERE debian_build.source = $1 ORDER BY finish_time DESC",
                package['name'])
    return await generate_pkg_file(
        request.app.database, request.app['config'], package, merge_proposals, runs,
        available_suites, span
    )


@html_template("cupboard/package-overview.html", headers={"Vary": "Cookie"})
async def handle_cupboard_pkg(request):
    span = aiozipkin.request_span(request)

    package_name = request.match_info["pkg"]
    async with request.app.database.acquire() as conn:
        with span.new_child('sql:package'):
            package = await conn.fetchrow(
                'SELECT name, vcswatch_status, maintainer_email, vcs_type, '
                'vcs_url, branch_url, vcs_browse, removed, codebase FROM package '
                'WHERE name = $1', package_name)
        if package is None:
            raise web.HTTPNotFound(text="No package with name %s" % package_name)
        with span.new_child('sql:merge-proposals'):
            merge_proposals = await conn.fetch("""\
SELECT DISTINCT ON (merge_proposal.url)
merge_proposal.url AS url, merge_proposal.status AS status, run.suite AS suite
FROM
merge_proposal
LEFT JOIN run
ON merge_proposal.revision = run.revision AND run.result_code = 'success'
INNER JOIN package ON run.codebase = package.codebase
WHERE package.name = $1
ORDER BY merge_proposal.url, run.finish_time DESC
""", package['name'])
        with span.new_child('sql:publishable-suites'):
            available_suites = await state.iter_publishable_suites(
                conn, package['codebase'])
    with span.new_child('sql:runs'):
        async with request.app.database.acquire() as conn:
            runs = await conn.fetch(
                "SELECT id, finish_time, result_code, suite FROM run "
                "LEFT JOIN debian_build ON run.id = debian_build.run_id "
                "WHERE debian_build.souce = $1 ORDER BY finish_time DESC",
                package['name'])
    return await generate_pkg_file(
        request.app.database, request.app['config'], package, merge_proposals, runs,
        available_suites, span
    )


@html_template(
    "maintainer-stats.html",
    headers={"Cache-Control": "max-age=60", "Vary": "Cookie"})
async def handle_cupboard_maintainer_stats(request):
    from .stats import write_maintainer_stats

    async with request.app.database.acquire() as conn:
        return await write_maintainer_stats(
            conn,
            request.app['http_client_session'],
            request.app['publisher_url'])


async def get_package_by_upstream_branch_url(
    conn: asyncpg.Connection, upstream_branch_urls: list[str]
) -> tuple[str, str, str] | None:
    query = """
SELECT
  package.package AS name,
  upstream_branch_urls.url AS upstream_branch_url,
  package.codebase AS codebase
INNER JOIN package ON upstream_branch_urls.package = package.name
FROM
  upstream_branch_urls
WHERE
  upstream_branch_urls.url = ANY($1::text[])
"""
    candidates = []
    for url in upstream_branch_urls:
        candidates.extend([
            url.rstrip('/'),
            url.rstrip('/') + '/',
        ])
    return await conn.fetchrow(query, candidates)


async def process_webhook(request, db):
    rescheduled: dict[str, list[str]] = {}

    urls: list[str] = []
    codebases: dict[str, str] = {}
    async for codebase, branch_url in parse_webhook(request, db):
        urls.append(branch_url)
        if codebase is not None:
            codebases[codebase] = branch_url

    # TODO(jelmer): also create candidates
    async with db.acquire() as conn:
        for codebase, branch_url in codebases.items():
            package = await conn.fetchrow(
                'SELECT name FROM package WHERE codebase = $1', codebase)
            if package:
                requestor = "Push hook for %s" % branch_url
                for suite in await state.iter_publishable_suites(
                        conn, codebase
                ):
                    if suite not in ('control', 'lintian-fixes', 'unchanged',
                                     'fresh-releases', 'fresh-snapshots',
                                     'scrub-obsolete', 'debianize'):
                        continue
                    if suite not in rescheduled.get(package['name'], []):
                        await do_schedule(
                            conn, campaign=suite,
                            requestor=requestor, bucket="hook",
                            codebase=codebase
                        )
                        rescheduled.setdefault(package['name'], []).append(suite)

            package = await get_package_by_upstream_branch_url(conn, [branch_url])
            if package is not None:
                requestor = "Push hook for %s" % package['upstream_branch_url']
                for suite in await state.iter_publishable_suites(
                    conn, package['codebase']
                ):
                    if suite not in ("fresh-releases", "fresh-snapshots"):
                        continue
                    if suite not in rescheduled.get(package['name'], []):
                        await do_schedule(
                            conn, campaign=suite,
                            requestor=requestor,
                            bucket="hook", codebase=package['codebase']
                        )
                        rescheduled.setdefault(package['name'], []).append(suite)

        return web.json_response({"rescheduled": rescheduled, "urls": urls})


def setup_pubsub_forwarder(app):
    app['runner_status'] = None

    async def forward_redis(app, name):
        async def handle_result_message(msg):
            data = json.loads(msg['data'])
            app["topic_notifications"].publish([name, data])
            if name == 'queue':
                app['runner_status'] = data

        async with app['redis'].pubsub(ignore_subscribe_messages=True) as ch:
            await ch.subscribe(name, **{name: handle_result_message})
            await ch.run()

    async def start_pubsub_forwarder(app):
        for name, title in [
            ('publish', 'publisher publish listening'),
            ('merge-proposal', 'merge proposal listening'),
            ('queue', 'queue listening'),
            ('result', 'result listening'),
        ]:
            listener = create_background_task(forward_redis(app, name), title)

            async def stop_listener(listener, app):
                listener.cancel()

            app.on_cleanup.append(functools.partial(stop_listener, listener))

    app.on_startup.append(start_pubsub_forwarder)


async def generate_evaluate(
        db, vcs_managers, http_client_session, differ_url, run_id, span):
    from breezy.revision import NULL_REVISION

    async with db.acquire() as conn:
        run = await conn.fetchrow(
            'SELECT codebase, '
            'array(SELECT row(role, remote_name, base_revision, revision) '
            'FROM new_result_branch WHERE run_id = id) AS result_branches, '
            'vcs_type, main_branch_revision, '
            'finish_time, value, command, suite AS campaign FROM run '
            'WHERE id = $1', run_id)

    async def show_diff(role):
        try:
            (remote_name, base_revid, revid) = state.get_result_branch(
                run['result_branches'], role)
        except KeyError:
            return ""
        external_url = f"/api/run/{run_id}/diff?role={role}"
        if run['vcs_type'] is None:
            return "no vcs known"
        if revid is None:
            return "Branch deleted"
        try:
            with span.new_child('vcs-diff'):
                return (await vcs_managers[run['vcs_type']].get_diff(
                    run['codebase'],
                    base_revid.encode('utf-8') if base_revid else NULL_REVISION,
                    revid.encode('utf-8'))
                ).decode("utf-8", "replace")
        except ClientResponseError as e:
            return f"Unable to retrieve diff; error code {e.status}"
        except NotImplementedError as e:
            return str(e)
        except ClientConnectorError as e:
            return f"Unable to retrieve diff; error {e}"
        except TimeoutError:
            return f"Timeout while retrieving diff; see it at {external_url}"

    async def get_revision_info(role):
        try:
            (remote_name, base_revid, revid) = state.get_result_branch(
                run['result_branches'], role)
        except KeyError:
            return []

        if base_revid == revid:
            return []
        if run['vcs_type'] is None:
            logging.warning("No vcs known for run %s", run_id)
            return []
        if revid is None:
            return []
        old_revid = base_revid.encode('utf-8') if base_revid else NULL_REVISION
        new_revid = revid.encode('utf-8')
        try:
            return await vcs_managers[run['vcs_type']].get_revision_info(
                run['codebase'], old_revid, new_revid)
        except ClientResponseError as e:
            logging.warning(
                "Unable to retrieve commit info; error code %d", e.status)
            return []
        except ClientConnectorError as e:
            logging.warning("Unable to retrieve diff; error %s", e)
            return []
        except TimeoutError:
            logging.warning("Timeout while retrieving commit info")
            return []

    async def show_debdiff():
        with span.new_child("sql:unchanged-run"):
            async with db.acquire() as conn:
                unchanged_run = await get_unchanged_run(
                    conn, run['codebase'], run['main_branch_revision'].encode('utf-8')
                )
        if unchanged_run is None:
            return "<p>No control run</p>"
        try:
            with span.new_child('archive-diff'):
                text, unused_content_type = await get_archive_diff(
                    http_client_session,
                    differ_url,
                    run_id,
                    unchanged_run['id'],
                    kind="debdiff",
                    filter_boring=True,
                    accept="text/html",
                )
                return text.decode("utf-8", "replace")
        except DebdiffRetrievalError as e:
            return f"Unable to retrieve debdiff: {e!r}"
        except BuildDiffUnavailable:
            return "<p>No build diff generated</p>"

    return {
        'run_id': run_id,
        'finish_time': run['finish_time'],
        'campaign': run['campaign'],
        'branches': run['result_branches'],
        'value': run['value'],
        'command': run['command'],
        'show_diff': show_diff,
        'show_debdiff': show_debdiff,
        "get_revision_info": get_revision_info,
    }


@routes.get("/evaluate/{run_id}", name="evaluate")
@html_template("generic/evaluate.html")
async def handle_cupboard_evaluate(request):
    run_id = request.match_info['run_id']
    span = aiozipkin.request_span(request)

    return await generate_evaluate(
        request.app['pool'],
        request.app['vcs_managers'],
        request.app['http_client_session'],
        request.app['differ_url'],
        run_id, span)


async def create_app(
        config, policy_config, *, minified=False,
        external_url=None, debugtoolbar=None,
        runner_url=None, publisher_url=None,
        archiver_url=None, vcs_managers=None,
        differ_url=None, dep_server_url=None, followup_url=None,
        listen_address=None, port=None, redis=None, db=None):
    if minified:
        minified_prefix = ""
    else:
        minified_prefix = "min."

    trailing_slash_redirect = normalize_path_middleware(append_slash=True)
    app = web.Application(middlewares=[trailing_slash_redirect])
    our_loader = PackageLoader("debian_janitor.site", "templates")
    aiohttp_jinja2.setup(
        app, loader=ChoiceLoader([our_loader, common_template_loader]),
        enable_async=True,
        autoescape=select_autoescape(["html", "xml"]),
        )
    jinja_env = aiohttp_jinja2.get_env(app)
    jinja_env.globals['tracker_url'] = (
        lambda pkg: "https://tracker.debian.org/pkg/%s" % pkg)
    jinja_env.globals.update(COMMON_TEMPLATE_ENV)

    assert jinja_env.loader

    private_app = web.Application(middlewares=[trailing_slash_redirect])

    app.middlewares.insert(0, metrics_middleware)
    app.middlewares.append(state.asyncpg_error_middleware)
    private_app.middlewares.insert(0, metrics_middleware)
    private_app.middlewares.append(state.asyncpg_error_middleware)
    metrics_route = private_app.router.add_get("/metrics", metrics, name="metrics")
    private_app.router.add_get("/health", handle_health, name="health")
    private_app.router.add_get("/ready", handle_ready, name="ready")

    app.router.add_routes(routes)
    app["topic_notifications"] = Topic("notifications")
    ws_notifications_route = app.router.add_get(
        "/ws/notifications",
        functools.partial(pubsub_handler, app["topic_notifications"]),  # type: ignore
        name="ws-notifications",
    )

    endpoint = aiozipkin.create_endpoint(
        "debian_janitor.site", ipv4=listen_address, port=port)
    if config.zipkin_address:
        tracer = await aiozipkin.create(
            config.zipkin_address, endpoint, sample_rate=1.0)
    else:
        tracer = await aiozipkin.create_custom(endpoint)
    trace_configs = [aiozipkin.make_trace_config(tracer)]

    aiozipkin.setup(private_app, tracer, skip_routes=[metrics_route])
    aiozipkin.setup(app, tracer, skip_routes=[ws_notifications_route])

    async def persistent_session(app):
        app['http_client_session'] = session = ClientSession(
            trace_configs=trace_configs)
        yield
        await session.close()

    app.cleanup_ctx.append(persistent_session)

    if redis is None:
        setup_redis(app)
    else:
        app['redis'] = redis
    setup_gpg(app)
    setup_pubsub_forwarder(app)

    for path, templatename in [
        ("/", "index"),
        ("/contact", "contact"),
        ("/about", "about"),
        ("/apt", "apt"),
    ]:
        app.router.add_get(
            path,
            functools.partial(handle_simple, templatename + ".html"),
            name=templatename,
        )
    app.router.add_get("/credentials", handle_credentials, name="credentials")
    app.router.add_get("/ssh_keys", handle_ssh_keys, name="ssh-keys")
    app.router.add_get(
        r"/pgp_keys{extension:(\.asc)?}", handle_pgp_keys, name="pgp-keys"
    )
    app.router.add_get(
        r"/archive-keyring{extension:(\.asc|\.gpg)}", handle_archive_keyring,
        name="archive-keyring"
    )
    SUITE_REGEX = "|".join([re.escape(campaign.name) for campaign in config.campaign])
    app.router.add_get(
        "/{campaign:" + SUITE_REGEX + "}/pkg/{pkg}/{filename:[^/]+\\.[^/]+}",
        handle_result_file,
        name="result-file",
    )

    from .lintian_fixes import register_lintian_fixes_endpoints
    register_lintian_fixes_endpoints(app.router)
    from .multiarch_hints import register_multiarch_hints_endpoints
    register_multiarch_hints_endpoints(app.router)
    from .orphan import register_orphan_endpoints
    register_orphan_endpoints(app.router)
    from .mia import register_mia_endpoints
    register_mia_endpoints(app.router)
    from .debianize import register_debianize_endpoints
    register_debianize_endpoints(app.router)
    from .scrub_obsolete import register_scrub_obsolete_endpoints
    register_scrub_obsolete_endpoints(app.router)
    from .new_upstream import register_new_upstream_endpoints
    register_new_upstream_endpoints(app.router)
    from .backports import register_backports_endpoints
    register_backports_endpoints(app.router, "bookworm")
    register_backports_endpoints(app.router, "bullseye")
    from .watch_fixes import register_watch_fixes_endpoints
    register_watch_fixes_endpoints(app.router)
    from .uncommitted import register_uncommitted_endpoints
    register_uncommitted_endpoints(app.router)

    app.router.add_get(
        "/{campaign:%s}/merge-proposals" % SUITE_REGEX,
        handle_merge_proposals,
        name="campaign-merge-proposals",
    )
    app.router.add_get(
        "/{campaign:%s}/merge-proposal" % SUITE_REGEX,
        handle_merge_proposal,
        name="campaign-merge-proposal",
    )
    app.router.add_get(
        "/{campaign:%s}/ready" % SUITE_REGEX,
        handle_ready_proposals, name="campaign-ready")
    app.router.add_get(
        "/{campaign:%s}/done" % SUITE_REGEX, handle_done_proposals,
        name="campaign-done")
    app.router.add_get(
        "/{campaign:%s}/maintainer" % SUITE_REGEX,
        handle_maintainer_list,
        name="campaign-maintainer-list")
    app.router.add_get(
        "/{campaign:%s}/pkg/" % SUITE_REGEX, handle_pkg_list,
        name="campaign-package-list"
    )
    app.router.add_get(
        "/{vcs:git|bzr}/", handle_repo_list, name="repo-list")
    app.router.add_get(
        "/{campaign:unchanged}", handle_apt_repo, name="unchanged-start")
    app.router.add_get(
        "/cupboard/maintainer/{maintainer}",
        handle_maintainer_overview,
        name="cupboard-maintainer-overview",
    )
    app.router.add_get(
        "/maintainer/{maintainer}",
        handle_maintainer_overview,
        name="maintainer-overview",
    )
    app.router.add_get("/m/", handle_maintainer_index, name="maintainer-index-short")
    app.router.add_get(
        "/m/{maintainer}", handle_maintainer_overview, name="maintainer-overview-short"
    )
    app.router.add_get("/cupboard/pkg/", handle_pkg_list, name="package-list")
    from janitor.site.cupboard import (register_cupboard_endpoints,
                                       register_cupboard_link)
    register_cupboard_link("Package by name", "/cupboard/pkg/")
    app['review_instructions_url'] = 'https://wiki.debian.org/Janitor/Reviews'
    register_cupboard_endpoints(
        app, config=config, publisher_url=publisher_url,
        runner_url=runner_url, trace_configs=trace_configs, db=db,
        evaluate_url=app.router['evaluate'].url_for(run_id='RUN_ID'))
    app.router.add_get(
        "/{campaign:" + SUITE_REGEX + "}/pkg/{pkg}/{run_id}/{filename:.+}",
        handle_result_file,
        name="result-file-run",
    )
    app.router.add_get(
        "/{campaign:" + SUITE_REGEX + "}/", handle_generic_start, name="generic-start"
    )
    app.router.add_get(
        "/{campaign:" + SUITE_REGEX + "}/candidates",
        handle_generic_candidates,
        name="generic-candidates",
    )
    app.router.add_get(
        "/{campaign:" + SUITE_REGEX + "}/pkg/{pkg}/",
        handle_generic_pkg,
        name="generic-package",
    )
    app.router.add_get(
        "/{campaign:" + SUITE_REGEX + "}/pkg/{pkg}/{run_id}",
        handle_generic_pkg,
        name="generic-run",
    )
    app.router.add_post(
        "/github/callback",
        handle_github_app_callback)
    # Debian specific
    app.router.add_get(
        "/cupboard/maintainer-stats",
        handle_cupboard_maintainer_stats,
        name="cupboard-maintainer-stats",
    )
    register_cupboard_link("Maintainer Statistics", "/cupboard/maintainer-stats")
    app.router.add_get(
        "/cupboard/vcs-regressions", handle_vcs_regressions, name="vcs-regressions"
    )
    register_cupboard_link("VCS Regressions", "/cupboard/vcs-regressions")
    app.router.add_get(
        "/cupboard/maintainer", handle_maintainer_list, name="maintainer-list"
    )
    app.router.add_get("/cupboard/pkg/{pkg}/", handle_pkg, name="cupboard-package")
    register_cupboard_link("Packages By Maintainer", "/cupboard/maintainer")

    app.router.add_get("/fresh-builds", handle_fresh_builds, name="fresh-builds")
    app.router.add_get("/fresh", handle_fresh, name="fresh")

    base_static_path = os.path.join(os.path.dirname(base_site.__file__), "_static")
    static_path = os.path.join(os.path.dirname(__file__), "_static")
    for entry in chain(os.scandir(base_static_path),
                       os.scandir(static_path)):
        app.router.add_get(
            "/_static/%s" % entry.name,
            functools.partial(handle_static_file, entry.path),
        )
    app.router.add_static(
        "/_static/images/datatables", "/usr/share/javascript/jquery-datatables/images"
    )
    for (name, kind, basepath) in [
        ("chart", "js", "/usr/share/javascript/chart.js/Chart"),
        ("jquery", "js", "/usr/share/javascript/jquery/jquery"),
        (
            "jquery.typeahead",
            "js",
            "/usr/share/javascript/jquery-typeahead/jquery.typeahead",
        ),
        (
            "jquery.datatables",
            "js",
            "/usr/share/javascript/jquery-datatables/jquery.dataTables",
        ),
        ("moment", "js", "/usr/share/javascript/moment/moment"),
    ]:
        path = f"{basepath}.{minified_prefix}{kind}"
        if not os.path.exists(path):
            raise FileNotFoundError(path)
        app.router.add_get(
            f"/_static/{name}.{kind}",
            functools.partial(
                handle_static_file, path
            ),
        )
    from janitor.site.api import create_app as create_api_app

    async def handle_post_root(request):
        if is_webhook_request(request):
            return await process_webhook(request, request.app.database)
        raise web.HTTPMethodNotAllowed(method='POST', allowed_methods=['GET', 'HEAD'])

    app['runner_url'] = runner_url
    app['archiver_url'] = archiver_url
    app['differ_url'] = differ_url
    app['policy'] = policy_config
    app['publisher_url'] = publisher_url
    app['vcs_managers'] = vcs_managers
    if external_url:
        app['external_url'] = URL(external_url)
    else:
        app['external_url'] = None

    if db is None:
        setup_postgres(app)
    else:
        app.database = app['pool'] = db
    app['config'] = config

    setup_artifact_manager(app, trace_configs=trace_configs)

    setup_openid(
        app, config.oauth2_provider.base_url if config.oauth2_provider else None)
    app.router.add_post("/", handle_post_root, name="root-post")
    api_app = create_api_app(
        publisher_url,
        runner_url,  # type: ignore
        vcs_managers,
        differ_url,
        config,
        external_url=(
            app['external_url'].join(URL("api")) if app['external_url'] else None
        ),
        trace_configs=trace_configs,
        db=db
    )
    api_app['policy_config'] = policy_config
    api_app['dep_server_url'] = dep_server_url
    api_app['followup_url'] = followup_url

    api_app.router.add_routes(api_routes)

    setup_aiohttp_apispec(
        app=api_app,
        title="Debian Janitor API Documentation",
        version=None,
        url="/swagger.json",
        swagger_path="/docs",
    )

    app.add_subapp("/api", api_app)
    import aiohttp_apispec
    app.router.add_static(
        '/static/swagger',
        os.path.join(os.path.dirname(aiohttp_apispec.__file__), "static"))

    if debugtoolbar:
        import aiohttp_debugtoolbar

        # install aiohttp_debugtoolbar
        aiohttp_debugtoolbar.setup(app, hosts=debugtoolbar)

    setup_logfile_manager(app, trace_configs=trace_configs)

    return private_app, app


async def main(argv=None):
    import argparse
    import os

    from janitor.config import read_config

    from debian_janitor.policy import read_policy

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--debugtoolbar", type=str, action="append",
        help="IP to allow debugtoolbar queries from.")
    parser.add_argument("--host", type=str, help="Host to listen on")
    parser.add_argument("--port", type=int, help="Port to listen on", default=8080)
    parser.add_argument(
        "--public-port", type=int, help="Public port to listen on", default=8090)
    parser.add_argument(
        "--publisher-url",
        type=str,
        default="http://localhost:9912/",
        help="URL for publisher.",
    )
    parser.add_argument(
        "--runner-url",
        type=str,
        default="http://localhost:9911/",
        help="URL for runner",
    )
    parser.add_argument(
        "--archiver-url",
        type=str,
        default="http://localhost:9914/",
        help="URL for archiver",
    )
    parser.add_argument(
        "--differ-url",
        type=str,
        default="http://localhost:9920/",
        help="URL for differ",
    )
    parser.add_argument(
        "--policy",
        help="Policy file to read",
        type=str,
        default=os.path.join(os.path.dirname(__file__), "..", "..", "policy.conf"),
    )
    parser.add_argument(
        "--config", type=str, default="janitor.conf", help="Path to configuration"
    )
    parser.add_argument(
        "--debug",
        action="store_true",
        help="Enable debugging mode. For example, avoid minified JS.",
    )
    parser.add_argument(
        "--gcp-logging", action='store_true', help='Use Google cloud logging')
    parser.add_argument(
        "--external-url", type=str, default=None, help="External URL")
    parser.add_argument(
        "--dep-server-url", type=str, default=None, help="Ognibuild dep server URL")
    parser.add_argument(
        "--followup-url", type=str, default=None, help="Followup server URL")

    args = parser.parse_args()

    if args.gcp_logging:
        import google.cloud.logging
        client = google.cloud.logging.Client()
        client.get_default_handler()
        client.setup_logging()
    else:
        if args.debug:
            logging.basicConfig(level=logging.DEBUG)
        else:
            logging.basicConfig(level=logging.INFO)

    if args.debug:
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.slow_callback_duration = 0.1
        warnings.simplefilter('always', ResourceWarning)

    with open(args.config) as f:
        config = read_config(f)

    with open(args.policy) as f:
        policy_config = read_policy(f)

    private_app, public_app = await create_app(
        config, policy_config, minified=args.debug,
        external_url=args.external_url,
        debugtoolbar=args.debugtoolbar,
        runner_url=args.runner_url,
        archiver_url=args.archiver_url,
        publisher_url=args.publisher_url,
        vcs_managers=get_vcs_managers_from_config(config),
        differ_url=args.differ_url,
        listen_address=args.host, followup_url=args.followup_url,
        port=args.port, dep_server_url=args.dep_server_url)

    private_runner = web.AppRunner(private_app)
    public_runner = web.AppRunner(public_app)
    await private_runner.setup()
    await public_runner.setup()
    site = web.TCPSite(private_runner, args.host, port=args.port)
    await site.start()
    logging.info("Listening on %s:%s", args.host, args.port)
    site = web.TCPSite(public_runner, args.host, port=args.public_port)
    await site.start()
    logging.info("Listening on %s:%s", args.host, args.public_port)
    while True:
        await asyncio.sleep(3600)


if __name__ == "__main__":
    import sys

    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    sys.exit(asyncio.run(main(sys.argv)))
