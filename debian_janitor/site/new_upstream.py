#!/usr/bin/python3

import aiozipkin
import asyncpg
from aiohttp import web
from aiohttp_apispec import docs
from janitor.config import get_campaign_config
from janitor.site import TRANSIENT_ERROR_RESULT_CODES
from janitor.site.common import (html_template,
                                 render_template_for_request)
from debian_janitor.site import generate_pkg_context


async def generate_candidates(db, suite):
    async with db.acquire() as conn:
        query = """
SELECT
  package.name AS package,
  candidate.suite AS suite,
  candidate.context AS version,
  candidate.value AS value,
  candidate.success_chance AS success_chance,
  package.archive_version AS archive_version
FROM candidate
INNER JOIN package on package.codebase = candidate.codebase
WHERE NOT package.removed AND suite = $1
"""
        candidates = await conn.fetch(query, suite)
    candidates.sort(key=lambda row: row['package'])
    return {"candidates": candidates, "suite": suite}


@html_template(
    "new-upstream/package.html", headers={"Cache-Control": "max-age=600"}
)
async def handle_new_upstream_pkg(request):
    suite = request.match_info["suite"]
    pkg = request.match_info["pkg"]
    run_id = request.match_info.get("run_id")
    return await generate_pkg_context(
        request.app.database,
        request.app['config'],
        suite,
        request.app['http_client_session'],
        request.app['differ_url'],
        request.app['vcs_managers'],
        pkg,
        aiozipkin.request_span(request),
        run_id)


@html_template(
    "new-upstream/candidates.html",
    headers={"Cache-Control": "max-age=600"})
async def handle_new_upstream_candidates(request):
    suite = request.match_info["suite"]
    return await generate_candidates(request.app.database, suite)


@html_template(
    "new-upstream/stats.html",
    headers={"Cache-Control": "max-age=60"})
async def handle_stats(request):
    suite = request.match_info["suite"]
    return {"suite": suite}


@html_template(
    "new-upstream/repology.html",
    headers={"Cache-Control": "max-age=60"})
async def handle_repology(request):
    suite = request.match_info["suite"]
    return {"suite": suite}


async def get_published_packages(conn: asyncpg.Connection, suite):
    return await conn.fetch("""
select distinct on (debian_build.source) debian_build.source, debian_build.version,
package.archive_version from debian_build
left join package on
debian_build.source = package.name and debian_build.distribution = package.distribution
where debian_build.distribution = $1 and not package.removed
order by debian_build.source, debian_build.version desc
""", suite)


async def handle_apt_repo(request):
    suite = request.match_info["suite"]

    async with request.app.database.acquire() as conn:
        vs = {
            "packages": await get_published_packages(conn, suite),
            "suite": suite,
            "campaign_config": get_campaign_config(request.app['config'], suite),
        }
        text = await render_template_for_request(suite + ".html", request, vs)
        return web.Response(
            content_type="text/html",
            text=text,
            headers={"Cache-Control": "max-age=60"},
        )


async def summarize_results(db, suite):
    results = {
        'success': 0,
        'nothing-to-do': 0,
        'error': 0,
        'native-package': 0,
        'upstream-vcs-unsupported': 0,
        'transient-error': 0}
    async with db.acquire() as conn:
        for result_code, c in await conn.fetch(
                "SELECT result_code, count(*) from last_runs "
                "where suite = $1 group by result_code", suite):
            if result_code in ('success', 'nothing-to-do', 'native-package'):
                results[result_code] += c
            elif result_code.startswith('upstream-unsupported-vcs'):
                results['upstream-vcs-unsupported'] += c
            elif result_code in TRANSIENT_ERROR_RESULT_CODES:
                results['transient-error'] += c
            else:
                results['error'] += c
    return results


async def handle_chart_results(request):
    suite = request.match_info["suite"]
    return web.json_response(
        list((await summarize_results(request.app.database, suite)).items()))


@docs()
async def handle_report(request):
    suite = request.match_info["suite"]
    report = {}
    merge_proposal = {}
    async with request.app['db'].acquire() as conn:
        for package, url in await conn.fetch("""
SELECT
    DISTINCT ON (merge_proposal.url)
    merge_proposal.package, merge_proposal.url
FROM
    merge_proposal
LEFT JOIN run
ON merge_proposal.revision = run.revision AND run.result_code = 'success'
AND status = 'open'
WHERE run.suite = $1
""", suite):
            merge_proposal[package] = url
        query = """
SELECT DISTINCT ON (package)
  result_code,
  start_time,
  package,
  result
FROM
  last_unabsorbed_runs
WHERE suite = $1
ORDER BY package, suite, start_time DESC
"""
        for record in await conn.fetch(query, suite):
            if record['result_code'] not in ("success", "nothing-to-do"):
                continue
            data = {
                "timestamp": record['start_time'].isoformat(),
                "upstream-version": record['result'].get("upstream_version"),
                "old-upstream-version": record['result'].get("old_upstream_version"),
            }
            if record['package'] in merge_proposal:
                data["merge-proposal"] = merge_proposal[record['package']]
            report[record['package']] = data
    return web.json_response(
        report, headers={"Cache-Control": "max-age=600"}, status=200
    )


def register_new_upstream_endpoints(router):
    NEW_UPSTREAM_REGEX = "fresh-(releases|snapshots)"
    router.add_get(
        "/{suite:%s}/" % NEW_UPSTREAM_REGEX, handle_apt_repo,
        name="new-upstream-start"
    )
    router.add_get(
        "/{suite:%s}/stats" % NEW_UPSTREAM_REGEX, handle_stats,
        name="new-upstream-stats"
    )
    router.add_get(
        "/{suite:%s}/+chart/results" % NEW_UPSTREAM_REGEX, handle_chart_results,
        name="new-upstream-chart-results"
    )
    router.add_get(
        "/{suite:%s}/pkg/{pkg}/" % NEW_UPSTREAM_REGEX,
        handle_new_upstream_pkg,
        name="new-upstream-package",
    )
    router.add_get(
        "/{suite:%s}/pkg/{pkg}/{run_id}" % NEW_UPSTREAM_REGEX,
        handle_new_upstream_pkg,
        name="new-upstream-run",
    )
    router.add_get(
        "/{suite:%s}/candidates" % NEW_UPSTREAM_REGEX,
        handle_new_upstream_candidates,
        name="new-upstream-candidates",
    )
    router.add_get(
        "/{suite:" + NEW_UPSTREAM_REGEX + "}/api/report", handle_report,
        name="report")
    router.add_get(
        "/{suite:" + NEW_UPSTREAM_REGEX + "}/repology", handle_repology,
        name="repology")
