#!/usr/bin/python3

import logging

import asyncpg
from aiohttp import ClientConnectorError, ClientResponseError
from yarl import URL


async def get_candidates(
        conn: asyncpg.Connection,
        maintainer):
    query = """
SELECT
  package.name AS package,
  candidate.codebase AS codebase,
  candidate.suite AS suite,
  candidate.context AS context,
  candidate.value AS value,
  candidate.success_chance AS success_chance
FROM candidate
LEFT JOIN package ON candidate.codebase = package.codebase
WHERE maintainer_email = $1 OR $1 = ANY(uploader_emails) AND NOT removed
"""
    return await conn.fetch(query, maintainer)


async def get_rate_limits(http_client_session, publisher_url):
    url = URL(publisher_url) / "rate-limits"
    try:
        async with http_client_session.get(url, raise_for_status=True) as resp:
            return (await resp.json())
    except ClientConnectorError as e:
        logging.warning('Unable to reach publisher: %r', e)
        return None
    except ClientResponseError as e:
        logging.warning('Error from publisher: %r', e)
        return None


async def write_maintainer_stats(conn, http_client_session, publisher_url):
    rate_limits = await get_rate_limits(http_client_session, publisher_url)

    by_maintainer: dict[str, dict[str, int]] = {}
    for maintainer_email, status, count in await conn.fetch(
        """
select maintainer_email, status, count(*) from merge_proposal
left join package on package.codebase = merge_proposal.codebase
group by maintainer_email, status
order by maintainer_email asc
"""
    ):
        by_maintainer.setdefault(maintainer_email, {})[status] = count

    if rate_limits is not None:
        for email, cnt in rate_limits['proposals_per_bucket'].items():
            by_maintainer.setdefault(email, {})['remaining_open'] = cnt['remaining']
            by_maintainer.setdefault(email, {})['max_open'] = cnt['max_open']

    return {"by_maintainer": by_maintainer}


async def get_proposals(conn: asyncpg.Connection, email):
    return await conn.fetch("""
SELECT
    DISTINCT ON (merge_proposal.url)
    package.name AS package, merge_proposal.url AS url,
    merge_proposal.status AS status,
    run.suite AS suite
FROM
    merge_proposal
LEFT JOIN run
ON merge_proposal.revision = run.revision AND run.result_code = 'success'
INNER JOIN package ON package.codebase = run.codebase
WHERE maintainer_email = $1 OR $1 = ANY(uploader_emails) AND NOT removed
ORDER BY merge_proposal.url, run.finish_time DESC
""", email)


async def get_reviews(conn: asyncpg.Connection, email):
    stats = await conn.fetch(
        'SELECT verdict, count(*) FROM review WHERE reviewer = $1 GROUP BY 1',
        email)
    if not stats:
        return {}, []
    latest = await conn.fetch(
        'SELECT run.codebase, package.name AS package, '
        'run.suite AS campaign, review.* FROM review '
        'LEFT JOIN run ON review.run_id = run.id '
        'LEFT JOIN package on run.codebase = package.codebase '
        'WHERE reviewer = $1 ORDER BY reviewed_at DESC '
        'LIMIT 10', email)
    return stats, latest


async def write_maintainer_overview(
        conn, http_client_session, publisher_url, maintainer, span):
    if publisher_url is not None:
        rate_limits = await get_rate_limits(http_client_session, publisher_url)
    else:
        rate_limits = None

    if rate_limits is not None:
        maintainer_rate_limits = rate_limits['proposals_per_bucket'].get(maintainer, {})
        remaining_open_mps = maintainer_rate_limits.get('remaining')
        max_open_mps = maintainer_rate_limits.get('max_open')
    else:
        remaining_open_mps = None
        max_open_mps = None

    with span.new_child('sql:packages'):
        packages = await conn.fetch(
            'SELECT * FROM package '
            'WHERE maintainer_email = $1 OR $1 = ANY(uploader_emails) '
            'AND NOT removed', maintainer)

    with span.new_child('sql:proposals'):
        proposals = await get_proposals(conn, maintainer)
    with span.new_child('sql:candidates'):
        candidates = await get_candidates(conn, maintainer)
    with span.new_child('sql:reviews'):
        review_stats, review_latest = await get_reviews(conn, maintainer)

    with span.new_child('sql:runs'):
        runs = await conn.fetch("""
SELECT DISTINCT ON (package.name)
  id,
  package.name,
  command,
  finish_time,
  result_code
FROM
  last_unabsorbed_runs
LEFT JOIN package ON last_unabsorbed_runs.codebase = package.codebase
LEFT JOIN debian_build ON last_unabsorbed_runs.id = debian_build.run_id
WHERE maintainer_email = $1 OR $1 = ANY(uploader_emails) AND NOT removed
ORDER BY package.name, suite, start_time DESC
""", maintainer)

    return {
        "packages": packages,
        "runs": runs,
        "candidates": candidates,
        "maintainer": maintainer,
        "proposals": proposals,
        "max_open_mps": max_open_mps,
        "review_stats": review_stats,
        "review_latest": review_latest,
        "remaining_open_mps": remaining_open_mps,
    }
