#!/usr/bin/python3

from aiohttp import web
from janitor.site.common import html_template

SUITE = "mia"

routes = web.RouteTableDef()


@routes.get("/mia/", name="mia-start")
@html_template("mia/start.html")
async def handle_mia_start(request):
    return {}


def register_mia_endpoints(router):
    router.add_routes(routes)
