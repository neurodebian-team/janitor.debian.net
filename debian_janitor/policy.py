#!/usr/bin/python3
# Copyright (C) 2018 Jelmer Vernooij <jelmer@jelmer.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

import logging
import re
from dataclasses import dataclass
from datetime import datetime, timedelta
from email.utils import parseaddr
from fnmatch import fnmatch
from typing import TextIO

import asyncpg
from aiohttp import ClientResponseError, ClientSession
from google.protobuf import text_format  # type: ignore
from yarl import URL

from . import policy_pb2
from .package_overrides_pb2 import OverrideConfig

PolicyConfig = policy_pb2.PolicyConfig


def read_policy(f: TextIO) -> policy_pb2.PolicyConfig:
    return text_format.Parse(f.read(), policy_pb2.PolicyConfig())


def matches(match, package_name, vcs_url, package_maintainer,
            package_uploaders, in_base, release_stages):
    package_maintainer_email = parseaddr(package_maintainer)[1]
    for maintainer in match.maintainer:
        if not fnmatch(package_maintainer_email, maintainer):
            return False
    package_uploader_emails = [
        parseaddr(uploader)[1] for uploader in (package_uploaders or [])
    ]
    for uploader in match.uploader:
        if not any(fnmatch(u, uploader) for u in package_uploader_emails):
            return False
    for source_package in match.source_package:
        if not fnmatch(package_name, source_package):
            return False
    for vcs_url_regex in match.vcs_url_regex:
        if vcs_url is None or not re.fullmatch(vcs_url_regex, vcs_url):
            return False
    if match.HasField("in_base") and match.in_base != in_base:
        return False
    for before_stage in match.before_stage:
        if release_stages is None:
            raise ValueError(
                'no release stages passed in, unable to match on before_stage')
        try:
            threshold = (
                release_stages[before_stage.stage_name]
                + timedelta(days=before_stage.days_delta))
        except KeyError as e:
            raise ValueError(f'no such stage {before_stage.stage_name}') from e
        if datetime.utcnow() > threshold:
            return False
    for after_stage in match.after_stage:
        if release_stages is None:
            raise ValueError(
                'no release stages passed in, unable to match on after_stage')
        try:
            threshold = (
                release_stages[after_stage.stage_name]
                + timedelta(days=after_stage.days_delta))
        except KeyError as e:
            raise ValueError(f'no such stage {after_stage.stage_name}') from e
        if datetime.utcnow() < threshold:
            return False
    return True


PUBLISH_MODE_STR = {
    policy_pb2.propose: "propose",
    policy_pb2.attempt_push: "attempt-push",
    policy_pb2.bts: "bts",
    policy_pb2.push: "push",
    policy_pb2.build_only: "build-only",
    policy_pb2.skip: "skip",
    policy_pb2.push_derived: "push-derived",
}


CHANGELOG_BEHAVIOUR_STR = {
    policy_pb2.auto: "auto",
    policy_pb2.update: "update",
    policy_pb2.leave: "leave",
}


REVIEW_POLICY_STR = {
    policy_pb2.required: "required",
    policy_pb2.not_required: "not-required",
    None: None,
}


@dataclass
class ExpandedPolicy:
    per_branch: dict[str, tuple[str, int]]
    command: str
    manual_review: str | None


def expand_policy(
    config: policy_pb2.PolicyConfig,
    overrides: OverrideConfig,
    campaign: str,
    package_name: str,
    vcs_url: str | None,
    maintainer: str,
    uploaders: list[str],
    in_base: bool,
    release_stages: dict[str, datetime]
) -> ExpandedPolicy:
    publish_mode = {}
    env: dict[str, str] = {}
    if campaign in ('fresh-releases', 'fresh-snapshots'):
        for override in overrides.package:
            if override.name == package_name and override.upstream_branch_url:
                env['UPSTREAM_BRANCH_URL'] = override.upstream_branch_url

    command = None
    manual_review = None
    for policy in config.policy:
        if policy.match and not any(
                matches(m, package_name, vcs_url, maintainer, uploaders,
                        in_base, release_stages)
                for m in policy.match):
            continue
        if policy.update_changelog is not None:
            env['DEB_UPDATE_CHANGELOG'] = (
                CHANGELOG_BEHAVIOUR_STR[policy.update_changelog])
        if policy.compat_release:
            env['DEB_COMPAT_RELEASE'] = policy.compat_release
        for env_entry in policy.env:
            if env_entry.value is None:
                del env[env_entry.name]
            else:
                env[env_entry.name] = env_entry.value
        for s in policy.campaign:
            if s.name == campaign:
                break
        else:
            continue
        for publish in s.publish:
            publish_mode[publish.role] = (publish.mode, publish.max_frequency_days)
        if s.command:
            command = s.command
        if s.manual_review:
            manual_review = s.manual_review
    if command is not None:
        command = ' '.join(
            ['%s=%s' % e for e in sorted(env.items())] + [command])
    return ExpandedPolicy(
        per_branch={k: (PUBLISH_MODE_STR[v[0]], v[1])
                    for (k, v) in publish_mode.items()},
        command=command,  # type: ignore
        manual_review=REVIEW_POLICY_STR[manual_review],
    )


async def read_release_stages(url: str) -> dict[str, datetime]:
    import yaml
    ret: dict[str, datetime] = {}
    async with ClientSession() as session:
        async with session.get(url) as resp:
            body = await resp.read()
            y = yaml.safe_load(body)
            for stage, data in y['stages'].items():
                if data['starts'] == 'TBA':
                    continue
                ret[stage] = datetime.fromisoformat(data['starts'])
    return ret


class PolicyApplyer:

    def __init__(self, policy, overrides):
        self.policy = policy
        self.overrides = overrides

    async def __aenter__(self):
        if self.policy.freeze_dates_url:
            self.release_stages = await read_release_stages(
                self.policy.freeze_dates_url)
            logging.info('Release stages passed: %r', self.release_stages)
        else:
            self.release_stages = {}
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        return False

    @classmethod
    def from_paths(cls, path, overrides_path=None):
        with open(path) as f:
            policy = read_policy(f)
        if overrides_path:
            with open(overrides_path) as f:
                overrides = text_format.Parse(f.read(), OverrideConfig())
        else:
            overrides = OverrideConfig()
        return cls(policy, overrides)

    def expand(self,
               campaign: str, package_name: str, vcs_url: str | None,
               maintainer: str, uploaders: list[str], in_base: bool) -> ExpandedPolicy:
        return expand_policy(
            self.policy, self.overrides, campaign, package_name, vcs_url, maintainer,
            uploaders, in_base, self.release_stages)

    def known_campaigns(self):
        ret = set()
        for policy in self.policy.policy:
            for campaign in policy.campaign:
                ret.add(campaign.name)
        return ret


async def iter_packages(conn: asyncpg.Connection, package: str | None = None):
    query = """
SELECT
  name,
  vcs_url,
  maintainer_email,
  uploader_emails,
  in_base
FROM
  package
WHERE
  NOT removed
"""
    args = []
    if package:
        query += " AND name = $1"
        args.append(package)
    return await conn.fetch(query, *args)


def serialize_publish_policy(intended_policy, maintainer_email):
    return {
        'rate_limit_bucket': maintainer_email,
        'per_branch': {
            role: {'mode': mode, 'max_frequency_days': max_frequency_days}
            for role, (mode, max_frequency_days)
            in intended_policy.per_branch.items()}}


def publish_policy_name(campaign, package):
    return campaign + '-' + package


async def upload_publish_policy(
        session, publisher_url, name, serialized_publish_policy):
    logging.info("%s -> %r", name, serialized_publish_policy)
    async with session.put(
            publisher_url / 'policy' / name,
            json=serialized_publish_policy,
            raise_for_status=True):
        pass


async def sync_publish_policy(
        conn, policy_applier, publisher_url: URL,
        selected_package: str | None = None):
    current_policy = {}
    campaigns = policy_applier.known_campaigns()
    num_updated = 0
    logging.info('Creating current policy')
    async with ClientSession() as session:
        async with session.get(publisher_url / "policy",
                               raise_for_status=True) as resp:
            current_policy = await resp.json()
        logging.info('Current policy: %d entries', len(current_policy))
        logging.info('Updating policy')
        for package in await iter_packages(conn, package=selected_package):
            updated = False
            for campaign in campaigns:
                intended_policy = policy_applier.expand(
                    campaign,
                    package['name'],
                    package['vcs_url'],
                    package['maintainer_email'],
                    package['uploader_emails'],
                    package['in_base'])
                name = publish_policy_name(campaign, package['name'])
                intended_publish_policy = serialize_publish_policy(
                    intended_policy, package['maintainer_email'])
                stored_policy = current_policy.pop(name, None)
                if stored_policy != intended_publish_policy:
                    await upload_publish_policy(
                        session, publisher_url, name, intended_publish_policy)
                    updated = True
            if updated:
                num_updated += 1
        if selected_package is None:
            num_deleted = len(current_policy.keys())
            logging.info('Deleting %d from policy', num_deleted)
            for name in current_policy.keys():
                try:
                    async with session.delete(
                            publisher_url / 'policy' / name,
                            raise_for_status=True) as resp:
                        pass
                except ClientResponseError as e:
                    if e.status == 412:
                        logging.warning('Conflict removing policy %r', name)
                        continue
                    raise
        else:
            num_deleted = None
        return num_updated, num_deleted


def validate_policy(config, policy):
    from lintian_brush.config import resolve_release_codename
    campaigns = set()
    for campaign in config.campaign:
        campaigns.add(campaign.name)
    for p in policy.policy:
        if p.compat_release and not resolve_release_codename(p.compat_release):
            raise ValueError(
                'invalid compat release %s' % p.compat_release)
        for env_entry in p.env:
            if not env_entry.name:
                raise ValueError("env entry has no name")
        for s in p.campaign:
            if s.name not in campaigns:
                raise ValueError('unknown campaign %s' % s.name)


async def main(argv):
    import argparse

    from janitor import state
    from janitor.config import read_config

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--config", type=str, default="janitor.conf", help="Path to configuration."
    )
    parser.add_argument(
        "--policy",
        type=str,
        default="policy.conf",
        help="Path to policy configuration.",
    )
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--publisher-url', type=str)
    parser.add_argument('--validate', action='store_true')
    parser.add_argument(
        "--gcp-logging", action='store_true', help='Use Google cloud logging.')
    parser.add_argument(
        "--package-overrides", type=str, help="Package overrides")
    parser.add_argument('package', type=str, nargs='?')
    args = parser.parse_args(argv[1:])

    if args.gcp_logging:
        import google.cloud.logging
        client = google.cloud.logging.Client()
        client.get_default_handler()
        client.setup_logging()
    elif args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    with open(args.config) as f:
        config = read_config(f)

    if args.validate:
        with open(args.policy) as f:
            policy = read_policy(f)

        validate_policy(config, policy)
    else:
        db = await state.create_pool(config.database_location)

        async with PolicyApplyer.from_paths(
                    args.policy, args.package_overrides) as policy_applier, \
                db.acquire() as conn:
            num_updated, num_delete = await sync_publish_policy(
                conn, policy_applier, URL(args.publisher_url),
                selected_package=args.package)
        logging.info('Updated policy for %d packages.', num_updated)


if __name__ == "__main__":
    import asyncio
    import sys
    asyncio.run(main(sys.argv))
