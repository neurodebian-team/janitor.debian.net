#!/usr/bin/python
# Copyright (C) 2022 Jelmer Vernooij <jelmer@jelmer.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

from unittest.mock import patch
from datetime import datetime

from fakeredis.aioredis import FakeRedis
from janitor.config import Config
from janitor.runner import store_run, store_change_set

from debian_janitor.policy import PolicyConfig
from debian_janitor.site.__main__ import create_app
from debian_janitor.site.api import format_madison_lines


def create_config():
    config = Config()
    campaign = config.campaign.add()
    campaign.name = "lintian-fixes"
    campaign = config.campaign.add()
    campaign.name = "fresh-releases"
    campaign = config.campaign.add()
    campaign.name = "fresh-snapshots"
    return config


async def test_create_app(db):
    await create_app(
        config=create_config(), policy_config=PolicyConfig(), redis=FakeRedis(), db=db)


async def create_public_client(aiohttp_client, db):
    assert db
    private_app, public_app = await create_app(
        config=create_config(), policy_config=PolicyConfig(), redis=FakeRedis(),
        db=db)
    return await aiohttp_client(public_app)


async def test_apt(aiohttp_client, db):
    client = await create_public_client(aiohttp_client, db)
    resp = await client.get('/apt')
    assert resp.status == 200
    text = await resp.text()
    assert '<body>' in text


async def test_about(aiohttp_client, db):
    client = await create_public_client(aiohttp_client, db)
    resp = await client.get('/about')
    assert resp.status == 200
    text = await resp.text()
    assert '<body>' in text


async def test_maintainer_stats(aiohttp_client, db):
    client = await create_public_client(aiohttp_client, db)
    resp = await client.get('/m/jelmer@debian.org')
    assert resp.status == 200
    text = await resp.text()
    assert '<body>' in text


async def test_start(aiohttp_client, db):
    client = await create_public_client(aiohttp_client, db)
    resp = await client.get('/')
    assert resp.status == 200
    text = await resp.text()
    assert '<body>' in text


async def test_contact(aiohttp_client, db):
    client = await create_public_client(aiohttp_client, db)
    resp = await client.get('/contact')
    assert resp.status == 200
    text = await resp.text()
    assert '<body>' in text


async def test_fresh_releases(aiohttp_client, db):
    client = await create_public_client(aiohttp_client, db)
    resp = await client.get('/fresh-releases')
    assert resp.status == 200
    text = await resp.text()
    assert '<body>' in text


async def test_fresh_snapshots(aiohttp_client, db):
    client = await create_public_client(aiohttp_client, db)
    resp = await client.get('/fresh-snapshots')
    assert resp.status == 200
    text = await resp.text()
    assert '<body>' in text


async def test_madison(aiohttp_client, db):
    client = await create_public_client(aiohttp_client, db)

    # TODO(jelmer): Add a fixture for creating a dummy codebase + run
    run_id = 'some-run-id'
    codebase = 'foo'
    campaign = 'mycampaign'
    async with db.acquire() as conn:
        await store_change_set(conn, run_id, campaign=campaign)
        await conn.execute('INSERT INTO codebase (name) VALUES ($1)', codebase)
        await store_run(
            conn, run_id=run_id,
            codebase=codebase, campaign=campaign,
            vcs_type="git", subpath="",
            start_time=datetime.utcnow(),
            finish_time=datetime.utcnow(),
            command="true",
            result_code="missing-result-code",
            codemod_result={},
            main_branch_revision=b'some-revid',
            revision=b'revid',
            description='Did a thing',
            context=None,
            instigated_context=None,
            logfilenames=[],
            value=1,
            change_set=run_id,
            worker_name=None,
            branch_url='https://example.com/blah')

        await conn.execute(
            'INSERT INTO debian_build (source, distribution, version, binary_packages, run_id) '
            'VALUES ($1, $2, $3, $4, $5)', 'foo', campaign, '1.0-1', ['bar', 'bla'], run_id)
    resp = await client.get('/api/madison?package=foo&text=on')
    assert resp.status == 200
    text = await resp.text()
    assert text == 'foo | 1.0-1 | mycampaign | \n'


def test_madison_lines():
    assert ['a | b | cc\n'] == list(format_madison_lines([('a', 'b', 'cc')]))
    assert ['a | bb | c\n', 'd | e  | f\n'] == list(format_madison_lines([('a', 'bb', 'c'), ('d', 'e', 'f')]))
    assert [] == list(format_madison_lines([]))


async def test_missing_upstream_urls(aiohttp_client, db):
    client = await create_public_client(aiohttp_client, db)
    resp = await client.get('/api/missing-upstream-urls')
    assert resp.status == 200
    assert await resp.json() == []
