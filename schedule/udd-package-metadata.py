#!/usr/bin/python3
# Copyright (C) 2018 Jelmer Vernooij <jelmer@jelmer.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

"""Exporting of upstream metadata from UDD."""

import itertools
import logging
import re
from collections.abc import Iterator
from email.utils import parseaddr
from typing import Optional, Any

from debian.changelog import Version
from debmutate.vcs import unsplit_vcs_url, split_vcs_url

from breezy import urlutils
from breezy.branch import Branch
from breezy.transport import Transport
from debian.changelog import Version
from debmutate.vcs import split_vcs_url, unsplit_vcs_url
from janitor.vcs import (BranchOpenFailure, get_vcs_abbreviation,
                         open_branch_ext)
from lintian_brush.salsa import guess_repository_url, salsa_url_from_alioth_url
from lintian_brush.vcs import determine_browser_url
from silver_platter.probers import select_preferred_probers, select_probers

from debian_janitor import is_alioth_url
from debian_janitor.package_metadata_pb2 import PackageList
from debian_janitor.udd import DEFAULT_UDD_URL


def extract_uploader_emails(uploaders: str | None) -> list[str]:
    if not uploaders:
        return []
    ret = []
    for uploader in uploaders.split(","):
        if not uploader:
            continue
        email = parseaddr(uploader)[1]
        if not email:
            continue
        ret.append(email)
    return ret


async def iter_packages_with_metadata(
        udd, release, packages: list[str] | None = None):
    args = [release]
    query = """
select distinct on (sources.source) sources.source AS source,
sources.maintainer_email AS maintainer_email,
sources.uploaders AS uploaders,
popcon_src.insts AS insts,
vcswatch.vcs AS vcswatch_vcs_type,
sources.vcs_type AS control_vcs_type,
vcswatch.url AS vcswatch_vcs_url, sources.vcs_url AS control_vcs_url,
vcswatch.branch AS vcswatch_branch,
vcswatch.browser AS vcswatch_vcs_browser, sources.vcs_browser AS control_vcs_browser,
commit_id,
status as vcswatch_status,
sources.version AS control_version,
vcswatch.version AS vcswatch_version,
sources.original_maintainer as original_maintainer,
sources.release as release
from sources left join popcon_src on sources.source = popcon_src.source
left join vcswatch on vcswatch.source = sources.source
where sources.release = $1
"""
    if packages:
        query += " and sources.source = ANY($2::text[])"
        args.append(packages)
    query += " order by sources.source, sources.version desc"
    for row in await udd.fetch(query, *args):
        yield row


async def iter_removals(udd, release: str, packages: list[str] | None = None) -> Iterator:
    query = """\
SELECT name, version from package_removal
left join package_removal_batch on package_removal_batch.id = package_removal.batch_id
WHERE 'source' = any(arch_array) AND package_removal_batch.distribution = $1
"""
    args: list[Any] = [release]
    if packages:
        query += " and name = ANY($1::text[])"
        args.append(packages)
    return await udd.fetch(query, *args)


def possible_urls_from_alioth_url(vcs_type, vcs_url):
    # These are the same transformations applied by vcswatch. The goal is mostly
    # to get a URL that properly redirects.
    https_alioth_url = re.sub(
        r"(https?|git)://(anonscm|git).debian.org/(git/)?",
        r"https://anonscm.debian.org/git/",
        vcs_url,
    )

    yield https_alioth_url
    yield salsa_url_from_alioth_url(vcs_type, vcs_url)


def possible_salsa_urls_from_package_name(
        package_name: str, maintainer_email: Optional[str] = None):
    yield guess_repository_url(package_name, maintainer_email)
    yield "https://salsa.debian.org/debian/%s.git" % package_name


def open_guessed_salsa_branch(
    pkg: str, maintainer_email: str | None, vcs_type, vcs_url, possible_transports=None,
):
    probers = select_probers("git")
    vcs_url, params = urlutils.split_segment_parameters_raw(vcs_url)

    tried = set([vcs_url])

    for salsa_url in itertools.chain(
        possible_urls_from_alioth_url(vcs_type, vcs_url),
        possible_salsa_urls_from_package_name(pkg, maintainer_email),
    ):
        if not salsa_url or salsa_url in tried:
            continue

        tried.add(salsa_url)

        salsa_url = urlutils.join_segment_parameters_raw(salsa_url, *params)

        logging.debug("Trying to access salsa URL %s instead.", salsa_url)
        try:
            branch = open_branch_ext(
                salsa_url,
                possible_transports=possible_transports, probers=probers)
        except BranchOpenFailure:
            pass
        else:
            logging.info("Converting alioth URL: %s -> %s", vcs_url, salsa_url)
            return branch
    return None


def find_alioth_replacement(
        package: str, maintainer_email: str | None, vcs_type: str, repo_url: str,
        branch_name: Optional[str] = None,
        possible_transports: Optional[list[Transport]] = None) -> Optional[Branch]:
    probers = select_preferred_probers(vcs_type)
    logging.info(
        'Opening branch %s with %r', repo_url,
        [p.__name__ for p in probers])
    try:
        return open_branch_ext(
            repo_url, possible_transports=possible_transports, probers=probers)
    except BranchOpenFailure:
        try:
            return open_guessed_salsa_branch(
                package,
                maintainer_email,
                vcs_type,
                repo_url,
                possible_transports=possible_transports,
            )
        except BranchOpenFailure:
            return None


async def main():
    possible_transports: list[Transport] = []
    import argparse

    import asyncpg

    from debian_janitor.package_overrides import read_package_overrides

    parser = argparse.ArgumentParser(prog="udd-candidates")
    parser.add_argument("packages", nargs="*")
    parser.add_argument(
        "--gcp-logging", action='store_true', help='Use Google cloud logging.')
    parser.add_argument("--udd-url", type=str, default=DEFAULT_UDD_URL, help="UDD URL")
    parser.add_argument("--release", type=str, default="sid")
    parser.add_argument(
        "--package-overrides",
        type=str,
        help="Path to package overrides.",
        default=None,
    )
    args = parser.parse_args()

    if args.gcp_logging:
        import google.cloud.logging
        client = google.cloud.logging.Client()
        client.get_default_handler()
        client.setup_logging()
    else:
        logging.basicConfig(level=logging.INFO)

    if args.package_overrides:
        with open(args.package_overrides) as f:
            package_overrides = read_package_overrides(f)
    else:
        package_overrides = {}

    udd = await asyncpg.connect(args.udd_url)

    role = await udd.fetchval(
        'SELECT role FROM releases WHERE release = $1', args.release)

    removals = {}
    for name, version in await iter_removals(udd, role, packages=args.packages):
        if name not in removals:
            removals[name] = Version(version)
        else:
            removals[name] = max(Version(version), removals[name])

    for name, version in removals.items():
        pl = PackageList()
        removal = pl.removal.add()
        removal.name = name
        removal.version = str(version)
        print(pl)

    async for row in iter_packages_with_metadata(udd, args.release, args.packages):
        pl = PackageList()
        package = pl.package.add()
        package.name = row['source']
        if row['maintainer_email'] is None:
            logging.warning('package %s has no maintainer set', row['source'])
        else:
            package.maintainer_email = row['maintainer_email']
        package.uploader_email.extend(extract_uploader_emails(row['uploaders']))
        package.in_base = (row['original_maintainer'] is not None)
        if row['insts'] is not None:
            package.insts = row['insts']
        if row['vcswatch_vcs_type']:
            package.vcs_type = row['vcswatch_vcs_type']
            repo_url, oldbranch, subpath = split_vcs_url(row['vcswatch_vcs_url'])
            if oldbranch != row['vcswatch_branch']:
                package.vcs_url = unsplit_vcs_url(
                    repo_url, row['vcswatch_branch'], subpath)
                logging.info(
                    "Fixing up branch name from vcswatch: %s → %s",
                    row['vcswatch_vcs_url'], package.vcs_url)
            else:
                package.vcs_url = row['vcswatch_vcs_url']
            if row['vcswatch_vcs_browser']:
                package.vcs_browser = row['vcswatch_vcs_browser']
        elif row['control_vcs_type']:
            package.vcs_type = row['control_vcs_type']
            package.vcs_url = row['control_vcs_url']
            if row['control_vcs_browser']:
                package.vcs_browser = row['control_vcs_browser']
        repo_url, branch_name, subpath = split_vcs_url(package.vcs_url)
        if is_alioth_url(repo_url):
            logging.info(
                'Attempting to find a replacement for obsolete alioth URL %s',
                repo_url)
            new_branch = find_alioth_replacement(
                row['source'], row['maintainer_email'], package.vcs_type,
                repo_url, branch_name, possible_transports=possible_transports)
            if new_branch:
                package.vcs_type = get_vcs_abbreviation(new_branch.repository)
                package.vcs_url = unsplit_vcs_url(
                    new_branch.repository.user_url.rstrip('/'),
                    new_branch.name, subpath)
                package.vcs_browser = determine_browser_url(
                    package.vcs_type, package.vcs_url)
        if row['commit_id']:
            package.commit_id = row['commit_id']
        if row['vcswatch_status']:
            package.vcswatch_status = row['vcswatch_status']
        package.archive_version = row['control_version']
        if row['vcswatch_version']:
            package.vcswatch_version = row['vcswatch_version']
        if row['source'] not in removals:
            package.removed = False
        else:
            package.removed = Version(row['control_version']) <= removals[row['source']]
        try:
            override = package_overrides[row['source']]
        except KeyError:
            pass
        else:
            if override.branch_url:
                package.vcs_url = override.branch_url
                package.origin = args.package_overrides
        print(pl)


if __name__ == "__main__":
    import asyncio

    asyncio.run(main())
