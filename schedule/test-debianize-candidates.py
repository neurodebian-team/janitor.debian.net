#!/usr/bin/python3

import logging
from collections.abc import AsyncIterator

import asyncpg
from breezy.plugins.debian.directory import vcs_git_url_to_bzr_url

from debian_janitor.candidates_pb2 import Candidate, CandidateList
from debian_janitor.udd import DEFAULT_UDD_URL

DEFAULT_VALUE_NEW = 50
DEFAULT_VALUE_ALREADY_PACKAGED = 25


async def iter_upstream_codebases(
        udd, packages: list[str] | None = None
        ) -> AsyncIterator[Candidate]:
    args = []
    query = """
select distinct on (sources.source) sources.source as name,
  upstream_metadata.value as vcs_url, ''
  from sources
  left join upstream_metadata on upstream_metadata.source = sources.source
  where sources.release = 'sid' AND upstream_metadata.key = 'Repository'
"""
    if packages:
        query += " and sources.source = ANY($1::text[])"
        args.append(packages)
    query += " order by sources.source, sources.version desc"
    for row in await udd.fetch(query, *args):
        candidate = Candidate()
        candidate.distribution = "upstream"
        candidate.package = row['name']
        # TODO(jelmer): Set context
        # candidate.context = None
        candidate.command = "debianize %s" % vcs_git_url_to_bzr_url(row['vcs_url'])
        candidate.campaign = "test-debianize"
        candidate.value = DEFAULT_VALUE_ALREADY_PACKAGED
        candidate.origin = "UDD"
        yield candidate


async def main():
    import argparse

    parser = argparse.ArgumentParser(prog="test-debianize-candidates")
    parser.add_argument("--udd-url", type=str, default=DEFAULT_UDD_URL, help="UDD URL")
    parser.add_argument(
        "--gcp-logging", action='store_true', help='Use Google cloud logging.')

    parser.add_argument("packages", nargs="*", default=None)

    args = parser.parse_args()

    if args.gcp_logging:
        import google.cloud.logging
        client = google.cloud.logging.Client()
        client.get_default_handler()
        client.setup_logging()
    else:
        logging.basicConfig(format='%(message)s', level=logging.INFO)

    udd = await asyncpg.connect(args.udd_url)

    async for candidate in iter_upstream_codebases(udd, args.packages):
        cl = CandidateList()
        cl.candidate.append(candidate)
        print(cl)


if __name__ == "__main__":
    import asyncio

    asyncio.run(main())
