#!/usr/bin/python3

import logging

import asyncpg
from debian.changelog import Version

from debian_janitor.candidates_pb2 import Candidate, CandidateList
from debian_janitor.udd import DEFAULT_UDD_URL

DEFAULT_VALUE_NEW_UPSTREAM = 30
INVALID_VERSION_DOWNGRADE = 5


async def iter_fresh_releases_candidates(udd, distribution, packages=None):
    args = [distribution]
    query = """\
SELECT DISTINCT ON (sources.source)
sources.source, upstream.upstream_version FROM upstream \
INNER JOIN sources ON upstream.version = sources.version \
AND upstream.source = sources.source where \
status = 'newer package available' AND \
sources.vcs_url != '' AND \
sources.release = $1
"""
    if packages is not None:
        query += " AND upstream.source = any($2::text[])"
        args.append(tuple(packages))
    query += " ORDER BY sources.source, sources.version DESC"
    for row in await udd.fetch(query, *args):
        candidate = Candidate()
        candidate.distribution = distribution
        candidate.package = row[0]
        candidate.context = row[1]
        candidate.campaign = "fresh-releases"
        candidate.value = DEFAULT_VALUE_NEW_UPSTREAM
        try:
            Version(row[1])
        except ValueError:
            candidate.value -= INVALID_VERSION_DOWNGRADE
        yield candidate


async def main():
    import argparse

    parser = argparse.ArgumentParser(prog="fresh-releases-candidates")
    parser.add_argument("packages", nargs="*", default=None)
    parser.add_argument("--udd-url", type=str, default=DEFAULT_UDD_URL, help="UDD URL")
    parser.add_argument(
        "--gcp-logging", action='store_true', help='Use Google cloud logging.')

    args = parser.parse_args()

    if args.gcp_logging:
        import google.cloud.logging
        client = google.cloud.logging.Client()
        client.get_default_handler()
        client.setup_logging()
    else:
        logging.basicConfig(
            format='%(message)s',
            level=(logging.DEBUG if args.debug else logging.INFO))

    udd = await asyncpg.connect(args.udd_url)
    async for candidate in iter_fresh_releases_candidates(
            udd, "sid", args.packages or None):
        cl = CandidateList()
        cl.candidate.append(candidate)
        print(cl)


if __name__ == "__main__":
    import asyncio

    asyncio.run(main())
