#!/bin/bash -ex

CAMPAIGN="$1"
shift

SCHEDULE=${SCHEDULE:-/schedule}
CONFIG=${CONFIG:-/etc/janitor/janitor.conf}
POLICY=${POLICY:-/etc/janitor/policy.conf}
OVERRIDES=${OVERRIDES:-/etc/janitor/package_overrides.conf}
LOGGING=${LOGGING:---gcp-logging}
RUNNER_URL=${RUNNER_URL:-http://runner.default.svc:9911}
export PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION=python
if [ -n "${PROMETHEUS_PUSHGATEWAY}" ]; then
	PROMETHEUS_ARG="--prometheus=${PROMETHEUS_PUSHGATEWAY}"
else
	PROMETHEUS_ARG=""
fi

set -o pipefail

"$@" > candidates
python3 -m debian_janitor.candidates --campaign "${CAMPAIGN}" --config=${CONFIG} --policy=${POLICY} ${LOGGING} --runner-url ${RUNNER_URL} --package-overrides ${OVERRIDES} ${PROMETHEUS_ARG} < candidates
