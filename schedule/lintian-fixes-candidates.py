#!/usr/bin/python3

# Copyright (C) 2018-2020 Jelmer Vernooij <jelmer@jelmer.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

import logging

from lintian_brush.__main__ import calculate_value

from debian_janitor.candidates_pb2 import Candidate, CandidateList
from debian_janitor.udd import DEFAULT_UDD_URL


async def iter_lintian_fixes_candidates(udd, release, packages, available_fixers):
    """Iterate over all of the packages affected by a set of tags."""
    package_rows = {}
    package_tags = {}

    args = [tuple(available_fixers), release]
    query = """
SELECT DISTINCT ON (sources.source)
sources.source,
sources.version,
sources.vcs_type,
sources.vcs_url,
sources.maintainer_email,
sources.uploaders,
ARRAY(SELECT tag FROM lintian WHERE
    sources.source = lintian.package AND
    sources.version = lintian.package_version AND
    lintian.package_type = 'source' AND
    tag = any($1::text[])
)
FROM sources
WHERE
sources.release = $2
AND vcs_type != ''
"""
    if packages is not None:
        query += " AND sources.source = any($3::text[])"
        args.append(tuple(packages))
    query += " ORDER BY sources.source, sources.version DESC"
    for row in await udd.fetch(query, *args):
        package_rows[row[0]] = row[:6]
        package_tags[row[0]] = list(row[6])
    args = [tuple(available_fixers), release]
    query = """\
SELECT DISTINCT ON (sources.source)
sources.source,
sources.version,
sources.vcs_type,
sources.vcs_url,
sources.maintainer_email,
sources.uploaders,
ARRAY(SELECT lintian.tag FROM lintian
    INNER JOIN packages ON packages.package = lintian.package \
    and packages.version = lintian.package_version \
WHERE
    lintian.tag = any($1::text[]) AND
    lintian.package_type = 'binary' AND
    sources.version = packages.version and \
    sources.source = packages.source
)
FROM
sources
WHERE sources.release = $2 AND vcs_type != ''"""
    if packages is not None:
        query += " AND sources.source = ANY($3::text[])"
        args.append(tuple(packages))
    query += " ORDER BY sources.source, sources.version DESC"
    for row in await udd.fetch(query, *args):
        package_tags.setdefault(row[0], []).extend(row[6])
    for row in package_rows.values():
        tags = package_tags[row[0]]
        value = calculate_value(tags)
        context = " ".join(sorted(set(tags)))
        candidate = Candidate()
        candidate.distribution = release
        candidate.package = row[0]
        candidate.context = context
        if value:
            candidate.value = value
        candidate.campaign = "lintian-fixes"
        yield candidate


async def main():
    import argparse

    import asyncpg
    from lintian_brush.__main__ import available_lintian_fixers

    parser = argparse.ArgumentParser(prog="lintian-fixes-candidates")
    parser.add_argument("packages", nargs="*", default=None)
    parser.add_argument("--udd-url", type=str, default=DEFAULT_UDD_URL, help="UDD URL")
    parser.add_argument(
        "--gcp-logging", action='store_true', help='Use Google cloud logging.')

    args = parser.parse_args()

    if args.gcp_logging:
        import google.cloud.logging
        client = google.cloud.logging.Client()
        client.get_default_handler()
        client.setup_logging()
    else:
        logging.basicConfig(format='%(message)s', level=logging.INFO)

    tags = set()
    available_fixers = list(available_lintian_fixers())
    for fixer in available_fixers:
        tags.update(fixer.lintian_tags)

    udd = await asyncpg.connect(args.udd_url)
    async for candidate in iter_lintian_fixes_candidates(
        udd, "sid", args.packages or None, tags
    ):
        cl = CandidateList()
        cl.candidate.append(candidate)
        print(cl)


if __name__ == "__main__":
    import asyncio

    asyncio.run(main())
