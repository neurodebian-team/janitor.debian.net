#!/usr/bin/python3

import json
import logging
import sys

from asyncpg import InterfaceError
import backoff
from debian_janitor.repology import load_from_repology
from janitor import set_user_agent, state
from janitor.config import read_config


backoff.on_exception(
    backoff.expo,
    (InterfaceError, ),
    max_tries=10)
async def store_repology(conn, name, repos):
    rows = []
    async with conn.transaction():
        await conn.execute(
            'DELETE FROM repology_project_repo WHERE project = $1', name)
        for repo in repos:
            if 'srcname' not in repo:
                logging.debug('No srcname in %r', repo)
                continue
            rows.append(
                (name, repo['repo'], repo['srcname'], repo['version'], repo['status']))
        await conn.executemany("""\
INSERT INTO repology_project_repo (project, repo, srcname, version, status)
VALUES ($1, $2, $3, $4, $5)
ON CONFLICT (project, repo) DO UPDATE SET
srcname = EXCLUDED.srcname,
version = EXCLUDED.version,
status = EXCLUDED.status""", rows)


async def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', type=str, default='janitor.conf')
    parser.add_argument('--debug', action='store_true')
    parser.add_argument(
        "--gcp-logging", action='store_true', help='Use Google cloud logging.')
    args = parser.parse_args()

    if args.gcp_logging:
        import google.cloud.logging
        client = google.cloud.logging.Client()
        client.get_default_handler()
        client.setup_logging()
    else:
        logging.basicConfig(
            format='%(message)s',
            level=(logging.DEBUG if args.debug else logging.INFO))

    with open(args.config) as f:
        config = read_config(f)

    set_user_agent(config.user_agent)

    async with state.create_pool(config.database_location) as db, \
            db.acquire() as conn:
        for name, repos in load_from_repology():
            await store_repology(conn, name, repos)
        return 0


if __name__ == '__main__':
    import asyncio
    sys.exit(asyncio.run(main()))
