#!/bin/bash -ex
DISTRO="$1"
shift 1
CONFIG=${CONFIG:-/etc/janitor/janitor.conf}
LOGGING=${LOGGING:---gcp-logging}
PUBLISHER_URL=${PUBLISHER_URL:-http://publish.default.svc:9912}
RUNNER_URL=${PUBLISHER_URL:-http://runner.default.svc:9911}

set -o pipefail

"$@" > codebases

wc -l codebases

python3 -m debian_janitor.package_metadata --distribution=${DISTRO} --config=${CONFIG} ${LOGGING} --publisher-url=${PUBLISHER_URL} --runner-url=${RUNNER_URL} < codebases

cat <<EOF | curl --data-binary @- $PROMETHEUS_PUSHGATEWAY/metrics/job/schedule-codebases
# TYPE job_last_success_unixtime gauge
job_last_success_unixtime $(date +%s).0
EOF
