#!/usr/bin/python3

import logging

from debian_janitor.candidates_pb2 import Candidate, CandidateList
from debian_janitor.udd import DEFAULT_UDD_URL

DEFAULT_VALUE_ORPHAN = 60


async def iter_orphan_candidates(udd, release, packages=None):
    args = [release]
    query = """\
SELECT DISTINCT ON (sources.source) sources.source, now() - orphaned_time, bug
FROM sources
JOIN orphaned_packages ON orphaned_packages.source = sources.source
WHERE sources.vcs_url != '' AND sources.release = $1 AND
orphaned_packages.type in ('O') AND
(sources.uploaders != '' OR
sources.maintainer != 'Debian QA Group <packages@qa.debian.org>')
"""
    if packages is not None:
        query += " AND sources.source = any($2::text[])"
        args.append(tuple(packages))
    for row in await udd.fetch(query, *args):
        candidate = Candidate()
        candidate.distribution = release
        candidate.package = row[0]
        candidate.campaign = "orphan"
        candidate.context = str(row[2])
        candidate.value = DEFAULT_VALUE_ORPHAN
        yield candidate


async def main():
    import argparse

    import asyncpg

    parser = argparse.ArgumentParser(prog="orphan-candidates")
    parser.add_argument("packages", nargs="*", default=None)
    parser.add_argument("--udd-url", type=str, default=DEFAULT_UDD_URL, help="UDD URL")
    parser.add_argument(
        "--gcp-logging", action='store_true', help='Use Google cloud logging.')

    args = parser.parse_args()

    if args.gcp_logging:
        import google.cloud.logging
        client = google.cloud.logging.Client()
        client.get_default_handler()
        client.setup_logging()
    else:
        logging.basicConfig(format='%(message)s', level=logging.INFO)

    udd = await asyncpg.connect(args.udd_url)
    async for candidate in iter_orphan_candidates(udd, "sid", args.packages or None):
        cl = CandidateList()
        cl.candidate.append(candidate)
        print(cl)


if __name__ == "__main__":
    import asyncio

    asyncio.run(main())
