#!/usr/bin/python3

import logging

import asyncpg

from debian_janitor.candidates_pb2 import Candidate, CandidateList
from debian_janitor.udd import DEFAULT_UDD_URL

DEFAULT_VALUE_NEW_UPSTREAM = 30
DEFAULT_VALUE_WATCH_FIXES = 50


async def iter_candidates(conn, release, packages=None):
    args = [release]
    query = """
SELECT source, version, status, errors, warnings, last_check FROM
upstream_status
WHERE
  release = $1
"""
    if packages is not None:
        args.append(tuple(packages))
        query += " AND source = ANY($2::text[])"
    for entry in await conn.fetch(query, *args):
        candidate = Candidate()
        if entry['status'] in ('Newer version available', 'package available'):
            candidate.distribution = release
            candidate.package = entry['source']
            candidate.context = entry['version']   # Or maybe debian_uversion?
            candidate.campaign = "fresh-releases"
            candidate.value = DEFAULT_VALUE_NEW_UPSTREAM
        elif entry['status'] in ('error', 'only older package available'):
            candidate.distribution = release
            candidate.campaign = "watch-fixes"
            candidate.package = entry['source']
            candidate.context = entry['last_check'].isoformat()
            candidate.value = DEFAULT_VALUE_WATCH_FIXES
        elif entry['status'] == 'Debian version newer than remote site':
            logging.warning(
                '%s has debian version newer than upstream', entry['source'])
            continue
        elif entry['status'] == 'up to date':
            continue
        elif entry['status'] is None:
            logging.warning('No status known for %s', entry['source'])
            continue
        else:
            raise ValueError(
                f"Unexpected status {entry['status']} for {entry['source']}")
        yield candidate


async def main():
    import argparse

    parser = argparse.ArgumentParser(prog="watch-candidates")
    parser.add_argument("packages", nargs="*", default=None)

    parser.add_argument("--udd-url", type=str, default=DEFAULT_UDD_URL, help="UDD URL")
    parser.add_argument("--release", type=str, default="sid")
    parser.add_argument(
        "--gcp-logging", action='store_true', help='Use Google cloud logging.')

    args = parser.parse_args()

    if args.gcp_logging:
        import google.cloud.logging
        client = google.cloud.logging.Client()
        client.get_default_handler()
        client.setup_logging()
    else:
        logging.basicConfig(format='%(message)s', level=logging.INFO)

    udd = await asyncpg.connect(args.udd_url)

    async for candidate in iter_candidates(udd, args.release, args.packages or None):
        cl = CandidateList()
        cl.candidate.append(candidate)
        print(cl)


if __name__ == "__main__":
    import asyncio

    asyncio.run(main())
