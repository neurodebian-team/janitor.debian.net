#!/usr/bin/python3

import logging
from urllib.parse import urlparse

from debian_janitor import is_alioth_url
from debian_janitor.candidates_pb2 import Candidate, CandidateList
from debian_janitor.udd import DEFAULT_UDD_URL

DEFAULT_VALUE_UNCOMMITTED = 60
DEFAULT_VALUE_UNVERSIONED = 50
DEFAULT_VALUE_IMPORT_FROM_ALIOTH = 80
UNCOMMITTED_NMU_BONUS = 60
DEFAULT_VALUE_UNCHANGED = 1


async def iter_candidates(conn, release, packages=None):
    args = [release]
    query = """
SELECT
sources.source as package, vcswatch.changelog_version as package_version,
vcswatch.status as status, vcswatch.error as error, vcswatch.commit_id as hash,
vcswatch.url as url, vcswatch.vcs as vcs
from sources left join vcswatch on vcswatch.source =
sources.source where sources.release = $1
"""
    if packages is not None:
        args.append(tuple(packages))
        query += " AND sources.source = ANY($2::text[])"
    for entry in await conn.fetch(query, *args):
        candidate = Candidate()
        if entry['status'] is None:
            candidate.distribution = release
            candidate.package = entry['package']
            candidate.campaign = "unversioned"
            candidate.value = DEFAULT_VALUE_UNVERSIONED
            candidate.extra_arg.append(f"--package={entry['package']}")
        elif entry['status'] in ('OLD', 'UNREL'):
            value = DEFAULT_VALUE_UNCOMMITTED
            if "nmu" in str(entry['package_version']):
                value += UNCOMMITTED_NMU_BONUS
            candidate.distribution = release
            candidate.package = entry['package']
            candidate.context = entry['package_version']
            candidate.campaign = "uncommitted"
            candidate.value = value
        elif entry['status'] == 'OK':
            continue
        elif entry['status'] in ('COMMITS', 'NEW'):
            candidate.distribution = release
            candidate.package = entry['package']
            #  TODO(jelmer): Convert to bzr revid?
            if entry.get('hash'):
                candidate.context = entry['hash']
            candidate.campaign = "unchanged"
            candidate.value = DEFAULT_VALUE_UNCHANGED
        elif entry['status'] == 'UNREL':
            logging.warning(
                'Ignoring package %s with version that is still UNRELEASED',
                entry['package'])
            continue
        elif entry['status'] == 'TODO':
            logging.warning('Ignoring package %s with status TODO', entry['package'])
            continue
        elif entry['status'] == 'ERROR':
            if is_alioth_url(entry['url']):
                if entry['vcs'] == 'Git':
                    candidate.distribution = release
                    candidate.package = entry['package']
                    candidate.campaign = "alioth-imports"
                    parsed_url = urlparse(entry['url'])
                    path = parsed_url.path.strip('/')
                    if path.startswith('git/'):
                        path = path[4:]
                    if not path.endswith('.git'):
                        path = path + '.git'
                    candidate.command = (
                        'import-alioth-archive.py '
                        f'https://alioth-archive.debian.org/git/{path}.tar.xz')
                    candidate.value = DEFAULT_VALUE_IMPORT_FROM_ALIOTH
                else:
                    candidate.distribution = release
                    candidate.package = entry['package']
                    candidate.campaign = "unversioned"
                    candidate.value = DEFAULT_VALUE_UNVERSIONED
                    candidate.extra_arg.append(f"--package={entry['package']}")
            else:
                logging.warning(
                    'Ignoring package %s with error: %s',
                    entry['package'], entry['error'])
                continue
        else:
            raise ValueError(
                f"Unexpected status {entry['status']} for {entry['package']}")
        yield candidate


async def main():
    import argparse

    import asyncpg

    parser = argparse.ArgumentParser(prog="uncommitted-candidates")
    parser.add_argument("packages", nargs="*", default=None)
    parser.add_argument(
        "--gcp-logging", action='store_true', help='Use Google cloud logging.')
    parser.add_argument("--udd-url", type=str, default=DEFAULT_UDD_URL, help="UDD URL")

    args = parser.parse_args()

    if args.gcp_logging:
        import google.cloud.logging
        client = google.cloud.logging.Client()
        client.get_default_handler()
        client.setup_logging()
    else:
        logging.basicConfig(format='%(message)s', level=logging.INFO)

    udd = await asyncpg.connect(args.udd_url)

    async for candidate in iter_candidates(udd, "sid", args.packages or None):
        cl = CandidateList()
        cl.candidate.append(candidate)
        print(cl)


if __name__ == "__main__":
    import asyncio

    asyncio.run(main())
