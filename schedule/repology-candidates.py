#!/usr/bin/python3

import json
import logging
import sys

from janitor import set_user_agent, state
from janitor.config import read_config
from ognibuild.upstream import find_upstream_from_repology

from debian_janitor.repology import load_from_repology
from debian_janitor.candidates_pb2 import CandidateList

DEFAULT_VALUE_NEW_RELEASE = 30
DEFAULT_VALUE_NEW_SNAPSHOT = 20
DEFAULT_VALUE_DEBIANIZE = 10
DEFAULT_VALUE_DEB_MERGE = 10


MIMICK_DISTROS = ['fedora', 'raspbian', 'ubuntu', 'kali', 'opensuse']


def iter_repo(repos, name):
    for repo in repos:
        if name == repo['repo']:
            yield repo


def find_newest_version(repos):
    version = None
    others = []
    for repo in repos:
        if repo['status'] in ('newest', 'unique'):
            version = repo['version']
            others.append(repo)
    if others:
        return version, others
    raise KeyError


def is_up_to_date(packages):
    statuses = [repo['status'] for repo in packages]
    return 'newest' in statuses or 'unique' in statuses


def iter_related_repos(repos):
    for repo in repos:
        if repo['repo'].startswith('ubuntu_'):
            yield repo
        if repo['repo'].startswith('raspbian_'):
            yield repo
        if repo['repo'].startswith('kali_'):
            yield repo


def iter_repo_with_prefixes(repos, prefixes):
    for repo in repos:
        for prefix in prefixes:
            if repo['repo'].startswith(prefix + '_'):
                yield repo
                break


def repology_to_debian_srcname(name, repos):
    if ':' not in name:
        return name
    family, name = name.split(':')
    if family == 'python':
        return f'python-{name}'
    if family == 'perl':
        return f'lib{name}-perl'
    if family == 'rust':
        return f'rust-{name}'
    if family == 'ruby':
        return f'ruby-{name}'
    if family == 'r':
        cran_packages = iter_repo(repos, 'cran')
        if cran_packages:
            return 'r-cran-{cran_packages[0]["srcname"]}'
        logging.warning('unable to find R repository for r package %s', name)
        return None
    if family == 'gnome':
        return f'gnome-{name}'
    if family == 'go':
        return f'golang-{name}'
    if family == 'haskell':
        return f'haskell-{name}'
    if family == 'octave':
        return f'octave-{name}'
    if family == 'node':
        return f'node-{name}'
    if family == 'php':
        return f'php-{name}'
    if family == 'purple':
        return f'purple-{name}'
    logging.warning('unsupported repology family %r in %s', family, name)
    return None


async def schedule_for_project(name, repos):
    try:
        newest_version, repos_with_newest = find_newest_version(repos)
    except KeyError:
        return

    merge_candidates: list[tuple[str, str | None, str]] = []
    for repo in iter_related_repos(repos_with_newest):
        # TODO(jelmer): Find vcs_url:
        # * Find relevant APT repository
        # * Download metadata
        # * Find Vcs-Git header
        vcs_url = None
        if vcs_url:
            merge_candidates.append((repo['srcname'], vcs_url, repo['repo']))
    unstable_packages = list(iter_repo(repos, 'debian_unstable'))
    if unstable_packages:
        print('# unstable has %s %s' % (
            unstable_packages[-1]['srcname'],
            unstable_packages[-1]['version']))

    fresh_releases_packages = list(iter_repo(repos, 'debian_janitor_releases'))
    if fresh_releases_packages:
        print('# fresh-releases has %s %s' % (
            fresh_releases_packages[-1]['srcname'],
            fresh_releases_packages[-1]['version']))
    if is_up_to_date(fresh_releases_packages):
        pass
    elif (fresh_releases_packages and not is_up_to_date(fresh_releases_packages)
          and unstable_packages and is_up_to_date(unstable_packages)):
        pass  # TODO(jelmer): deb-merge --discard-local
    elif ((fresh_releases_packages and not is_up_to_date(fresh_releases_packages))
            or (not fresh_releases_packages and
                unstable_packages and
                not is_up_to_date(unstable_packages))):
        cl = CandidateList()
        candidate = cl.candidate.add()
        candidate.campaign = "fresh-releases"
        candidate.distribution = 'sid'
        if fresh_releases_packages:
            candidate.package = fresh_releases_packages[0]['srcname']
        else:
            candidate.package = unstable_packages[0]['srcname']
        candidate.context = newest_version
        candidate.value = DEFAULT_VALUE_NEW_RELEASE
        names = [repo["repo"] for repo in repos_with_newest
                 if not repo["repo"].startswith("debian_janitor_")]
        candidate.comment = f'newest version ({newest_version}) present in {names}'
        candidate.origin = "repology"
        print(cl)
    elif any(iter_repo_with_prefixes(repos, MIMICK_DISTROS)):
        for srcname, vcs_url, repo_name in merge_candidates:
            cl = CandidateList()
            candidate = cl.candidate.add()
            candidate.campaign = "debianize"
            candidate.distribution = 'upstream'
            candidate.package = srcname
            candidate.context = newest_version
            candidate.command = 'deb-merge %s' % vcs_url
            candidate.value = DEFAULT_VALUE_DEB_MERGE
            candidate.comment = f'import {newest_version} from {repo_name}'
            candidate.origin = "repology"
            print(cl)
            break
        else:
            for repo in iter_related_repos(repos_with_newest):
                srcname = repo['srcname']
                break
            else:
                srcname = repology_to_debian_srcname(name, repos)
                if srcname is None:
                    logging.error('unable to determine source name for %s', name)

            try:
                upstream_info = find_upstream_from_repology(name)
            except Exception:
                logging.exception('finding upstream info from repology name')
                return

            if srcname and upstream_info and upstream_info.branch_url:
                cl = CandidateList()
                candidate = cl.candidate.add()
                candidate.campaign = "debianize"
                candidate.distribution = 'upstream'
                candidate.package = srcname
                candidate.context = newest_version
                candidate.value = DEFAULT_VALUE_DEBIANIZE
                candidate.command = f'debianize {upstream_info.branch_url}'
                candidate.origin = "repology"
                print(cl)

    fresh_snapshots_packages = list(iter_repo(repos, 'debian_janitor_snapshots'))

    if fresh_snapshots_packages:
        print('# fresh-snapshots has %s %s' % (
            fresh_snapshots_packages[-1]['srcname'],
            fresh_snapshots_packages[-1]['version']))

    if is_up_to_date(fresh_snapshots_packages):
        pass
    elif ((fresh_snapshots_packages and not is_up_to_date(fresh_snapshots_packages))
            or (not fresh_snapshots_packages and
                unstable_packages and
                not is_up_to_date(unstable_packages))):
        cl = CandidateList()
        candidate = cl.candidate.add()
        candidate.campaign = "fresh-snapshots"
        candidate.distribution = 'sid'
        if fresh_snapshots_packages:
            candidate.package = fresh_snapshots_packages[0]['srcname']
        else:
            candidate.package = unstable_packages[0]['srcname']
        candidate.value = DEFAULT_VALUE_NEW_SNAPSHOT
        candidate.origin = "repology"
        print(cl)


async def load_from_db(db):
    async with db.acquire() as conn, conn.transaction():
        for (name, ) in await conn.fetch(
               'SELECT DISTINCT project FROM repology_project_repo'):
            repos = await conn.fetch(
                'SELECT srcname, status, repo, version FROM repology_project_repo '
                'WHERE project = $1', name)
            yield name, repos


async def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--direct', action='store_true')
    parser.add_argument('--config', type=str, default='janitor.conf')
    parser.add_argument('--debug', action='store_true')
    parser.add_argument(
        "--gcp-logging", action='store_true', help='Use Google cloud logging.')
    args = parser.parse_args()

    if args.gcp_logging:
        import google.cloud.logging
        client = google.cloud.logging.Client()
        client.get_default_handler()
        client.setup_logging()
    else:
        logging.basicConfig(
            format='%(message)s',
            level=(logging.DEBUG if args.debug else logging.INFO))

    with open(args.config) as f:
        config = read_config(f)

    set_user_agent(config.user_agent)

    async with state.create_pool(config.database_location) as db:
        if args.direct:
            async def async_load_from_repology():
                for entry in load_from_repology():
                    yield entry
            data = async_load_from_repology()
        else:
            data = load_from_db(db)

        async for name, repos in data:
            await schedule_for_project(name, repos)
        return 0


if __name__ == '__main__':
    import asyncio
    sys.exit(asyncio.run(main()))
