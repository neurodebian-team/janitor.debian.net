#!/usr/bin/python3

import logging

import asyncpg

from debian_janitor.candidates_pb2 import Candidate, CandidateList
from debian_janitor.udd import DEFAULT_UDD_URL

DEFAULT_VALUE_NEW_UPSTREAM_SNAPSHOTS = 20


async def iter_fresh_snapshots_candidates(udd, release, packages):
    args = [release]
    query = """\
SELECT DISTINCT ON (sources.source)
sources.source, exists (
select from upstream_metadata where
key = 'Repository' and source = sources.source)
from sources
where sources.vcs_url != '' and position('-' in sources.version) > 0 AND
sources.release = $1
"""
    if packages is not None:
        query += " AND sources.source = any($1::text[])"
        args.append(tuple(packages))
    query += " ORDER BY sources.source, sources.version DESC"
    for row in await udd.fetch(query, *args):
        candidate = Candidate()
        candidate.distribution = release
        candidate.package = row[0]
        candidate.campaign = "fresh-snapshots"
        candidate.value = DEFAULT_VALUE_NEW_UPSTREAM_SNAPSHOTS
        candidate.success_chance = 1.0 if row[1] else 0.1
        yield candidate


async def main():
    import argparse

    parser = argparse.ArgumentParser(prog="fresh-snapshots-candidates")
    parser.add_argument("packages", nargs="*", default=None)
    parser.add_argument("--udd-url", type=str, default=DEFAULT_UDD_URL, help="UDD URL")
    parser.add_argument(
        "--gcp-logging", action='store_true', help='Use Google cloud logging.')

    args = parser.parse_args()

    if args.gcp_logging:
        import google.cloud.logging
        client = google.cloud.logging.Client()
        client.get_default_handler()
        client.setup_logging()
    else:
        logging.basicConfig(format='%(message)s', level=logging.INFO)

    udd = await asyncpg.connect(args.udd_url)
    async for candidate in iter_fresh_snapshots_candidates(
            udd, "sid", args.packages or None):
        cl = CandidateList()
        cl.candidate.append(candidate)
        print(cl)


if __name__ == "__main__":
    import asyncio

    asyncio.run(main())
