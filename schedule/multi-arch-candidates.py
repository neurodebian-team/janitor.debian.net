#!/usr/bin/python3

import logging

from debian_janitor.candidates_pb2 import Candidate, CandidateList
from lintian_brush.multiarch_hints import (
    calculate_value, download_multiarch_hints, multiarch_hints_by_source,
    parse_multiarch_hints)


async def iter_multiarch_candidates(release, packages=None):
    with download_multiarch_hints() as f:
        hints = parse_multiarch_hints(f)
        bysource = multiarch_hints_by_source(hints)
    for source, entries in bysource.items():
        if packages is not None and source not in packages:
            continue
        hints = [entry["link"].rsplit("#", 1)[-1] for entry in entries]
        value = calculate_value(hints)
        candidate = Candidate()
        candidate.distribution = release
        candidate.campaign = "multiarch-fixes"
        candidate.package = source
        candidate.context = " ".join(sorted(hints))
        candidate.value = value
        yield candidate


async def main():
    import argparse

    parser = argparse.ArgumentParser(prog="multi-arch-candidates")
    parser.add_argument("packages", nargs="*", default=None)
    parser.add_argument("--release", type=str, default="sid")
    parser.add_argument(
        "--gcp-logging", action='store_true', help='Use Google cloud logging.')

    args = parser.parse_args()

    if args.gcp_logging:
        import google.cloud.logging
        client = google.cloud.logging.Client()
        client.get_default_handler()
        client.setup_logging()
    else:
        logging.basicConfig(format='%(message)s', level=logging.INFO)

    async for candidate in iter_multiarch_candidates(args.release, args.packages or None):
        cl = CandidateList()
        cl.candidate.append(candidate)
        print(cl)


if __name__ == "__main__":
    import asyncio

    asyncio.run(main())
