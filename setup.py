#!/usr/bin/python3

from setuptools import setup
from setuptools_protobuf import Protobuf

setup(protobufs=[
    Protobuf('debian_janitor/policy.proto', mypy=True),
    Protobuf('debian_janitor/candidates.proto', mypy=True),
    Protobuf('debian_janitor/package_metadata.proto', mypy=True),
    Protobuf('debian_janitor/package_overrides.proto', mypy=True),
    Protobuf('debian_janitor/upstream_project.proto', mypy=True),
])
