#!/bin/bash -e

podman run --pull=always -it -e BUILD_URL="${BUILD_URL}" -p8080:8080 -v ${DEBIAN_JANITOR_CREDENTIALS}:/credentials.json eu.gcr.io/debian-janitor/worker:latest --credentials=/credentials.json --base-url https://janitor.debian.net/api/ --tee "$@"
