This repository contains the configuration for https://janitor.debian.net/.
It's an instance of the Janitor project, which can be found at
https://github.com/jelmer/janitor.

schedule/ contains the various scripts for importing package metadata
and candidates.

See the k8s/ directory for the kubernetes configuration.

If you're looking to adjust the policy for a specific team, package, maintainer
or URL please take a look at the [policy.conf](k8s/policy.conf) file.

Debian-Janitor-specific services
================================

The Debian Janitor runs stock versions of various services that are
a part of the core Janitor project. In addition, it has a couple of
its own:

* *site* - The web site, with integration for other Debian services and
  e.g. authentication with Salsa
* *followup* - Schedules followup actions for builds

It ships a custom worker image that bundles the codemods.

The "schedule" container is run regularly and schedules new runs based
on data in e.g. UDD.

Relationship with other projects
================================

The Debian Janitor is built on top of the core janitor project, which lives at
https://github.com/jelmer/janitor. It adds the web site and all the Debian specific bits.
This separation isn't super clear at the moment, and ongoing.

Most of the actual work is done by other projects, which aren't specific to the
Debian Janitor. The only requirement is that they support the
[silver-platter codemod protocol
](https://github.com/jelmer/silver-platter/blob/master/codemod-protocol.rst>).

For the Debian Janitor, these projects currently are:

* [lintian-brush](https://salsa.debian.org/jelmer/lintian-brush). Provides
  *lintian-brush*, *debianize*, *deb-scrub-obsolete* and
  *apply-multi-arch-hints*

* [brz-debian](https://code.launchpad.net/brz-debian). Provides
  *deb-import-upstream*, *deb-auto-backport*, *deb-move-orphaned* and
  *deb-import-uncommitted*

* [debmutate](https://salsa.debian.org/jelmer/debmutate). Provides *drop-mia-uploaders*

Contributing
============

To coordinate, join the Matrix/IRC channel at
[https://matrix.to/#/#debian-janitor:matrix.debian.social](#debian-janitor:matrix.debian.social).

There are several ways to contribute.

You can join the [reviewer
group](https://salsa.debian.org/janitor-team/reviewers) and [review
changes](https://janitor.debian.net/cupboard/review) from
the Janitor.

If you have an idea for a change the janitor could make, but no time to work on it,
[file a bug](https://salsa.debian.org/janitor-team/janitor.debian.net/-/issues/new).

You can also work on
identifying issues and adding fixers for lintian-brush. There is
[a guide](https://salsa.debian.org/jelmer/lintian-brush/-/blob/master/doc/fixer-writing-guide.rst)
on identifying good candidates and writing fixers in the lintian-brush
repository.

If you're interested in working on adding another campaign, see
[adding-a-new-campaign](https://github.com/jelmer/janitor/blob/master/devnotes/adding-a-new-campaign.rst).
